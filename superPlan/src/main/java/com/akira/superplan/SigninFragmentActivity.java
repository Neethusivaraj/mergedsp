package com.akira.superplan;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.akira.superplan.adapters.CustomViewPager;
import com.akira.superplan.api.ApiMethods;
import com.akira.superplan.calls.TelephonyInfo;
import com.akira.superplan.dao.OperatorDetails;
import com.akira.superplan.fragments.CreateProfileFragment;
import com.akira.superplan.fragments.OTPFragment;
import com.akira.superplan.fragments.TourFragment;
import com.akira.superplan.fragments.VerifyNumberFragment;
import com.akira.superplan.model.GenerateOTP;
import com.akira.superplan.repositories.NumberCodesRepo;
import com.akira.superplan.repositories.OperatorDetailsRepo;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.EventBusHelper;
import com.akira.superplan.utils.Logger;
import com.akira.superplan.utils.Utils;
import com.squareup.okhttp.OkHttpClient;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class SigninFragmentActivity extends FragmentActivity {

    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private Dialog emailsDialog;
    private LinearLayout popupLayout;
    private LinearLayout transparentLayout;
    private EditText mEditText;
    CountDownTimer timer;

    Context context =this;
     private SharedPreference sharedPreference;
    private String TAG="sign in fragment";
    private int kcount=1;
    public String k;
    public String otp;
    private String operatorName;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_signin);

        replaceFragment(new TourFragment(), true);
        popupLayout = (LinearLayout) findViewById(R.id.popup_layout);
        transparentLayout = (LinearLayout) findViewById(R.id.transparentLayout);
    }

    public void getNumberDetails(String number) {
        SharedPreferences sp = DaoTestApplication.getSharedPreferences();
        SharedPreferences.Editor editor = sp.edit();
        String type = null;
        number = number.substring(0, 4);

       // NumberCode numberCodes = NumberCodesRepo.get(getApplicationContext(), number);
       // editor.putString("myCircleCode", NumberCodesRepo.getNumberCircle(getApplicationContext(), number));
        Log.d(TAG, "telecom circle :" + NumberCodesRepo.getNumberCircle(getApplicationContext(), number));
        //editor.putString("myOperatorCode", NumberCodesRepo.getNetworkOperator(getApplicationContext(), number));
        Log.d(TAG, " operator code :" + NumberCodesRepo.getNetworkOperator(getApplicationContext(), number));
        editor.putString("myCircleCode", NumberCodesRepo.getNumberCircle(getApplicationContext(), number));
        editor.putString("myOperatorCode",NumberCodesRepo.getNetworkOperator(getApplicationContext(), number));
        editor.commit();
        OperatorDetails mOperatorDetails = OperatorDetailsRepo.getOperatorName(
                getApplicationContext(),NumberCodesRepo.getNetworkOperator(getApplicationContext(), number));
      

        operatorName=mOperatorDetails.getOperatorName();

    }

    private void getPhoneType(Context context) {
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
        String lPhoneType;
        String imeiSIM1 = telephonyInfo.getImeiSIM1();
        String imeiSIM2 = telephonyInfo.getImeiSIM2();
        boolean isDualSIM = false;
        if (imeiSIM2 != null) {
            isDualSIM = !(imeiSIM1.equals(imeiSIM2));
        } else {
            isDualSIM = telephonyInfo.isDualSIM();
        }
        if (isDualSIM) {
            lPhoneType = "Dual";
        } else {
            lPhoneType = "Single";
            SharedPreferences settings = context.getSharedPreferences("SimType", 0);
            SharedPreferences.Editor editor = settings.edit();
            Log.d(TAG,"sim type for u:"+lPhoneType);
            editor.putString("simtype", lPhoneType);
            editor.commit();

        }
    }
    public void replaceFragment(final Fragment fragment, final boolean addToBackStack) {
        final String backStateName = fragment.getClass().getName();
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.fragment_container, fragment);
        if (addToBackStack) {
            mFragmentTransaction.addToBackStack(backStateName);
        }
        mFragmentTransaction.commit();
}

    @Override
    public void onBackPressed() {
        if (popupLayout.getVisibility() == View.VISIBLE) {
            if (popupLayout.getTag().equals(R.string.verify_popup)) {
                timer.cancel();
                removePopup();
                replaceFragment(new VerifyNumberFragment(), true);
            }
            if (popupLayout.getTag().equals(R.string.emails_popup)) {
                removePopup();
            }
        } else {
            final FragmentManager fragmentManager = getSupportFragmentManager();
            final int count = fragmentManager.getBackStackEntryCount();
            if (count == 1) {
                finish();
            } else {
                super.onBackPressed();

            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBusHelper.registerSubscriber(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBusHelper.unregisterSubscriber(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void onEvent(final String message) {
        if (!TextUtils.isEmpty(message)) {
            if (message.equals(Constants.SHOW_EMAILS_POPUP)) {
                showSelectEmailsView();
            } else if (message.equals(Constants.OPEN_VERIFY_NUMBER_SCREEN)) {
                replaceFragment(new VerifyNumberFragment(), true);
            } else if (message.equals(Constants.SHOW_AUTHENTICATING_POPUP)) {
                showAuthenticationPopup();
            } else if (message.equals(Constants.OPEN_CREATE_PROFILE)) {
                replaceFragment(new CreateProfileFragment(), true);
            } else if (message.equals(Constants.PERFORM_BACK_PRESS)) {
                onBackPressed();
            }

        }
    }
    public void showSelectEmailsView() {
        final View child = getLayoutInflater().inflate(R.layout.popup_select_emails, null);

        popupLayout.removeAllViews();
        popupLayout.addView(child);
        popupLayout.setTag(getString(R.string.emails_popup));
        Utils.overrideFonts(this, child);

        final RadioGroup radioGroup = (RadioGroup) child.findViewById(R.id.radio_group);
        final Button continueButton = (Button) child.findViewById(R.id.continue_button);
        radioGroup.removeAllViews();
        try {
            final Account[] accounts = AccountManager.get(this).getAccountsByType("com.google");
            int i = 0;
            for (final Account account : accounts) {
                i = i + 1;
                final String name = account.name;
                if (!TextUtils.isEmpty(name)) {
                    final RadioButton radioButton = new RadioButton(this);
                    radioButton.setText(name);
                    radioButton.setId(i + 100);
                    radioGroup.addView(radioButton);
                    if (i == 1) {
                        radioGroup.check(radioButton.getId());
                    }
                }
            }

        } catch (final Exception e) {
        }


        continueButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                if (radioGroup.getCheckedRadioButtonId() != -1) {

                    final int id = radioGroup.getCheckedRadioButtonId();
                    final View radioButton = radioGroup.findViewById(id);
                    final int radioId = radioGroup.indexOfChild(radioButton);
                    final RadioButton btn = (RadioButton) radioGroup.getChildAt(radioId);
                    final String emailID = (String) btn.getText();
                    removePopup();
                    String number = "8870715891";
                    number = number.substring(0, 4);
                    Logger.d(TAG, " " + number);
                    getNumberDetails(number);
                    getPhoneType(getApplicationContext());
                    DaoTestApplication mainObject = DaoTestApplication.getInstance();
                    mainObject.setOperatorName(operatorName);
                    replaceFragment(new VerifyNumberFragment(), true);
                }
            }

        });
        showPopup();

        CustomViewPager pager = (CustomViewPager) findViewById(R.id.tour_screens);
        pager.setPagingEnabled(false);
    }
    private void showPopup() {
        final Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);

        popupLayout.startAnimation(bottomUp);
        popupLayout.setVisibility(View.VISIBLE);
        transparentLayout.setVisibility(View.VISIBLE);
        transparentLayout.bringToFront();
        popupLayout.bringToFront();
    }

    private void removePopup() {
        final Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        popupLayout.startAnimation(bottomUp);
        popupLayout.setVisibility(View.GONE);
        transparentLayout.setVisibility(View.GONE);
    }

    public void showAuthenticationPopup() {
        final View child = getLayoutInflater().inflate(R.layout.popup_authentication_number, null);
        Utils.overrideFonts(this, child);

        popupLayout.removeAllViews();
        popupLayout.addView(child);
        popupLayout.setTag(R.string.verify_popup);
        showPopup();

        timer = new CountDownTimer(Constants.AUTHENTICATING_POPUP_TIME, 1000) {

            @Override
            public void onTick(final long millisUntilFinished) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onFinish() {
                showDonePopup();
            }
        }.start();

        SpannableString ss = new SpannableString("Thanks. We have sent a One Time Password to your number. " +
                "Please give us a moment to verify. Stuck on verification?" +
                " Enter OTP manually.");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                timer.cancel();
                removePopup();

                replaceFragment(new OTPFragment(), true);
             }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 115, 133, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        ss.setSpan(new ForegroundColorSpan(Color.BLUE), 115, 133, 0);
        TextView textView = (TextView) findViewById(R.id.desc);
        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

    }

    private void showDonePopup() {
        final View child = getLayoutInflater().inflate(R.layout.popup_done, null);
        Utils.overrideFonts(this, child);
        popupLayout.removeAllViews();
        popupLayout.addView(child);
        popupLayout.setTag(R.string.verify_popup);
        showPopup();
        new CountDownTimer(Constants.AUTHENTICATING_POPUP_TIME, 1000) {

            @Override
            public void onTick(final long millisUntilFinished) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onFinish() {
                getOTP();
               // showOTPPopup();
            }
        }.start();
    }
    private void showOTPPopup() {
        final View child = getLayoutInflater().inflate(R.layout.popup_enter_otp, null);
        Utils.overrideFonts(this, child);
        popupLayout.removeAllViews();
        popupLayout.addView(child);
        popupLayout.setTag(R.string.verify_popup);
        mEditText = (EditText) child.findViewById(R.id.phone_number_view);
       Log.d(TAG,"otp to showotp frag:"+otp);
        mEditText.setText(otp);
        final TextView submitTextView = (TextView) child.findViewById(R.id.submit);
        submitTextView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                if (validate()) {
                    removePopup();
                    EventBusHelper.postMessage(Constants.OPEN_CREATE_PROFILE);

               //     replaceFragment(new OTPFragment(), true);
                } else {
                    return;
                }
            }
        });
        showPopup();

    }
    final RequestInterceptor requestInterceptor = new RequestInterceptor() {
        public void intercept(RequestFacade request) {
            //String device_token = DaoTestApplication.getSharedPreferences().getString(Constants.DEVICE_TOKEN, "");
            //   String install_id = DaoTestApplication.getSharedPreferences().getString(Constants.INSTALLATION_ID,"");
            request.addHeader("Content-Type", "application/json");
            //  request.addHeader("Application-Id", app_token);
            //  request.addHeader("Device-Token",device_token);
            // request.addHeader("Installation-Id",install_id);
        }
    };
    public void getOTP() {
        final Cursor cursor = getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
        if ( cursor.moveToFirst()) { // must check the result to prevent
            // exception
            do {
                String msgData = "";
                for (int idx = 0; idx < cursor.getColumnCount(); idx++) {
                    msgData += " " + cursor.getColumnName(idx) + ":" + cursor.getString(idx);
                     k=cursor.getString(idx);
                    Log.d(TAG,"string :"+k);
                    Log.d(TAG,"message data:"+msgData);
                     if(msgData.contains("body:Hi, Please find your OTP "))
                     {
                         Log.d(TAG, "messssage found !!!");
                         cursor.close();

                         kcount=8;

                     }
                    if(kcount==8) {
                        k = k.replaceAll("\\D+", "");

                        if (k != null) {

                            otp = k;
                            Log.d(TAG, "got otp:" + otp);
                            OkHttpClient okHttpClient = new OkHttpClient();
                            //okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
                            okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);
                            RestAdapter restAdapter = new RestAdapter.Builder()
                                    .setEndpoint(Constants.API_URL)
                                    .setRequestInterceptor(requestInterceptor)
                                    .setLogLevel(RestAdapter.LogLevel.FULL)
                                    .setClient(new OkClient(okHttpClient))
                                    .build();
                            ApiMethods methods = restAdapter.create(ApiMethods.class);

                            HashMap<String, String> data = new HashMap<String, String>();
                            data.put("otp_id", "15 ");
                            data.put("otp",otp);
                            data.put("mobile_number", "8870715891");


                            GenerateOTP otpdetails = new GenerateOTP();
                            sharedPreference = new SharedPreference();
                            // otpdetails.number = sharedPreference.getValue(context);;
                            // otpdetails.data=otpdetails.number;

                            //Logger.d(TAG, "otp executing started2!!" + otpdetails.number);
                            otpdetails.setData(data);
                            otpdetails.setUserId("1");
                            otpdetails.setEpoch("123456");
                            otpdetails.setSignature("");
                            methods.putVerifyOTP(otpdetails, new Callback<GenerateOTP>() {
                                @Override

                                public void success(GenerateOTP generateOTP, Response response) {
                                    Logger.d(TAG, "verify otp id:" + generateOTP.getId());
                                    Logger.d(TAG,"verify otp mobile:"+generateOTP.getMobile_number());
                                    Logger.d(TAG,"verify otp operator:"+generateOTP.getOperator_code());
                                    Logger.d(TAG,"verify otp circle:"+generateOTP.getCircle_code());

                                }

                                @Override

                                public void failure(RetrofitError error) {
                                    Logger.d(TAG, " api details error " + error.getUrl());
                                    //  Logger.d(TAG, " api details error " + error.getBody());
                                    Logger.d(TAG, " api details error " + error.getLocalizedMessage());
                                }
                            });

                          //  fragment.setArguments(bundle);
                            showOTPPopup();

                        }

                       return;

                    }
                }
                // use msgData
            } while (cursor.moveToNext());
        } else {
            // empty box, no SMS
        }
    }

    public boolean validate() {
        boolean valid = true;

        String otp_number = mEditText.getText().toString();

        if (otp_number.isEmpty() || otp_number.length() < 6 || otp_number.length() > 6) {
            mEditText.setError("Incorrect OTP Number");
            valid = false;
        } else {
            mEditText.setError(null);
        }

        return valid;
    }

}
