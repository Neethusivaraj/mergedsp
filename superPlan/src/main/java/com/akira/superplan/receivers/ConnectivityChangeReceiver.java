package com.akira.superplan.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.os.Bundle;
import android.util.Log;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.DataLog;
import com.akira.superplan.dao.WifiList;
import com.akira.superplan.repositories.DataLogRepo;
import com.akira.superplan.repositories.WifiListRepo;
import com.akira.superplan.utils.Connectivity;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.Logger;

import java.util.Date;
import java.util.List;

/**
 * Created by nanda on 6/15/15.
 */
public class ConnectivityChangeReceiver extends BroadcastReceiver {

    private static final String TAG = "ConnectivityChangeReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
//        debugIntent(intent, "Debugging");
        measureDataUsage(context, intent);
    }

    private void measureDataUsage(Context context, Intent intent) {
        SharedPreferences sp = DaoTestApplication.getSharedPreferences();
        SharedPreferences.Editor editor = sp.edit();
        String type, status, currentStatus, previousStatus;
        long defaultValue = 0L;
        long spTotalRx = sp.getLong(Constants.TOTAL_RX_BYTES, defaultValue);
        long spTotalTx = sp.getLong(Constants.TOTAL_TX_BYTES, defaultValue);
        previousStatus = sp.getString(Constants.CONNECTIVITY, "");

        Bundle extras = intent.getExtras();
        NetworkInfo networkInfo = (NetworkInfo) extras.get("networkInfo");
        type = networkInfo.getTypeName();
        if (type.equalsIgnoreCase("mobile") || type.equalsIgnoreCase("mobile_hipri")
                || type.equalsIgnoreCase("mobile_supl") || type.equalsIgnoreCase("mobile_mms")
                || type.equalsIgnoreCase("mobile_dun")) {
            type = "MOBILE";
        }
        status = networkInfo.getState().toString();

        currentStatus = getCurrentStatus(type, status);

        if (previousStatus.equals(currentStatus)) {
            return;
        }
        if (currentStatus.equals(Constants.WIFI_DISCONNECTED)) {
            long totalRxBytes = TrafficStats.getTotalRxBytes();
            long totalTxBytes = TrafficStats.getTotalTxBytes();
            editor.putLong(Constants.TOTAL_RX_BYTES, totalRxBytes);
            editor.putLong(Constants.TOTAL_TX_BYTES, totalTxBytes);
            editor.putString(Constants.CONNECTIVITY, currentStatus);
            editor.commit();

            long rx = totalRxBytes - spTotalRx;
            long tx = totalTxBytes - spTotalTx;
            WifiListRepo.updateTime(context, rx, tx);
            List<WifiList> allWifiList = WifiListRepo.getAllWifiList(context);
            for (WifiList wifiList : allWifiList) {
                Logger.d(TAG, "disconnected " + wifiList.getSSID() + " " + wifiList.getWifiName()
                        + " " + wifiList.getLatitude() + " " + wifiList.getLongitude() + " "
                        + wifiList.getConnectionEstablishedTime() + " " + wifiList.getRx() + " "
                        + wifiList.getTx() + " " + wifiList.getConnectionOffTime());
            }
        }
        if (currentStatus.equals(Constants.WIFI_CONNECTED)) {
            Connectivity.getWifiInfo(context);
            List<WifiList> wifiLists = WifiListRepo.getAllWifiList(context);
            for (WifiList wifiList : wifiLists) {
                Logger.d(TAG, "connected " + wifiList.getSSID() + " " + wifiList.getWifiName()
                        + " " + wifiList.getLatitude() + " " + wifiList.getLongitude() + " "
                        + wifiList.getConnectionEstablishedTime() + " " + wifiList.getRx() + " "
                        + wifiList.getTx() + " " + wifiList.getConnectionOffTime());
            }
        }
        if (status.equals("DISCONNECTED")) {
            long totalRxBytes = TrafficStats.getTotalRxBytes();
            long totalTxBytes = TrafficStats.getTotalTxBytes();
            editor.putLong(Constants.TOTAL_RX_BYTES, totalRxBytes);
            editor.putLong(Constants.TOTAL_TX_BYTES, totalTxBytes);
            editor.putString(Constants.CONNECTIVITY, currentStatus);
            editor.commit();

            long rx = totalRxBytes - spTotalRx;
            long tx = totalTxBytes - spTotalTx;

            Date happenedAt = new Date(System.currentTimeMillis());
            DataLog dataLog = new DataLog();
            dataLog.setDataResponseID((long) 0);
            dataLog.setRx(rx);
            dataLog.setTx(tx);
            dataLog.setType(type);
            dataLog.setHappenedAt(happenedAt);
            dataLog.setStatus(Constants.STATUS_UNSENT);
          //  dataLog.setMyNumber(DaoTestApplication.getInstance().getMyPhoneNumber());
            DataLogRepo.insertOrUpdate(context, dataLog);
            DaoTestApplication.getInstance().startPeriodLogService(context);
        } else {
            editor.putString(Constants.CONNECTIVITY, currentStatus);
            editor.commit();
        }
    }

    private String getCurrentStatus(String type, String status) {
        if (status.equals("DISCONNECTED") && type.equalsIgnoreCase("WIFI")) {
            return Constants.WIFI_DISCONNECTED;
        } else if (status.equals("CONNECTED") && type.equalsIgnoreCase("WIFI")) {
            return Constants.WIFI_CONNECTED;
        } else if (status.equals("DISCONNECTED") && type.equalsIgnoreCase("MOBILE")) {
            return Constants.MOBILE_DISCONNECTED;
        } else if (status.equals("CONNECTED") && type.equalsIgnoreCase("MOBILE")) {
            return Constants.MOBILE_CONNECTED;
//        else if(status.equals("DISCONNECTED") && type.equals("mobile"))
//            return Constants.MOBILE_DISCONNECTED;
//        else if(status.equals("CONNECTED") && type.equals("mobile"))
//            return Constants.MOBILE_CONNECTED;
//        else if(status.equals("DISCONNECTED") && type.equals("wifi"))
//            return Constants.MOBILE_DISCONNECTED;
//        else if(status.equals("CONNECTED") && type.equals("wifi"))
//            return Constants.MOBILE_CONNECTED;
        }
        return status + type;
    }

    private void debugIntent(Intent intent, String tag) {
        Log.i(tag, "action: " + intent.getAction());
        Log.i(tag, "component: " + intent.getComponent());
        Bundle extras = intent.getExtras();
        if (extras != null) {
            for (String key : extras.keySet()) {
                Log.i(tag, "key [" + key + "]: " + extras.get(key));
            }
        } else {
            Log.v(tag, "no extras");
        }
    }
}
