package com.akira.superplan.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.TrafficStats;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.services.ContentObserverService;
import com.akira.superplan.utils.Connectivity;
import com.akira.superplan.utils.Constants;


public class BootCompletedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        /**
         * Start the content observer service which registers for the content URIs
         *
         */
        Intent mIntent = new Intent(context, ContentObserverService.class);
        context.startService(mIntent);

        /**
         * Start the periodical send to server service
         *
         */
        DaoTestApplication.getInstance().startPeriodSendToServer(context);

        /**
         * Set the shared preferences with total rx/tx and the current connectivity status
         *
         */
        SharedPreferences sp = DaoTestApplication.getSharedPreferences();
        SharedPreferences.Editor editor = sp.edit();

        long totalRx = TrafficStats.getTotalRxBytes();
        long totalTx = TrafficStats.getTotalTxBytes();

        editor.putLong(Constants.TOTAL_RX_BYTES, totalRx);
        editor.putLong(Constants.TOTAL_TX_BYTES, totalTx);
        String currentStatus = Connectivity.getCurrentNetworkStatus(context);
        editor.putString(Constants.CONNECTIVITY, currentStatus);
        editor.commit();
        DaoTestApplication.getInstance().startPeriodLogService(context);
    }
}
