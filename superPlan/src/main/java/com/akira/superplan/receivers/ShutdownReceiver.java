package com.akira.superplan.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.TrafficStats;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.DataLog;
import com.akira.superplan.repositories.DataLogRepo;
import com.akira.superplan.utils.Connectivity;
import com.akira.superplan.utils.Constants;

import java.util.Date;

/**
 * Created by nanda on 6/17/15.
 */
public class ShutdownReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        /**
         * Insert data log entry
         *
         */

        SharedPreferences sp = DaoTestApplication.getSharedPreferences();
        long defaultValue = 0L;
        long spTotalRx = sp.getLong(Constants.TOTAL_RX_BYTES, defaultValue);
        long spTotalTx = sp.getLong(Constants.TOTAL_TX_BYTES, defaultValue);
        long totalRx = TrafficStats.getTotalRxBytes();
        long totalTx = TrafficStats.getTotalTxBytes();
        long rx = totalRx - spTotalRx;
        long tx = totalTx - spTotalTx;
        String type = Connectivity.getNetworkType(context);

        Date happenedAt = new Date(System.currentTimeMillis());
        DataLog dataLog = new DataLog();
        dataLog.setDataResponseID((long) 0);
        dataLog.setRx(rx);
        dataLog.setTx(tx);
        dataLog.setType(type);
        dataLog.setHappenedAt(happenedAt);
        dataLog.setStatus(Constants.STATUS_UNSENT);
        //dataLog.setMyNumber(DaoTestApplication.getInstance().getMyPhoneNumber());
        DataLogRepo.insertOrUpdate(context, dataLog);
    }
}
