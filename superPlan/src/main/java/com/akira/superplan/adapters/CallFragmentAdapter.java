package com.akira.superplan.adapters;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.akira.superplan.R;
import com.akira.superplan.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nanda on 30/4/15.
 */
public class CallFragmentAdapter extends BaseAdapter {

    private static final String TAG = "CallFragmentAdapter";

    public ArrayList<HashMap<String, String>> list;
    Fragment fragment;

    public CallFragmentAdapter(Fragment fragment, ArrayList<HashMap<String, String>> list) {
        super();
        this.fragment = fragment;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = fragment.getActivity().getLayoutInflater();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.column_row, null);
            holder = new ViewHolder();
            holder.txtFirst = (TextView) convertView.findViewById(R.id.txtNumber);
            holder.txtSecond = (TextView) convertView.findViewById(R.id.txtType);
            holder.txtThird = (TextView) convertView.findViewById(R.id.txtDuration);
            holder.txtFourth = (TextView) convertView.findViewById(R.id.txtHappenedAt);
            holder.txtFifth = (TextView) convertView.findViewById(R.id.RoamingStatus);
            holder.txtSixth = (TextView) convertView.findViewById(R.id.PopupText);
            holder.txtSeventh = (TextView) convertView.findViewById(R.id.Network);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        HashMap<String, String> map = list.get(position);

//        Logger.d(TAG, "Cost : " + map.get(Constants.SIXTH_COLUMN).trim());

        holder.txtFirst.setText(map.get(Constants.FIRST_COLUMN));
        holder.txtSecond.setText(map.get(Constants.SECOND_COLUMN));
        holder.txtThird.setText(map.get(Constants.THIRD_COLUMN));
        holder.txtFourth.setText(map.get(Constants.FOURTH_COLUMN));
        holder.txtFifth.setText(map.get(Constants.FIFTH_COLUMN));
        holder.txtSixth.setText(map.get(Constants.SIXTH_COLUMN));
         holder.txtSeventh.setText(map.get(Constants.SEVENTH_COLUMN));
        return convertView;
    }


     private class ViewHolder {
         TextView txtFirst;
         TextView txtSecond;
         TextView txtThird;
         TextView txtFourth;
         TextView txtFifth;
         TextView txtSixth;
         TextView txtSeventh;
     }


}
