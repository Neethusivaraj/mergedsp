package com.akira.superplan.adapters;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.akira.superplan.R;
import com.akira.superplan.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;

public class DataLogFragmentAdapter extends BaseAdapter {

    Fragment fragment;
    ArrayList<HashMap<String, String>> list;


    public DataLogFragmentAdapter(Fragment fragment, ArrayList<HashMap<String, String>> list) {
        super();
        this.fragment = fragment;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        LayoutInflater inflater = fragment.getActivity().getLayoutInflater();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.datausage_column_row, null);
            viewHolder = new ViewHolder();
            viewHolder.tvTotalRx = (TextView) convertView.findViewById(R.id.txtTotalRx);
            viewHolder.tvTotalTx = (TextView) convertView.findViewById(R.id.txtTotalTx);
            viewHolder.tvType = (TextView) convertView.findViewById(R.id.txtType);
            viewHolder.tvHappenedAt = (TextView) convertView.findViewById(R.id.txtHappenedAt);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        HashMap<String, String> map = list.get(position);
        viewHolder.tvTotalRx.setText("Rx - " + map.get(Constants.TOTAL_RX_BYTES));
        viewHolder.tvTotalTx.setText("Tx - " + map.get(Constants.TOTAL_TX_BYTES));
        viewHolder.tvType.setText("Type - " + map.get(Constants.COL_TYPE));
        viewHolder.tvHappenedAt.setText("" + map.get(Constants.COL_HAPPENED_AT));

        return convertView;
    }

    private class ViewHolder {
        TextView tvTotalRx;
        TextView tvTotalTx;
        TextView tvType;
        TextView tvHappenedAt;
    }
}
