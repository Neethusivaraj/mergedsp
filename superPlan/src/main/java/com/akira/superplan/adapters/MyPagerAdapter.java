package com.akira.superplan.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.akira.superplan.fragments.CallsFragment;
import com.akira.superplan.fragments.DataLogFragment;
import com.akira.superplan.fragments.MessageFragment;
import com.akira.superplan.utils.Constants;

/**
 * Created by nanda on 5/25/15.
 */
public class MyPagerAdapter extends FragmentPagerAdapter {

    private String[] tabNames = {"Calls", "SMS", "Data"};

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabNames[position];
    }

    @Override
    public int getCount() {
        return Constants.NUMBER_OF_TABS;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new CallsFragment();
            case 1:
                return new MessageFragment();
            default:
                return new DataLogFragment();
        }

    }
}