package com.akira.superplan.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.akira.superplan.R;
import com.akira.superplan.utils.Utils;

public class RegionsListAdapter extends BaseAdapter {
    private final Context context;
    public RegionsListAdapter(final Context context) {
        this.context = context;
    }
    @Override
    public int getCount() {
        return 20;
    }

    @Override
    public Object getItem(final int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(final int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View view, final ViewGroup parent) {
        if (view == null) {
            final ViewHolder holder = new ViewHolder();
            final LayoutInflater mInflater = LayoutInflater.from(context);
            final View convertView = mInflater.inflate(R.layout.item_region, null);
            Utils.overrideFonts(context, convertView);
            convertView.setTag(holder);
            view = convertView;

        }
        return view;
    }
    class ViewHolder {
        TextView mRegionName;
    }

}
