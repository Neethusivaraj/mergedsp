package com.akira.superplan.adapters;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.akira.superplan.R;
import com.akira.superplan.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;

public class MessageFragmentAdapter extends BaseAdapter {

    Fragment fragment;
    ArrayList<HashMap<String, String>> list;


    public MessageFragmentAdapter(Fragment fragment, ArrayList<HashMap<String, String>> list) {
        super();
        this.fragment = fragment;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        LayoutInflater inflater = fragment.getActivity().getLayoutInflater();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.messages_column_row, null);
            viewHolder = new ViewHolder();
            viewHolder.tvOtherNumber = (TextView) convertView.findViewById(R.id.txtMessageNumber);
            viewHolder.tvType = (TextView) convertView.findViewById(R.id.txtMessageType);
            viewHolder.tvMessageLength = (TextView) convertView.findViewById(R.id.txtMessageLength);
            viewHolder.tvHappenedAt = (TextView) convertView.findViewById(R.id.txtMessageHappenedAt);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        HashMap<String, String> map = list.get(position);
        viewHolder.tvOtherNumber.setText(map.get(Constants.COL_NUMBER));
        viewHolder.tvType.setText(map.get(Constants.COL_TYPE));
        viewHolder.tvMessageLength.setText(map.get(Constants.COL_MESSAGE_LENGTH));
        viewHolder.tvHappenedAt.setText(map.get(Constants.COL_HAPPENED_AT));

        return convertView;
    }

    private class ViewHolder {
        TextView tvOtherNumber;
        TextView tvType;
        TextView tvMessageLength;
        TextView tvHappenedAt;
    }


}
