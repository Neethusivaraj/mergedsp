package com.akira.superplan.dao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table "PLAN".
 */
public class Plan {

    private Integer PlanId;
    private String PlanName;
    private String OperatorCode;
    private String CircleCode;
    private String PlanType;
    private String PlanCategory;
    private String Cost;
    private String Validity;
    private String PlanText;
    private String RecommendationType;
    private Integer Rank;
    private java.util.Date Date;

    public Plan() {
    }

    public Plan(Integer PlanId, String PlanName, String OperatorCode, String CircleCode, String PlanType, String PlanCategory, String Cost, String Validity, String PlanText, String RecommendationType, Integer Rank, java.util.Date Date) {
        this.PlanId = PlanId;
        this.PlanName = PlanName;
        this.OperatorCode = OperatorCode;
        this.CircleCode = CircleCode;
        this.PlanType = PlanType;
        this.PlanCategory = PlanCategory;
        this.Cost = Cost;
        this.Validity = Validity;
        this.PlanText = PlanText;
        this.RecommendationType = RecommendationType;
        this.Rank = Rank;
        this.Date = Date;
    }

    public Integer getPlanId() {
        return PlanId;
    }

    public void setPlanId(Integer PlanId) {
        this.PlanId = PlanId;
    }

    public String getPlanName() {
        return PlanName;
    }

    public void setPlanName(String PlanName) {
        this.PlanName = PlanName;
    }

    public String getOperatorCode() {
        return OperatorCode;
    }

    public void setOperatorCode(String OperatorCode) {
        this.OperatorCode = OperatorCode;
    }

    public String getCircleCode() {
        return CircleCode;
    }

    public void setCircleCode(String CircleCode) {
        this.CircleCode = CircleCode;
    }

    public String getPlanType() {
        return PlanType;
    }

    public void setPlanType(String PlanType) {
        this.PlanType = PlanType;
    }

    public String getPlanCategory() {
        return PlanCategory;
    }

    public void setPlanCategory(String PlanCategory) {
        this.PlanCategory = PlanCategory;
    }

    public String getCost() {
        return Cost;
    }

    public void setCost(String Cost) {
        this.Cost = Cost;
    }

    public String getValidity() {
        return Validity;
    }

    public void setValidity(String Validity) {
        this.Validity = Validity;
    }

    public String getPlanText() {
        return PlanText;
    }

    public void setPlanText(String PlanText) {
        this.PlanText = PlanText;
    }

    public String getRecommendationType() {
        return RecommendationType;
    }

    public void setRecommendationType(String RecommendationType) {
        this.RecommendationType = RecommendationType;
    }

    public Integer getRank() {
        return Rank;
    }

    public void setRank(Integer Rank) {
        this.Rank = Rank;
    }

    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date Date) {
        this.Date = Date;
    }

}
