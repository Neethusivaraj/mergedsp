package com.akira.superplan.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * DAO for table "MESSAGE_HISTORY".
*/
public class MessageHistoryDao extends AbstractDao<MessageHistory, Long> {

    public static final String TABLENAME = "MESSAGE_HISTORY";

    /**
     * Properties of entity MessageHistory.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property MessageResponseId = new Property(0, Long.class, "MessageResponseId", false, "MESSAGE_RESPONSE_ID");
        public final static Property MessageHistoryId = new Property(1, Long.class, "MessageHistoryId", true, "MESSAGE_HISTORY_ID");
        public final static Property MessageLogId = new Property(2, Long.class, "MessageLogId", false, "MESSAGE_LOG_ID");
        public final static Property OtherNumber = new Property(3, String.class, "OtherNumber", false, "OTHER_NUMBER");
        public final static Property Type = new Property(4, String.class, "Type", false, "TYPE");
        public final static Property MessageLength = new Property(5, Long.class, "MessageLength", false, "MESSAGE_LENGTH");
        public final static Property HappenedAt = new Property(6, java.util.Date.class, "happenedAt", false, "HAPPENED_AT");
        public final static Property PopupalertId = new Property(7, Long.class, "PopupalertId", false, "POPUPALERT_ID");
        public final static Property Status = new Property(8, String.class, "Status", false, "STATUS");
    };


    public MessageHistoryDao(DaoConfig config) {
        super(config);
    }
    
    public MessageHistoryDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"MESSAGE_HISTORY\" (" + //
                "\"MESSAGE_RESPONSE_ID\" INTEGER," + // 0: MessageResponseId
                "\"MESSAGE_HISTORY_ID\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 1: MessageHistoryId
                "\"MESSAGE_LOG_ID\" INTEGER," + // 2: MessageLogId
                "\"OTHER_NUMBER\" TEXT," + // 3: OtherNumber
                "\"TYPE\" TEXT," + // 4: Type
                "\"MESSAGE_LENGTH\" INTEGER," + // 5: MessageLength
                "\"HAPPENED_AT\" INTEGER," + // 6: happenedAt
                "\"POPUPALERT_ID\" INTEGER," + // 7: PopupalertId
                "\"STATUS\" TEXT);"); // 8: Status
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"MESSAGE_HISTORY\"";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, MessageHistory entity) {
        stmt.clearBindings();
 
        Long MessageResponseId = entity.getMessageResponseId();
        if (MessageResponseId != null) {
            stmt.bindLong(1, MessageResponseId);
        }
 
        Long MessageHistoryId = entity.getMessageHistoryId();
        if (MessageHistoryId != null) {
            stmt.bindLong(2, MessageHistoryId);
        }
 
        Long MessageLogId = entity.getMessageLogId();
        if (MessageLogId != null) {
            stmt.bindLong(3, MessageLogId);
        }
 
        String OtherNumber = entity.getOtherNumber();
        if (OtherNumber != null) {
            stmt.bindString(4, OtherNumber);
        }
 
        String Type = entity.getType();
        if (Type != null) {
            stmt.bindString(5, Type);
        }
 
        Long MessageLength = entity.getMessageLength();
        if (MessageLength != null) {
            stmt.bindLong(6, MessageLength);
        }
 
        java.util.Date happenedAt = entity.getHappenedAt();
        if (happenedAt != null) {
            stmt.bindLong(7, happenedAt.getTime());
        }
 
        Long PopupalertId = entity.getPopupalertId();
        if (PopupalertId != null) {
            stmt.bindLong(8, PopupalertId);
        }
 
        String Status = entity.getStatus();
        if (Status != null) {
            stmt.bindString(9, Status);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1);
    }    

    /** @inheritdoc */
    @Override
    public MessageHistory readEntity(Cursor cursor, int offset) {
        MessageHistory entity = new MessageHistory( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // MessageResponseId
            cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1), // MessageHistoryId
            cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2), // MessageLogId
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // OtherNumber
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // Type
            cursor.isNull(offset + 5) ? null : cursor.getLong(offset + 5), // MessageLength
            cursor.isNull(offset + 6) ? null : new java.util.Date(cursor.getLong(offset + 6)), // happenedAt
            cursor.isNull(offset + 7) ? null : cursor.getLong(offset + 7), // PopupalertId
            cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8) // Status
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, MessageHistory entity, int offset) {
        entity.setMessageResponseId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setMessageHistoryId(cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1));
        entity.setMessageLogId(cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2));
        entity.setOtherNumber(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setType(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setMessageLength(cursor.isNull(offset + 5) ? null : cursor.getLong(offset + 5));
        entity.setHappenedAt(cursor.isNull(offset + 6) ? null : new java.util.Date(cursor.getLong(offset + 6)));
        entity.setPopupalertId(cursor.isNull(offset + 7) ? null : cursor.getLong(offset + 7));
        entity.setStatus(cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(MessageHistory entity, long rowId) {
        entity.setMessageHistoryId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(MessageHistory entity) {
        if(entity != null) {
            return entity.getMessageHistoryId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
