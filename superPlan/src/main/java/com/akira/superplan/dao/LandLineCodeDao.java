package com.akira.superplan.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * DAO for table "LAND_LINE_CODE".
*/
public class LandLineCodeDao extends AbstractDao<LandLineCode, Long> {

    public static final String TABLENAME = "LAND_LINE_CODE";

    /**
     * Properties of entity LandLineCode.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property CodeID = new Property(0, Long.class, "CodeID", true, "CODE_ID");
        public final static Property StartingCode = new Property(1, String.class, "StartingCode", false, "STARTING_CODE");
        public final static Property TelecomCircleCode = new Property(2, String.class, "TelecomCircleCode", false, "TELECOM_CIRCLE_CODE");
        public final static Property NetworkOperatorCode = new Property(3, String.class, "NetworkOperatorCode", false, "NETWORK_OPERATOR_CODE");
        public final static Property VersionNumber = new Property(4, Long.class, "VersionNumber", false, "VERSION_NUMBER");
    };


    public LandLineCodeDao(DaoConfig config) {
        super(config);
    }
    
    public LandLineCodeDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"LAND_LINE_CODE\" (" + //
                "\"CODE_ID\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: CodeID
                "\"STARTING_CODE\" TEXT," + // 1: StartingCode
                "\"TELECOM_CIRCLE_CODE\" TEXT," + // 2: TelecomCircleCode
                "\"NETWORK_OPERATOR_CODE\" TEXT," + // 3: NetworkOperatorCode
                "\"VERSION_NUMBER\" INTEGER);"); // 4: VersionNumber
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"LAND_LINE_CODE\"";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, LandLineCode entity) {
        stmt.clearBindings();
 
        Long CodeID = entity.getCodeID();
        if (CodeID != null) {
            stmt.bindLong(1, CodeID);
        }
 
        String StartingCode = entity.getStartingCode();
        if (StartingCode != null) {
            stmt.bindString(2, StartingCode);
        }
 
        String TelecomCircleCode = entity.getTelecomCircleCode();
        if (TelecomCircleCode != null) {
            stmt.bindString(3, TelecomCircleCode);
        }
 
        String NetworkOperatorCode = entity.getNetworkOperatorCode();
        if (NetworkOperatorCode != null) {
            stmt.bindString(4, NetworkOperatorCode);
        }
 
        Long VersionNumber = entity.getVersionNumber();
        if (VersionNumber != null) {
            stmt.bindLong(5, VersionNumber);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public LandLineCode readEntity(Cursor cursor, int offset) {
        LandLineCode entity = new LandLineCode( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // CodeID
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // StartingCode
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // TelecomCircleCode
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // NetworkOperatorCode
            cursor.isNull(offset + 4) ? null : cursor.getLong(offset + 4) // VersionNumber
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, LandLineCode entity, int offset) {
        entity.setCodeID(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setStartingCode(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setTelecomCircleCode(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setNetworkOperatorCode(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setVersionNumber(cursor.isNull(offset + 4) ? null : cursor.getLong(offset + 4));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(LandLineCode entity, long rowId) {
        entity.setCodeID(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(LandLineCode entity) {
        if(entity != null) {
            return entity.getCodeID();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
