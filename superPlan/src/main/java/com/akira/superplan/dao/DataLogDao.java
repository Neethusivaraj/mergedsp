package com.akira.superplan.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * DAO for table "DATA_LOG".
*/
public class DataLogDao extends AbstractDao<DataLog, Long> {

    public static final String TABLENAME = "DATA_LOG";

    /**
     * Properties of entity DataLog.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property DataResponseID = new Property(0, Long.class, "DataResponseID", false, "DATA_RESPONSE_ID");
        public final static Property DataLogID = new Property(1, Long.class, "DataLogID", true, "DATA_LOG_ID");
        public final static Property Rx = new Property(2, Long.class, "Rx", false, "RX");
        public final static Property Tx = new Property(3, Long.class, "Tx", false, "TX");
        public final static Property Type = new Property(4, String.class, "Type", false, "TYPE");
        public final static Property HappenedAt = new Property(5, java.util.Date.class, "happenedAt", false, "HAPPENED_AT");
        public final static Property Status = new Property(6, String.class, "Status", false, "STATUS");
    };


    public DataLogDao(DaoConfig config) {
        super(config);
    }
    
    public DataLogDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"DATA_LOG\" (" + //
                "\"DATA_RESPONSE_ID\" INTEGER," + // 0: DataResponseID
                "\"DATA_LOG_ID\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 1: DataLogID
                "\"RX\" INTEGER," + // 2: Rx
                "\"TX\" INTEGER," + // 3: Tx
                "\"TYPE\" TEXT," + // 4: Type
                "\"HAPPENED_AT\" INTEGER," + // 5: happenedAt
                "\"STATUS\" TEXT);"); // 6: Status
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"DATA_LOG\"";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, DataLog entity) {
        stmt.clearBindings();
 
        Long DataResponseID = entity.getDataResponseID();
        if (DataResponseID != null) {
            stmt.bindLong(1, DataResponseID);
        }
 
        Long DataLogID = entity.getDataLogID();
        if (DataLogID != null) {
            stmt.bindLong(2, DataLogID);
        }
 
        Long Rx = entity.getRx();
        if (Rx != null) {
            stmt.bindLong(3, Rx);
        }
 
        Long Tx = entity.getTx();
        if (Tx != null) {
            stmt.bindLong(4, Tx);
        }
 
        String Type = entity.getType();
        if (Type != null) {
            stmt.bindString(5, Type);
        }
 
        java.util.Date happenedAt = entity.getHappenedAt();
        if (happenedAt != null) {
            stmt.bindLong(6, happenedAt.getTime());
        }
 
        String Status = entity.getStatus();
        if (Status != null) {
            stmt.bindString(7, Status);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1);
    }    

    /** @inheritdoc */
    @Override
    public DataLog readEntity(Cursor cursor, int offset) {
        DataLog entity = new DataLog( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // DataResponseID
            cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1), // DataLogID
            cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2), // Rx
            cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3), // Tx
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // Type
            cursor.isNull(offset + 5) ? null : new java.util.Date(cursor.getLong(offset + 5)), // happenedAt
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6) // Status
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, DataLog entity, int offset) {
        entity.setDataResponseID(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setDataLogID(cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1));
        entity.setRx(cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2));
        entity.setTx(cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3));
        entity.setType(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setHappenedAt(cursor.isNull(offset + 5) ? null : new java.util.Date(cursor.getLong(offset + 5)));
        entity.setStatus(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(DataLog entity, long rowId) {
        entity.setDataLogID(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(DataLog entity) {
        if(entity != null) {
            return entity.getDataLogID();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
