package com.akira.superplan;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Window;

import com.akira.superplan.api.ApiMethods;
import com.akira.superplan.dao.CircleDetails;
import com.akira.superplan.dao.Contacts;
import com.akira.superplan.dao.NumberCode;
import com.akira.superplan.dao.OperatorDetails;
import com.akira.superplan.repositories.CircleDetailsRepo;
import com.akira.superplan.repositories.ContactsRepo;
import com.akira.superplan.repositories.NumberCodesRepo;
import com.akira.superplan.repositories.OperatorDetailsRepo;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.Logger;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SplashScreenActivity extends Activity {
	private Handler splashScreenHandler = null;
	private Runnable inboxScreenRunnable = null;
	private String TAG="splashdumping";
	SharedPreferences sp = DaoTestApplication.getSharedPreferences();
	SharedPreferences.Editor editor = sp.edit();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash_screen);
		String dumpNumberCodes = sp.getString("dumpNumberCodes", "true");
		String dumpOperatorDetails = sp.getString("dumpOperatorDetails", "true");
		String dumpCircleDetails = sp.getString("dumpCircleDetails", "true");
		if (dumpNumberCodes.equals("true")) {
			dumpNumberCodes();
		}
		if (dumpOperatorDetails.equals("true")) {
			dumpOperatorDetails();
		}
		if (dumpCircleDetails.equals("true")) {
			dumpCircleDetails();
		}
		dumpNumberCodes();
		dumpOperatorDetails();
		dumpCircleDetails();
		boolean status = DaoTestApplication.getInstance().isAccessibilityEnabled(this,
				"com.akira.superplan/.services.NotificationAccessibilityService");
		Log.d(TAG,"status being updated:"+status);
		//dumpContacts();
		requestLaunchNextActivity();

	}
	private void dumpContacts() {
		String output = " ";

		ContentResolver contentResolver = getApplicationContext().getContentResolver();
		Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		int i = 0;
		List<Contacts> list=new ArrayList<>();
		while (cursor.moveToNext()) {
			String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.
					Contacts.NAME_RAW_CONTACT_ID));
			String name = cursor.getString(cursor.getColumnIndex(ContactsContract.
					Contacts.DISPLAY_NAME));
			String version = null;
			Cursor rawcursor = contentResolver.query(ContactsContract.RawContacts.CONTENT_URI, null,
					ContactsContract.RawContacts._ID + " = ? ", new String[]{contactId}, null);
			while (rawcursor.moveToNext()) {
				version = rawcursor.getString(rawcursor.getColumnIndex(ContactsContract
						.RawContacts.VERSION));
			}
			rawcursor.close();
			Cursor phoneCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.
					CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID
					+ " = ?", new String[]{contactId}, null);
			while (phoneCursor.moveToNext()) {
				String number = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.
						CommonDataKinds.Phone.NUMBER));
				number = number.replaceAll("[^\\x00-\\x7F]", "");
				number = number.replaceAll(" ", "");
				contactId = contactId.replaceAll("[^\\x00-\\x7F]", "");
				if (number != null) {
					Contacts record = new Contacts();
					record.setDeviceContactID(contactId);
					record.setName(name);
					record.setVersion(version);
					record.setNumber(number);
					record.setIsActive(1);
					list.add(record);
					// ContactsRepo.insertOrUpdate(this, record);
					// Log.d("insertcontact", ContactsRepo.getCount(getApplicationContext()) + " "
					// + record.getContact_id() + " " + record.getDevice_contact_id() + " " +
					// record.getName() + " " + record.getVersion() + " " + record.getNumber());
				}
			}
			phoneCursor.close();
		}
		cursor.close();
		ContactsRepo.insertOrUpdateTx(this, list);
	}

	public void dumpNumberCodes() {
		RestAdapter restAdapter = new RestAdapter.Builder()
				.setEndpoint(Constants.API_URL)
				.build();
		ApiMethods methods = restAdapter.create(ApiMethods.class);
		methods.getNumberCodes(new Callback<List<NumberCode>>() {
			@Override
			public void success(List<NumberCode> numberCodes, Response response) {

				NumberCodesRepo.insertOrUpdateNumberList(getApplicationContext(), numberCodes);
				editor.putString("dumpNumberCodes", "false");
				editor.commit();
				Logger.d(TAG, "Number Code : " + numberCodes);
				Log.d("numbercodes", "dumped");
			}

			@Override
			public void failure(RetrofitError error) {
				Log.d("numbercode", " " + error);
			}
		});
	}

	public void dumpOperatorDetails() {
		RestAdapter restAdapter = new RestAdapter.Builder()
				.setEndpoint(Constants.API_URL)
				.build();
		ApiMethods methods = restAdapter.create(ApiMethods.class);
		methods.getOperatorDetails(new Callback<List<OperatorDetails>>() {
			@Override
			public void success(List<OperatorDetails> operatorDetails, Response response) {

				OperatorDetailsRepo.insertOrUpdate(getApplicationContext(), operatorDetails);
				Logger.d(TAG, "operatorDetails : " + "dumped");
				editor.putString("dumpOperatorDetails", "false");
				editor.commit();
				// editor.putString("dumpOperatorDetails", "false");
				// editor.commit();

			}

			@Override
			public void failure(RetrofitError error) {
				Logger.d(TAG, "operator code : " + error);
			}
		});
	}

	public void dumpCircleDetails() {
		RestAdapter restAdapter = new RestAdapter.Builder()
				.setEndpoint(Constants.API_URL)
				.build();
		ApiMethods methods = restAdapter.create(ApiMethods.class);
		methods.getCircleDetails(new Callback<List<CircleDetails>>() {

			@Override
			public void success(List<CircleDetails> circleDetails, Response response) {

				CircleDetailsRepo.insertOrUpdate(getApplicationContext(), circleDetails);
				Logger.d(TAG, "circleDetails : " + "dumped");
				 editor.putString("dumpCircleDetails", "false");
				 editor.commit();
			}

			@Override
			public void failure(RetrofitError error) {

			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	public void onBackPressed() {
		if ((splashScreenHandler != null) && (inboxScreenRunnable != null)) {
			splashScreenHandler.removeCallbacks(inboxScreenRunnable);
		}
		super.onBackPressed();
	}

	/**
	 * Decides which activity needs to be launched depends on user's phone
	 * number registration and profile creation statuses
	 */
	private void requestLaunchNextActivity() {
       // dumpNumberCodes();
		Intent nextActivityIntent = new Intent(this,
				SigninFragmentActivity.class);

		delayedNextActivity(nextActivityIntent, 1500);
	}


	/**
	 * Waits for the given time before starting the next activity.
	 * 
	 * @param nextActivityIntent
	 *            Intent for next activity.
	 * @param delayBy
	 *            Time is ms by which the start of next activity should be
	 *            delayed.
	 */
	private void delayedNextActivity(final Intent nextActivityIntent,
			long delayBy) {
		// Keep the splash screen for few seconds and then move to the next
		// activity by killing the existing activity
		splashScreenHandler = new Handler();
		inboxScreenRunnable = new Runnable() {
			@Override
			public void run() {
				finish();
				try {
					SplashScreenActivity.this.startActivity(nextActivityIntent);
				} catch (ActivityNotFoundException anfe) {
					Logger.log(SplashScreenActivity.class.getSimpleName(), anfe);
				} catch (Exception e) {
					Logger.log(SplashScreenActivity.class.getSimpleName(), e);

				}
			}
		};
		splashScreenHandler.postDelayed(inboxScreenRunnable, 1500);
	}

}