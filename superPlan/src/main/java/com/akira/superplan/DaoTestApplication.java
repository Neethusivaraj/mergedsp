package com.akira.superplan;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.multidex.MultiDex;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;

import com.akira.superplan.api.ApiMethods;
import com.akira.superplan.dao.CallHistory;
import com.akira.superplan.dao.Contacts;
import com.akira.superplan.dao.CountryCodes;
import com.akira.superplan.dao.DaoMaster;
import com.akira.superplan.dao.DaoMaster.DevOpenHelper;
import com.akira.superplan.dao.DaoSession;
import com.akira.superplan.dao.LandLineCode;
import com.akira.superplan.dao.MessageHistory;
import com.akira.superplan.dao.USSDCode;
import com.akira.superplan.repositories.ContactsRepo;
import com.akira.superplan.repositories.CountryCodesRepo;
import com.akira.superplan.repositories.HistoryRepo;
import com.akira.superplan.repositories.LandlineCodesRepo;
import com.akira.superplan.repositories.MessageHistoryRepo;
import com.akira.superplan.repositories.NumberCodesRepo;
import com.akira.superplan.repositories.UssdCodeRepo;
import com.akira.superplan.services.ContentObserverService;
import com.akira.superplan.services.LogDataUsageService;
import com.akira.superplan.services.SendLogsService;
import com.akira.superplan.utils.Connectivity;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class DaoTestApplication extends Application {
    int count=0;
    public DaoSession daoSession;
    private static SharedPreferences sharedPreferences;
    private static DaoTestApplication instance;
    private static final String TAG = "DaoTestApplication";
    private static Context mContext;



    public DaoTestApplication() {
        instance = this;
    }

    public static DaoTestApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setupDatabase();
       // startAllServices();
       // dumpAndSetFlag();
    }

    public static Context getContext(){
        return mContext;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void startPeriodLogService(Context context) {
        Intent intent = new Intent(context, LogDataUsageService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis(),
                Constants.REPEAT_TIME, pendingIntent);
    }

    public void startPeriodSendToServer(Context context) {
        if ( !DaoTestApplication.getInstance()
                .isMyServiceRunning()) {
            Log.d(TAG,"wifi and connection for service");
            Intent intent = new Intent(this, SendLogsService.class);
            PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);
            AlarmManager alarmManager = (AlarmManager)
                    context.getSystemService(context.ALARM_SERVICE);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                    Constants.SERVER_SYNC_TIME, pendingIntent);
        }
    }

    public void dumpAndSetFlag() {
        this.startService(new Intent(this, ContentObserverService.class));


        String k=DaoTestApplication.getSharedPreferences().getString(Constants.IS_DUMPED, "");
        if(!k.equals(Constants.IS_DUMPED) && count==0)
        {
            DumpAllLogs task = new DumpAllLogs();
            task.execute(this);
        }
    }

    private class DumpAllLogs extends AsyncTask {
        @Override
        protected String doInBackground(Object[] params) {

            dumpCountryCodes();
            dumpLandlineCodes();
            dumpCallLogs();
            dumpUssdCode();
            dumpMessageLogs();
            dumpDataUsage();
           //dumpOperatorSenderCodes();
//            dumpOperatorMessageLogs();
            dumpWifiList();

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(Constants.IS_DUMPED,"isDumped");
            editor.commit();

                startPeriodLogService((Context) params[0]);
                startPeriodSendToServer((Context) params[0]);

            return null;
        }
    }

    public String getMyPhoneNumber() {
        return getSharedPreferences().getString(Constants.MY_NUMBER, "Nil");
    }

    public String getMyOperator() {

        return getSharedPreferences().getString(Constants.MY_OPERATOR, "Nil");
    }

    public String getSim1RoamingStatus() {
        String sim1Roaming = "false";
        TelephonyManager telephonyManager = (TelephonyManager) getApplicationContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.isNetworkRoaming()) {
            sim1Roaming = "true";

        }
        return sim1Roaming;
    }

    private void dumpDataUsage() {
        SharedPreferences sp = getSharedPreferences();

        long totalRxBytes = TrafficStats.getTotalRxBytes();
        long totalTxBytes = TrafficStats.getTotalTxBytes();

        SharedPreferences.Editor editor = sp.edit();
        editor.putLong(Constants.TOTAL_RX_BYTES, totalRxBytes);
        Log.d(TAG, "chcking rx_bytes" + totalRxBytes);
        Log.d(TAG,"chcking tx_bytes"+totalRxBytes);
        editor.putLong(Constants.TOTAL_TX_BYTES, totalTxBytes);
        editor.putString(Constants.CONNECTIVITY, Connectivity.getCurrentNetworkStatus(this));
        editor.commit();
    }

    private void dumpMessageLogs() {
        dumpInbox();
        dumpSent();
    }

    private void dumpSent() {
        dumpMessages(Constants.SMS_SENT);
    }

    private void dumpInbox() {
        dumpMessages(Constants.SMS_RECEIVED);
    }

    private void dumpMessages(String type) {
        Uri uri = null;
        if (type.equals(Constants.SMS_RECEIVED)) {
            uri = Uri.parse("content://sms/inbox");
        } else if
                (type.equals(Constants.SMS_SENT)) {
            uri = Uri.parse("content://sms/sent");
        }
        String myNumber = getMyPhoneNumber();
        Cursor managedCursor = getContentResolver().query(uri, null, null, null, null);

        int lSender = managedCursor.getColumnIndex(Telephony.Sms.ADDRESS);
        int lMessage = managedCursor.getColumnIndex(Telephony.Sms.BODY);
        int lHappenedAt = managedCursor.getColumnIndex(Telephony.Sms.DATE);
        int lId = managedCursor.getColumnIndex(Telephony.Sms._ID);
      //  int lSubId = managedCursor.getColumnIndex(Telephony.Sms.SUBSCRIPTION_ID);
        Long messageLogId = 0L;
        List<MessageHistory> list = new ArrayList<>();
        while (managedCursor.moveToNext()) {
            String sender = managedCursor.getString(lSender);
            String message = managedCursor.getString(lMessage);
            String messageDate = managedCursor.getString(lHappenedAt);
            Date happenedAt = new Date(Long.valueOf(messageDate));
            messageLogId = managedCursor.getLong(lId);
            //String sub=managedCursor.getString(sub_id);

            MessageHistory mh = new MessageHistory();
            mh.setMessageResponseId((long) 0);
            mh.setMessageLogId(messageLogId);
            mh.setOtherNumber(myNumber);
            mh.setOtherNumber(sender);


            mh.setType(type);

            mh.setMessageLength(Long.valueOf(message.length()));
            mh.setHappenedAt(happenedAt);
            mh.setStatus(Constants.STATUS_UNSENT);

            list.add(mh);
            //MessageHistoryRepo.insertOrUpdate(this, mh);
        }
        MessageHistoryRepo.insertOrUpdateTx(this,list);
        managedCursor.close();
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putLong(Constants.CURRENT_MESSAGE_LOG_ID, messageLogId);
        editor.commit();

    }

    private void dumpOperatorMessageLogs() {
        dumpOperatorInbox();
        dumpOperatorSent();
    }

    private void dumpOperatorSent() {
        dumpOperatorMessages(Constants.SMS_SENT);
    }

    private void dumpOperatorInbox() {
        dumpOperatorMessages(Constants.SMS_RECEIVED);
    }

    public static boolean stringContainsItemFromList(String inputString) //, String[] items)
    {
        if (inputString.length() >= 10) {
            return false;
        }
        String[] items = {"AT-", "VF", "FONE", "AIR", "AIRTEL", "140", "121", "199", "9848001119",
                "52811", "AX-", "AE-", "ERecharge", "52141", "VT-", "TD-"};
        for (int i = 0; i < items.length; i++) {
            if (inputString.equals(items[i])) {
                return true;
            }
        }
        return false;
    }

    private void dumpOperatorMessages(String type) {
        Uri uri = null;
        if (type.equals(Constants.SMS_RECEIVED)) {
            uri = Uri.parse("content://sms/inbox");
        } else if (type.equals(Constants.SMS_SENT)) {
            uri = Uri.parse("content://sms/sent");
        }

        String myNumber = getMyPhoneNumber();
        Cursor managedCursor = getContentResolver().query(uri, null, null, null, null);
        //getCallType(myNumber);
        int lSender = managedCursor.getColumnIndex(Telephony.Sms.ADDRESS);
        int lMessage = managedCursor.getColumnIndex(Telephony.Sms.BODY);
        int lHappenedAt = managedCursor.getColumnIndex(Telephony.Sms.DATE);
        int lId = managedCursor.getColumnIndex(Telephony.Sms._ID);

        Long messageLogId = 0L;

        while (managedCursor.moveToNext()) {
            String sender = managedCursor.getString(lSender);
            String message = managedCursor.getString(lMessage);
            String messageDate = managedCursor.getString(lHappenedAt);
            Date happenedAt = new Date(Long.valueOf(messageDate));
            messageLogId = managedCursor.getLong(lId);


    /*       if (stringContainsItemFromList(sender)) {
                OperatorMessageHistory mh = new OperatorMessageHistory();
                mh.setMessageLogId(messageLogId);
                mh.setMyNumber(myNumber);
                mh.setMessageText(message);
                mh.setOtherNumber(sender);
                mh.setType(type);
                mh.setMessageLength(Long.valueOf(message.length()));
                mh.setHappenedAt(happenedAt);
                mh.setStatus(Constants.STATUS_UNSENT);
                OperatorMessageHistoryRepo.insertOrUpdate(this, mh);
            } */
        }
        count++;
        managedCursor.close();
    }

    public void dumpCallLogs() {
        String myNumber = getMyPhoneNumber();

        String strOrder = CallLog.Calls._ID;
        Cursor managedCursor = getContentResolver().query(CallLog.Calls.CONTENT_URI, null,
                null, null, strOrder);

        int lNumber = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int lType = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int lDate = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int lDuration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        int lId = managedCursor.getColumnIndex(CallLog.Calls._ID);
        int lName = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        long lLogId = 0L;
        int lAcId = managedCursor.getColumnIndex(CallLog.Calls.PHONE_ACCOUNT_ID);
        List<CallHistory> list = new ArrayList<>();
        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(lNumber);
            String contactName = managedCursor.getString(lName);
            String callType = managedCursor.getString(lType);
            String callDate = managedCursor.getString(lDate);
            //callDate="2015-11-18T16:46:10.000Z";
            Date callDayTime = new Date(Long.valueOf(callDate));
            int callDuration = managedCursor.getInt(lDuration);
            lLogId = managedCursor.getLong(lId);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            // String account=managedCursor.getString(ac_id);
            // Log.d("calltype",dircode+" "+contactName+" "+phNumber);
            if (dircode != 10) {
                switch (dircode) {
                    case CallLog.Calls.OUTGOING_TYPE:
                        dir = Constants.OUTGOING_CALL;
                        break;

                    case CallLog.Calls.INCOMING_TYPE:
                        dir = Constants.RECEIVED_CALL;
                        break;

                    case CallLog.Calls.MISSED_TYPE:
                        dir = Constants.MISSED_CALL;
                        break;
                    case 5:
                        dir = Constants.RECEIVED_CALL;
                        break;
                    case 6:
                        dir = Constants.BLOCKED_CALL;
                        break;
                    default:
                        break;
                }

                String category = getCallCategory(phNumber);
                CallHistory record = new CallHistory();
                record.setCallHistoryResponseId((long) 0);
                record.setOtherNumber(phNumber);
              //  record.setName(getName(phNumber));
                record.setType(dir);
                record.setHappenedAt(callDayTime);
                record.setSeconds(callDuration);
                record.setStatus(Constants.STATUS_UNSENT);
               // record.setMyNumber(myNumber);
                record.setLogId(lLogId);
                record.setSim1Roaming("nil");
                record.setSim2Roaming("nil");
             //   record.setPopupText("nil");
                record.setCallCategory(category);
                record.setIsInNetwork(checkIsInNetwork(phNumber, category));

                list.add(record);
                // Logger.d("operator", checkIsInNetwork(phNumber, category));
                // Logger.d("operator", record.getIsInNetwork());
                //record.setCallType(finder.getCallType());
                //HistoryRepo.insertOrUpdate(this, record);
            }
        }
        HistoryRepo.insertOrUpdateTx(this,list);
        managedCursor.close();
        // save the recent logid
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putLong(Constants.CURRENT_CALL_LOG_ID, lLogId);
        editor.commit();
        count++;
    }

    public String checkIsInNetwork(String number, String category) {
        Logger.d(TAG, " " + number);
        Logger.d(TAG, " " + category);
        if (category != null) {
            if (category.equals("invalid")) {
                return "false";
            } else {
                number = number.replace("+91", "");
                if (number.startsWith("0"))
                    number = number.replaceFirst("0", "");
                String operator = NumberCodesRepo.getNetworkOperator(
                        getApplicationContext(), number.substring(0, 4));
                String myOperator = DaoTestApplication.getSharedPreferences().
                        getString("myOperatorCode", "");
                if (operator.equals(myOperator)) {
                    return "true";
                } else {
                    return "false";
                }
            }
        } else {
            return "false";
        }

    }

    public String getCallCategory(String number) {
        String category = null;
        String myCircle = DaoTestApplication.getSharedPreferences().getString("myCircleCode", "");
        if (number.startsWith("+91") || number.startsWith("0") || number.length() == 10) {
            number = number.replace("+91", "");
            if (number.startsWith("0")) {
                number = number.replaceFirst("0", "");
            }
            if(number.length() >= 4) {
                String code = number.substring(0, 4);
                String otherCircle = NumberCodesRepo.getNumberCircle(getApplicationContext(), code);
//                write comment here
                if ((otherCircle.equals("CH") && myCircle.equals("TN")) ||
                        (otherCircle.equals("TN") && myCircle.equals("CH"))) {
                    category = "local";
                } else if (otherCircle.equals(myCircle)) {
                    category = "local";
                } else {
                    category = "STD";
                }
                if (otherCircle.equals("invalid")) {
                    String circle = LandlineCodesRepo.getMobileCircle(getApplicationContext(), number);

                    if (circle.equals(myCircle)) {
                        category = "local landline";
                    } else if (circle.equals("invalid")) {
                        category = "invalid";
                    } else {
                        category = "STD landline";
                    }
                }
            }
            else
            {
                category = "invalid";
            }
        } else if (number.startsWith("+")) {
            String country = CountryCodesRepo.getCountryDetail(getApplicationContext(), number);
            if (country.equals("invalid")) {
                category = "invalid";
            } else {
                category = "ISD";
            }
        } else {
            category = "invalid";
        }
        return category;
    }

    private void dumpContacts() {
        String output = " ";

        ContentResolver contentResolver = getApplicationContext().getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        int i = 0;
        List<Contacts> list=new ArrayList<>();
        while (cursor.moveToNext()) {
            String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.
                    Contacts.NAME_RAW_CONTACT_ID));
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.
                    Contacts.DISPLAY_NAME));
            String version = null;
            Cursor rawcursor = contentResolver.query(ContactsContract.RawContacts.CONTENT_URI, null,
                    ContactsContract.RawContacts._ID + " = ? ", new String[]{contactId}, null);
            while (rawcursor.moveToNext()) {
                version = rawcursor.getString(rawcursor.getColumnIndex(ContactsContract
                        .RawContacts.VERSION));
            }
            rawcursor.close();
            Cursor phoneCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.
                    CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID
                    + " = ?", new String[]{contactId}, null);
            while (phoneCursor.moveToNext()) {
                String number = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.
                        CommonDataKinds.Phone.NUMBER));
                number = number.replaceAll("[^\\x00-\\x7F]", "");
                number = number.replaceAll(" ", "");
                contactId = contactId.replaceAll("[^\\x00-\\x7F]", "");
                if (number != null) {
                    Contacts record = new Contacts();
                    record.setDeviceContactID(contactId);
                    record.setName(name);
                    record.setVersion(version);
                    record.setNumber(number);
                    record.setIsActive(1);
                    list.add(record);
                  //  ContactsRepo.insertOrUpdate(this, record);
                   // Log.d("inserting contact", ContactsRepo.getCount(getApplicationContext()) + " "
                       //     + record.getContactId() + " " + record.getDeviceContactID() + " ");
                    // record.getName() + " " + record.getVersion() + " " + record.getNumber());
                }
            }
            phoneCursor.close();
        }
        count++;
        cursor.close();
        ContactsRepo.insertOrUpdateTx(this, list);
    }

    public void dumpUssdCode(){

        Logger.d(TAG,"---*dumpUssdCode is called*----");

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_URL)
                .build();
        ApiMethods methods = restAdapter.create(ApiMethods.class);
        methods.getUssdCodes(new Callback<List<USSDCode>>() {

            @Override
            public void success(List<USSDCode> ussdCodes, Response response) {

                UssdCodeRepo.insertOrUpdate(getApplicationContext(), ussdCodes);
                Logger.d(TAG, "USSD Codes : " + "dumped");
                SharedPreferences sp = DaoTestApplication.getSharedPreferences();
                SharedPreferences.Editor editor = sp.edit();

                editor.putString("dumpUssdCodes", "false");
                editor.commit();
                count++;

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

        Logger.d(TAG,"---*Dump UssdCode is done*---");

    }

//    public void dumpUssdCodes() {
//        ussd_codes codes = new ussd_codes();
//        codes.setOperatorcode("AIRCEL");
//        codes.setCallBalance("*125");
//        codes.setBalance_2g("*301");
//        codes.setBalance_3g("*301");
//        UssdCodeRepo.insertOrUpdate(getApplicationContext(), codes);
//
//        ussd_codes codes1 = new ussd_codes();
//        codes1.setOperatorcode("AIRTEL");
//        codes1.setCallBalance("*123");
//        codes1.setBalance_2g("*123*10");
//        codes1.setBalance_3g("*123*11");
//        UssdCodeRepo.insertOrUpdate(getApplicationContext(), codes1);
//
//        ussd_codes codes2 = new ussd_codes();
//        codes2.setOperatorcode("BSNL");
//        codes2.setCallBalance("*123");
//        codes2.setBalance_2g("*124");
//        codes2.setBalance_3g("*124");
//        UssdCodeRepo.insertOrUpdate(getApplicationContext(), codes2);
//
//        ussd_codes codes3 = new ussd_codes();
//        codes3.setOperatorcode("IDEA");
//        codes3.setCallBalance("*121");
//        codes3.setBalance_2g("*125");
//        codes3.setBalance_3g("*125");
//        UssdCodeRepo.insertOrUpdate(getApplicationContext(), codes3);
//
//        ussd_codes codes4 = new ussd_codes();
//        codes4.setOperatorcode("TATA DOCOMO");
//        codes4.setCallBalance("*111");
//        codes4.setBalance_2g("*111*1");
//        codes4.setBalance_3g("*111*1");
//        UssdCodeRepo.insertOrUpdate(getApplicationContext(), codes4);
//
//        ussd_codes codes5 = new ussd_codes();
//        codes5.setOperatorcode("VODAFONE INDIA");
//        codes5.setCallBalance("*141*2");
//        codes5.setBalance_2g("*111*6*2");
//        codes5.setBalance_3g("*111*6*2");
//        UssdCodeRepo.insertOrUpdate(getApplicationContext(), codes5);
//
//        ussd_codes codes6 = new ussd_codes();
//        codes6.setOperatorcode("RELIANCE");
//        codes6.setCallBalance("*367");
//        //codes6.setBalance_2g("*111*6*2");
//        //codes6.setBalance_3g("*111*6*2");
//        UssdCodeRepo.insertOrUpdate(getApplicationContext(), codes6);
//
//    }

    public void dumpWifiList() {
        if (Connectivity.isConnected(getApplicationContext())) {
            NetworkInfo networkInfo = Connectivity.getNetworkInfo(getApplicationContext());
            if (Connectivity.isConnectedWifi(getApplicationContext())) {
                count++;
                Logger.d(TAG, "network info" + networkInfo.toString());
                Connectivity.getWifiInfo(getApplicationContext());

            }
        }

    }

    public String getName(String number) {
        Log.d(TAG,"getting the name for :"+number);
        String name = number;
        Contacts contacts = new Contacts();
        contacts = ContactsRepo.getName(getApplicationContext(), number);
        if (contacts.getIsActive() != 0) {
            name = contacts.getName();
        }
        return name;
    }


    private void setupDatabase() {
        DevOpenHelper helper = new DevOpenHelper(this, Constants.SQLITE_DB_NAME, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
       // DaoMaster.dropAllTables(db,true);
        DaoMaster.createAllTables(db, true);
    }

    public void setPhoneNumber(String phNumber) {

        SharedPreferences sp = getSharedPreferences();
        SharedPreferences.Editor editor = sp.edit();

        editor.putString(Constants.MY_NUMBER, phNumber);
        editor.putBoolean(Constants.MY_NUMBER_FLAG, true);
        editor.commit();
    }

    public void setOperatorName(String name) {
        SharedPreferences sp = getSharedPreferences();
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(Constants.MY_OPERATOR, name);
        editor.commit();
    }

    private void startAllServices() {
        if (getSharedPreferences().getBoolean(Constants.MY_NUMBER_FLAG, false)) {
            this.startService(new Intent(this, ContentObserverService.class));
            startPeriodSendToServer(this);
        }
    }

    public boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            Log.i("Service name", service.service.getClassName().toString());
            Log.i("Service name - ", service.service.getClassName());
            if ("com.akira.superplan.services.SendCallLogService".
                    equals(service.service.getClassName().toString())) {
                Log.d(TAG,"services runningggggggggg: ");
                return true;
            }
        }
        return false;
    }

    public static boolean isAccessibilityEnabled(Context context, String id) {

        AccessibilityManager am = (AccessibilityManager) context
                .getSystemService(Context.ACCESSIBILITY_SERVICE);

        List<AccessibilityServiceInfo> runningServices = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            runningServices = am
                    .getEnabledAccessibilityServiceList(AccessibilityEvent.TYPES_ALL_MASK);

            for (AccessibilityServiceInfo service : runningServices) {
                Log.i("Access service name", service.getId());
                if (id.equals(service.getId())) {
                    return true;
                }
            }
            }
            return false;
        }


    public static synchronized SharedPreferences getSharedPreferences() {
        if (sharedPreferences == null) {
            sharedPreferences = DaoTestApplication.getInstance().getApplicationContext()
                    .getSharedPreferences(Constants.SHARED_PREFS_KEY, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public void dumpLandlineCodes() {
//        final OkHttpClient okHttpClient = new OkHttpClient();
//        okHttpClient.setReadTimeout(300, TimeUnit.SECONDS);
//        okHttpClient.setConnectTimeout(300, TimeUnit.SECONDS);
//        RestAdapter restAdapter = new RestAdapter.Builder()
//                .setEndpoint(Constants.API_URL)
//                        // .setErrorHandler(new RetrofitErrorHandler())
//                .setClient(new OkClient(okHttpClient))
//                .setLogLevel(RestAdapter.LogLevel.FULL)
//                .build();
//        ApiMethods methods2 = restAdapter.create(ApiMethods.class);
//
//
//        methods2.getLandlineCodes(new Callback<List<LandlineCodes>>() {
//            @Override
//            public void success(List<LandlineCodes> landlineCodes, Response response) {
//
////
//
//                Logger.d(TAG, "landlineCodes");
//
//                LandlineCodesRepo.insertOrUpdateTx(getApplicationContext(), landlineCodes);
//
//                Logger.d(TAG, "landlineCodes After insertion : "+landlineCodes);
//
//                Logger.d(TAG, "landlineCodes" + LandlineCodesRepo.getCount(getApplicationContext()));
//
//
//                SharedPreferences sp = DaoTestApplication.getSharedPreferences();
//                SharedPreferences.Editor editor = sp.edit();
//                editor.putString("landline", "success");
//                editor.commit();
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Log.d("landline", " error :" + error);
//                SharedPreferences sp = DaoTestApplication.getSharedPreferences();
//                SharedPreferences.Editor editor = sp.edit();
//                editor.putString("landline", "failure");
//                editor.commit();
//            }
//        });

        InputStream inputStream = getApplicationContext().getResources().openRawResource(R.raw.landline);
        InputStreamReader isr =new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(isr);
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            bufferedReader.readLine();
            line = bufferedReader.readLine();
            List<LandLineCode> list = new ArrayList<>();
            while ((line  != null)) {
                if((!line.isEmpty())) {
                    String[] country = line.split(",");
                   // Logger.d(TAG, country[0] + " " + country[1] + " " + country[2] + " " + country[3] + " " + country[4]);
                    LandLineCode codes=new LandLineCode();
                    codes.setStartingCode(country[0]);

                    codes.setTelecomCircleCode(country[2]);

                    codes.setNetworkOperatorCode(country[4]);
                    list.add(codes);
                    line = bufferedReader.readLine();
                }
            }
            LandlineCodesRepo.insertOrUpdateTx(getApplicationContext(),list);
            Logger.d(TAG, LandlineCodesRepo.getCount(getApplicationContext()) + " ");
            bufferedReader.close();
            count++;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void dumpCountryCodes() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_URL)
                .build();
        ApiMethods methods2 = restAdapter.create(ApiMethods.class);
        methods2.getCountryCodes(new Callback<List<CountryCodes>>() {
            @Override
            public void success(List<CountryCodes> countryCodes, Response response) {

                        CountryCodesRepo.insertOrUpdate(getApplicationContext(), countryCodes);
                count++;

            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("countrycode", " " + error);
            }
        });
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}

