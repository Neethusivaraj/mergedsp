package com.akira.superplan.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.api.HerokuApi;
import com.akira.superplan.dao.CallHistory;
import com.akira.superplan.dao.DataLog;
import com.akira.superplan.dao.MessageHistory;
import com.akira.superplan.dao.PopupAlerts;
import com.akira.superplan.dao.UserBalance;
import com.akira.superplan.dao.WifiList;
import com.akira.superplan.repositories.DataLogRepo;
import com.akira.superplan.repositories.HistoryRepo;
import com.akira.superplan.repositories.MessageHistoryRepo;
import com.akira.superplan.repositories.PopupAlertsRepo;
import com.akira.superplan.repositories.UserBalanceRepo;
import com.akira.superplan.repositories.WifiListRepo;
import com.akira.superplan.utils.ConnectionDetector;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.Logger;

import java.util.Date;
import java.util.List;

public class SendLogsService extends IntentService {

    HerokuApi herokuApi;
    Context aContext;
    private String TAG="SENDING IT TO SERVER ";

    public SendLogsService() {
        super("SendLogsService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            aContext = DaoTestApplication.getInstance().getApplicationContext();
            herokuApi = new HerokuApi();
            try{
                Date timeNow = new Date(System.currentTimeMillis());
                SharedPreferences.Editor editor = DaoTestApplication.getSharedPreferences().edit();
                editor.putString(Constants.LAST_SERVER_SYNC_STATUS, "In progress");
                editor.putString(Constants.LAST_SERVER_SYNC_TIME, timeNow.toString());
                editor.commit();
            try {
                List<CallHistory> histories = HistoryRepo.getUnsentHistories(aContext);
                herokuApi.putCallHistory(histories,aContext);
               /* for (int i = 0; i < histories.size(); i += Constants.SERVER_SEND_LIMIT) {
                    List<CallHistory> hist = histories.subList(i, Math.min(
                            histories.size(), i + Constants.SERVER_SEND_LIMIT));
                    Log.d(TAG, "checking your call log data:" + hist);
                    try {

                        herokuApi.putCallHistory(hist, aContext);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Logger.d("size", hist.size() + " ");
                }
                Logger.d("order", "calls Sent"); */
            }
            catch (Exception e) {
                Logger.d(TAG, " call error " + e.getCause());

                Logger.d(" call error"," "+ e.getLocalizedMessage());
            }

            try {
                List<MessageHistory> messageHistories = MessageHistoryRepo.getUnsentHistories(aContext);
                for (int i = 0; i < messageHistories.size(); i += Constants.SERVER_SEND_LIMIT) {
                    List<MessageHistory> hist = messageHistories.subList(
                            i, Math.min(messageHistories.size(), i + Constants.SERVER_SEND_LIMIT));
                    try {
                        herokuApi.putMessageHistory(hist, aContext);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Logger.d("size", hist.size() + " ");
                }
                Logger.d("order", "Messages Sent");
            }
            catch (Exception e) {
                Logger.d("message error"," "+ e.getLocalizedMessage());
            }
            try {
                List<DataLog> dataLogs = DataLogRepo.getUnsentHistories(aContext);
                for (int i = 0; i < dataLogs.size(); i += Constants.SERVER_SEND_LIMIT) {
                    List<DataLog> subList = dataLogs.subList(
                            i, Math.min(dataLogs.size(), i + Constants.SERVER_SEND_LIMIT));
                    try {
                        herokuApi.putDataLog(subList, aContext);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Logger.d("size", subList.size() + " ");
                }
                Logger.d("order", "Datalogs Sent");
            }
            catch (Exception e) {
                Logger.d("data error"," "+ e.getLocalizedMessage());
            }
                try {
                    List<PopupAlerts> popupAlerts = PopupAlertsRepo.getUnsentHistories(aContext);
                    for (int i = 0; i < popupAlerts.size(); i += Constants.SERVER_SEND_LIMIT) {
                        List<PopupAlerts> subList = popupAlerts.subList(
                                i, Math.min(popupAlerts.size(), i + Constants.SERVER_SEND_LIMIT));
                        try {
                            herokuApi.putPopupHistory(subList, aContext);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Logger.d("size", subList.size() + " ");
                    }
                    Logger.d("order", "popupAlerts Sent");
                }
                catch (Exception e) {
                    Logger.d("popup error"," "+ e.getLocalizedMessage());
                }
                try {
                    List<UserBalance> userBalanceList = UserBalanceRepo.getUnsentHistories(aContext);
                    for (int i = 0; i < userBalanceList.size(); i += Constants.SERVER_SEND_LIMIT) {
                        List<UserBalance> subList = userBalanceList.subList(
                                i, Math.min(userBalanceList.size(), i + Constants.SERVER_SEND_LIMIT));
                        try {
                            herokuApi.putUserBalance(subList, aContext);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Logger.d("size", subList.size() + " ");
                    }
                    Logger.d("order", "userBalance Sent");
                }
                catch (Exception e) {
                    Logger.d("user balance error"," "+ e.getLocalizedMessage());
                }
                try {
                    List<WifiList> wifiList = WifiListRepo.getUnsentWifi(aContext);
                    for (int i = 0; i < wifiList.size(); i += Constants.SERVER_SEND_LIMIT) {
                        List<WifiList> subList = wifiList.subList(
                                i, Math.min(wifiList.size(), i + Constants.SERVER_SEND_LIMIT));
                        try {
                            herokuApi.putWifiList(subList, aContext);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Logger.d("size", subList.size() + " ");
                    }
                    Logger.d("order", "wifiList Sent");
                }
                catch (Exception e) {
                    Logger.d("wifi error"," "+ e.getLocalizedMessage());
                }
           /* try {
                List<OperatorMessageHistory> OperatorMessageList = OperatorMessageHistoryRepo.
                        getUnsentHistories(aContext);
                for (int i = 0; i < OperatorMessageList.size(); i += Constants.SERVER_SEND_LIMIT) {
                    List<OperatorMessageHistory> subList = OperatorMessageList.subList(
                            i, Math.min(OperatorMessageList.size(), i + Constants.SERVER_SEND_LIMIT));
                    try {
                        herokuApi.putOperatorMessage(subList, aContext);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Logger.d("size", subList.size() + " ");
                }
                Logger.d("order", "OperatorMessage Sent");
            }
            catch (Exception e) {
                Logger.d("operator error"," "+ e.getLocalizedMessage());
            } */
                timeNow = new Date(System.currentTimeMillis());
                editor.putString(Constants.LAST_SERVER_SYNC_STATUS, "Success");
                editor.putString(Constants.LAST_SERVER_SYNC_TIME, timeNow.toString());
                editor.commit();
            } catch (Exception e) {
                Date timeNow1 = new Date(System.currentTimeMillis());
                SharedPreferences.Editor editor1 = DaoTestApplication.getSharedPreferences().edit();
                editor1.putString(Constants.LAST_SERVER_SYNC_STATUS, "Failed- "
                        + e.getLocalizedMessage());
                Logger.d("error"," "+ e.getLocalizedMessage());
                editor1.putString(Constants.LAST_SERVER_SYNC_TIME, timeNow1.toString());
                editor1.commit();
            } finally {
//                ma.clientLogout();
            }
        }
    }





}
