package com.akira.superplan.services;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.view.accessibility.AccessibilityEvent;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.CallHistory;
import com.akira.superplan.dao.MessageHistory;
import com.akira.superplan.dao.PopupAlerts;
import com.akira.superplan.dao.UserBalance;
import com.akira.superplan.fragments.StatusAct;
import com.akira.superplan.repositories.HistoryRepo;
import com.akira.superplan.repositories.MessageHistoryRepo;
import com.akira.superplan.repositories.PopupAlertsRepo;
import com.akira.superplan.repositories.UserBalanceRepo;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.Logger;

import java.util.Date;
import java.util.List;

/**
 * Created by nanda on 9/7/15.
 */
public class NotificationAccessibilityService extends AccessibilityService {

    private static final String TAG = "NotificationAccessibilityService";
    SharedPreferences sp = DaoTestApplication.getSharedPreferences();
    SharedPreferences.Editor spEdit = sp.edit();

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (event.getClassName() != null && event.getClassName().toString().equalsIgnoreCase(
                "android.app.AlertDialog") && event.getPackageName() != null
                && event.getPackageName().toString().equalsIgnoreCase("com.android.phone")) {

            Logger.d(TAG,"source info"+ " " + event.getClassName().toString());


            Logger.d(TAG, String.format("onAccessibilityEvent: [type] %s [class] %s [package] %s [time] %s [text] %s",
                    getEventType(event), event.getClassName(), event.getPackageName(),
                    event.getEventTime(), getEventText(event)));
            Date timeNow = new Date();
            String popupText = event.getText().toString();

            long logInsertedAt = sp.getLong(Constants.LOG_INSERTED_AT, 0L);
            long logSeconds = Math.abs((new Date(logInsertedAt).getTime()
                    - timeNow.getTime()) / 1000);


            spEdit.putLong(Constants.POPUP_INSERTED_AT, System.currentTimeMillis());
            spEdit.commit();


            PopupAlerts popupRecord = new PopupAlerts();
        //    popupRecord.setMyNumber(DaoTestApplication.getInstance().getMyPhoneNumber());
            popupRecord.setHappenedAt(timeNow);
            popupRecord.setStatus(Constants.STATUS_UNSENT);
            popupRecord.setPopupText(popupText);

//            if(popupRecord.getCallCost() != null || popupRecord.getCallDuration() != null ||
//                    popupRecord.getCurrentBalance() != null || popupRecord.getSmsCost()!= null ||
//                    popupRecord.getDataCost() != null || popupRecord.getDataUsage() != null ||
//                    popupRecord.getDataBalance() != null || popupRecord.getPromoBalance() != null ||
//                    popupRecord.getMobilePlan() != null) {

            Logger.d("textpop", "call cost: " + popupRecord.getCallCost() + " duration: " + popupRecord.getCallDuration() +
                    " curBalance: " + popupRecord.getCurrentBalance() + " mobileplan " + popupRecord.getMobilePlan() +
                    " smsCost: " + popupRecord.getSmsCost() + " datacost " + popupRecord.getDataCost() + " dataUsage " +
                    popupRecord.getDataUsage() + " databalance" + popupRecord.getDataBalance() + "promobalance" +
                    popupRecord.getPromoBalance());
            PopupAlertsRepo.insertOrUpdate(getApplicationContext(), popupRecord);

            Long lAlertId = popupRecord.getPopupAlterId();

            if (logSeconds < Constants.USSD_DELAY) {
                insertPopupIntoCallLog(getApplicationContext(), popupText, lAlertId);
                insertAlertIdIntoMessageHistory(getApplicationContext(),lAlertId);
            }
            Logger.d(TAG, "Newly Added Alert Id :" + popupRecord.getPopupAlterId());
         //   Logger.d(TAG, "Newly Added PopupText : " + popupRecord.getPopupText());


            Logger.d("popup", popupText);
            long ussdCalledAt = sp.getLong(Constants.USSD_CALLED_AT, 0L);
            long currentUssd = Math.abs((new Date(ussdCalledAt).getTime()
                    - timeNow.getTime()) / 1000);
            if (currentUssd < 30L) {
                //Logger.d(TAG, popupText);
                UserBalance updateBalance = new UserBalance();
                updateBalance.setUserBalanceResponseId((long) 0);
                updateBalance.setCurrentBalance(popupText);
                updateBalance.setLastUpdated(timeNow);
                if (updateBalance.getCurrentBalance() != null || updateBalance.getCallValidity() != null
                        || updateBalance.getDataBalance() != null || updateBalance.getDataValidity() != null) {
                    UserBalanceRepo.insertOrUpdate(getApplicationContext(), updateBalance);
                    List<UserBalance> list = UserBalanceRepo.getAllBalance(getApplicationContext());
                    for (UserBalance balance : list) {
                        Logger.d(TAG, "current balance " + balance.getCurrentBalance() +
                                "callValidity" + balance.getCallValidity() + " time "
                                + balance.getLastUpdated() + " data " + balance.getDataBalance()
                                + "data validity " + balance.getDataValidity());
                    }
                }

            }

        } else {
            return;
        }

    }

    private void insertAlertIdIntoMessageHistory(Context context, Long lalertId){
        MessageHistory messageHistory = MessageHistoryRepo.getLastLog(context);
        messageHistory.setPopupalertId(lalertId);
        MessageHistoryRepo.insertOrUpdate(context,messageHistory);
        if(messageHistory.getType() == "Outgoing" ){
            spEdit.putInt(Constants.MISSED_USSD_COUNT,sp.getInt(Constants.MISSED_USSD_COUNT,0)+1);
        }
    }

    private void insertPopupIntoCallLog(Context context, String popupText, Long lalertId) {


        CallHistory history = HistoryRepo.getLastLog(context);
       // history.setPopupText(popupText);
        history.setPopupalertId(lalertId);
        HistoryRepo.insertOrUpdate(context, history);

        if (history.getType() == "Outgoing" && history.getSeconds() > 0 && popupText == null) {
            spEdit.putInt(Constants.MISSED_USSD_COUNT,
                    sp.getInt(Constants.MISSED_USSD_COUNT, 0) + 1);
        }


//        CallHistoryBC callHistoryBC = CallHistoryBCRepo.getLastLog(context);
//        callHistoryBC.setPopupText(popupText);
//        CallHistoryBCRepo.insertOrUpdate(context, callHistoryBC);
    }

    @Override
    public boolean bindService(Intent service, ServiceConnection conn, int flags) {
        return super.bindService(service, conn, flags);
    }

    @Override
    public void onInterrupt() {
    }

    @Override
    public void onServiceConnected() {
        Logger.v(TAG, "*****  onServiceConnected  ******");

        Intent iLaunch = new Intent(NotificationAccessibilityService.this, StatusAct.class);
        iLaunch.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(iLaunch);

        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED;

        info.feedbackType = AccessibilityEvent.TYPES_ALL_MASK;
        setServiceInfo(info);

    }


    private String getEventType(AccessibilityEvent event) {
        switch (event.getEventType()) {
            case AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED:
                return "TYPE_NOTIFICATION_STATE_CHANGED";
            case AccessibilityEvent.TYPE_VIEW_CLICKED:
                return "TYPE_VIEW_CLICKED";
            case AccessibilityEvent.TYPE_VIEW_FOCUSED:
                return "TYPE_VIEW_FOCUSED";
            case AccessibilityEvent.TYPE_VIEW_LONG_CLICKED:
                return "TYPE_VIEW_LONG_CLICKED";
            case AccessibilityEvent.TYPE_VIEW_SELECTED:
                return "TYPE_VIEW_SELECTED";
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                return "TYPE_WINDOW_STATE_CHANGED";
            case AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED:
                return "TYPE_VIEW_TEXT_CHANGED";
        }
        return "default";
    }

    private String getEventText(AccessibilityEvent event) {
        StringBuilder sb = new StringBuilder();
        for (CharSequence s : event.getText()) {
            sb.append(s);
        }
        return sb.toString();
    }
}
