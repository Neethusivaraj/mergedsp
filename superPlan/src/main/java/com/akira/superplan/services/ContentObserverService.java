package com.akira.superplan.services;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.provider.CallLog;
import android.provider.ContactsContract;

import com.akira.superplan.listeners.CallLogObserver;
import com.akira.superplan.listeners.ContactsObserver;
import com.akira.superplan.listeners.MessageLogObserver;


public class ContentObserverService extends Service {

//    public ContentObserverService() {
//        super("ContentObserverService");
//    }

    @Override
//    public void onHandleIntent(Intent intent) {
    public void onCreate() {

        MessageLogObserver msgObserver = new MessageLogObserver(new Handler(), this);
        Uri msgUri = Uri.parse("content://sms/");
        this.getContentResolver().registerContentObserver(msgUri, true, msgObserver);

        CallLogObserver callObserver = new CallLogObserver(new Handler(), this);
        this.getContentResolver().registerContentObserver(CallLog.
                Calls.CONTENT_URI, true, callObserver);
        ContactsObserver contactsObserver = new ContactsObserver(new Handler(), this);
        this.getContentResolver().registerContentObserver(ContactsContract.
                Contacts.CONTENT_URI, true, contactsObserver);
        super.onCreate();

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
