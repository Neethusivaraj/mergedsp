package com.akira.superplan.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.TrafficStats;
import android.os.IBinder;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.DataLog;
import com.akira.superplan.repositories.DataLogRepo;
import com.akira.superplan.utils.Connectivity;
import com.akira.superplan.utils.Constants;

import java.util.Date;


public class LogDataUsageService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        logDataUsage(this);
    }

    private void logDataUsage
            (Context context) {
        if (Connectivity.isConnected(context)) {
            SharedPreferences sp = DaoTestApplication.getSharedPreferences();
            SharedPreferences.Editor editor = sp.edit();

            long defaultValue = 0L;
            long spTotalRx = sp.getLong(Constants.TOTAL_RX_BYTES, defaultValue);
            long spTotalTx = sp.getLong(Constants.TOTAL_TX_BYTES, defaultValue);
            String currentStatus = sp.getString(Constants.CONNECTIVITY, "");
            String type = currentStatus.split("_")[0];
            long totalRxBytes = TrafficStats.getTotalRxBytes();
            long totalTxBytes = TrafficStats.getTotalTxBytes();
            editor.putLong(Constants.TOTAL_RX_BYTES, totalRxBytes);
            editor.putLong(Constants.TOTAL_TX_BYTES, totalTxBytes);
            editor.commit();

            if (type.equalsIgnoreCase("mobile") || type.equalsIgnoreCase("mobile_hipri")
                    || type.equalsIgnoreCase("mobile_supl") || type.equalsIgnoreCase("mobile_mms")
                    || type.equalsIgnoreCase("mobile_dun")) {
                type = "MOBILE";

                long rx = totalRxBytes - spTotalRx;
                long tx = totalTxBytes - spTotalTx;
                Date happenedAt = new Date(System.currentTimeMillis());
                DataLog dataLog = new DataLog();
                dataLog.setDataResponseID((long) 0);
                dataLog.setRx(rx);
                dataLog.setTx(tx);
                dataLog.setType(type);
                dataLog.setHappenedAt(happenedAt);
                dataLog.setStatus(Constants.STATUS_UNSENT);
            //    dataLog.setMyNumber(DaoTestApplication.getInstance().getMyPhoneNumber());
                DataLogRepo.insertOrUpdate(context, dataLog);
            }

        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
