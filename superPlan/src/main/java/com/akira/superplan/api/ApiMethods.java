package com.akira.superplan.api;

import com.akira.superplan.dao.CallHistory;
import com.akira.superplan.dao.CircleDetails;
import com.akira.superplan.dao.CountryCodes;
import com.akira.superplan.dao.DataLog;
import com.akira.superplan.dao.LandLineCode;
import com.akira.superplan.dao.MessageHistory;
import com.akira.superplan.dao.NumberCode;
import com.akira.superplan.dao.OperatorDetails;
import com.akira.superplan.dao.PopupAlerts;
import com.akira.superplan.dao.USSDCode;
import com.akira.superplan.dao.UserBalance;
import com.akira.superplan.dao.WifiList;
import com.akira.superplan.model.GenerateOTP;
import com.akira.superplan.model.modelClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;

public interface ApiMethods {

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST("/api/v1/generate_otp")
    public void putGenerateOTP(@Body GenerateOTP generateOTP, Callback<GenerateOTP> callback);

    @POST("/api/v1/otp_verification_and_register")
    public void putVerifyOTP(@Body GenerateOTP generateOTP, Callback<GenerateOTP> callback);

    @GET("/api/v1/get_country_codes")
    public void getCountryCodes(Callback<List<CountryCodes>> callback);

    @GET("/api/v1/get_landline_codes")
    public void getLandlineCodes(Callback<List<LandLineCode>> callback);

    @GET("/api/v1/get_operators")
    public void getOperatorDetails(Callback<List<OperatorDetails>> callBack);

    @GET("/api/v1/get_circles")
    public void getCircleDetails(Callback<List<CircleDetails>> callback);

    @GET("/api/v1/get_ussd_codes")
    public void getUssdCodes(Callback<List<USSDCode>> callback);

    @POST("/api/v1/popup_alerts")
    public List<PopupAlerts> putPopupHistory(@Body HashMap<String,List<PopupAlerts>> hmap);

    @POST("/api/v1/wifi_histories")
    public List<WifiList> putWifiList(@Body HashMap<String, List<WifiList>> hmap);

    @POST("/api/v1/balance_histories")
    public List<UserBalance> putUserBalance(@Body HashMap<String, List<UserBalance>> hmap);


    @GET("/api/v1/get_number_codes")
    public void getNumberCodes(Callback<List<NumberCode>> callback);

    @POST("/api/v1/call_histories")
    public ArrayList<CallHistory> putCallLog(@Body modelClass modelobj);

    @POST("/api/v1/message_histories")
    public List<MessageHistory> putMessageLog(@Body HashMap<String, List<MessageHistory>> hmap);

    @POST("/api/v1/data_logs")
    public List<DataLog> putDataLog(@Body HashMap<String, List<DataLog>> hmap);
}