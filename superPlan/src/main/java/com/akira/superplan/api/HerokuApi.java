package com.akira.superplan.api;

import android.content.Context;
import android.util.Log;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.CallHistory;
import com.akira.superplan.dao.DataLog;
import com.akira.superplan.dao.MessageHistory;
import com.akira.superplan.dao.PopupAlerts;
import com.akira.superplan.dao.UserBalance;
import com.akira.superplan.dao.WifiList;
import com.akira.superplan.model.modelClass;
import com.akira.superplan.repositories.DataLogRepo;
import com.akira.superplan.repositories.HistoryRepo;
import com.akira.superplan.repositories.MessageHistoryRepo;
import com.akira.superplan.repositories.PopupAlertsRepo;
import com.akira.superplan.repositories.UserBalanceRepo;
import com.akira.superplan.repositories.WifiListRepo;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.Logger;
import com.google.gson.annotations.SerializedName;
import com.squareup.okhttp.OkHttpClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by vinitha on 15-02-2016.
 */
public class HerokuApi {

    @SerializedName("call_history")
    List<CallHistory> list;

    private static final String TAG = "HerokuApi";
    private String app_token = "token";

    RequestInterceptor requestInterceptor = new RequestInterceptor() {
        public void intercept(RequestFacade request) {
            String device_token = DaoTestApplication.getSharedPreferences().getString(Constants.DEVICE_TOKEN, "");
            String install_id = DaoTestApplication.getSharedPreferences().getString(Constants.INSTALLATION_ID,"");
            request.addHeader("Content-Type", "application/json");
            request.addHeader("Application-Id", app_token);
            request.addHeader("Device-Token",device_token);
            request.addHeader("Installation-Id",install_id);
        }
    };


    public void putCallHistory(List<CallHistory> histories, final Context context)
            throws InterruptedException {
        Log.d(TAG, "callhistory method:" + histories);
        final OkHttpClient okHttpClient = new OkHttpClient();
        //okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);
        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_URL)
                .setRequestInterceptor(requestInterceptor)
                .setClient(new OkClient(okHttpClient))
                .build();
        ApiMethods methods = restAdapter.create(ApiMethods.class);
        HashMap<String, List<CallHistory>> hmap = new HashMap<String, List<CallHistory>>();
        hmap.put("data", histories);
        modelClass obj=new modelClass();
        ArrayList d=new ArrayList();
       // d.add(histories);
        d.add(histories);
          obj.setData(d);
        obj.setUser_number_id("");
        obj.setEpoch("");
        obj.setSignature("");
        ArrayList<CallHistory> hList=methods.putCallLog(obj);
        for (CallHistory history : hList) {
            Logger.d(TAG, "callLog Success Response : " + history.getCallHistoryResponseId()
                    + " " + history.getCallHistoryId());
            CallHistory obj1 = HistoryRepo.getHistory(context, history.getCallHistoryId());
            obj1.setCallHistoryResponseId(history.getCallHistoryResponseId());
            obj1.setStatus("SENT");
            HistoryRepo.update(context, obj1);


        }

    }


    public void putMessageHistory(List<MessageHistory> messageHistory1, final Context context) throws InterruptedException {


        final OkHttpClient okHttpClient = new OkHttpClient();
        //okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);

        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_URL)
                .setClient(new OkClient(okHttpClient))
                .build();
        ApiMethods methods = restAdapter.create(ApiMethods.class);
        HashMap<String, List<MessageHistory>> hmap = new HashMap<String, List<MessageHistory>>();
        hmap.put("message_history", messageHistory1);
        List<MessageHistory> hList=methods.putMessageLog(hmap);
        Logger.d(TAG, "inside");
        for (MessageHistory history : hList) {
            Logger.d(TAG, "MessageLog Success Response : " + history.getMessageResponseId()
                    + " " + history.getMessageHistoryId());
            MessageHistory obj = MessageHistoryRepo.getMessageHistory(context, history.getMessageHistoryId());
            obj.setMessageResponseId(history.getMessageResponseId());
            obj.setStatus("SENT");
            MessageHistoryRepo.update(context, obj);
        }

    }

    public void putDataLog(List<DataLog> dataLogs, final Context context)
            throws InterruptedException {

        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_URL)
                .build();
        ApiMethods methods = restAdapter.create(ApiMethods.class);
        HashMap<String, List<DataLog>> hmap = new HashMap<String, List<DataLog>>();
        hmap.put("data_log", dataLogs);
        List<DataLog> dataLogList=methods.putDataLog(hmap);
        for (DataLog dataLog:dataLogList) {
            Logger.d(TAG, "DataLog Success Response : " + dataLog.getDataResponseID()
                    + " " + dataLog.getDataLogID());
            DataLog obj = DataLogRepo.getDataLog(context, dataLog.getDataLogID());
            obj.setDataResponseID(dataLog.getDataResponseID());
            obj.setStatus("SENT");
            DataLogRepo.update(context, obj);

        }
    }

    public void putPopupHistory(List<PopupAlerts> popupAlerts, final Context context)
            throws InterruptedException {

        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_URL)
                .build();
        ApiMethods methods = restAdapter.create(ApiMethods.class);
        Logger.d("popup", popupAlerts.get(0).getPopupAlterId()+" "
                +popupAlerts.get(0).getPopupText()+" "+popupAlerts.get(0).getHappenedAt()+" "+popupAlerts.get(0).getStatus()
                +popupAlerts.get(0).getCallCost()+" "+popupAlerts.get(0).getCallDuration()+" "+popupAlerts.get(0).getSmsCost()
                +popupAlerts.get(0).getDataCost()+" "+popupAlerts.get(0).getDataUsage()+" "+popupAlerts.get(0).getDataBalance()
                +popupAlerts.get(0).getCurrentBalance()+" "+popupAlerts.get(0).getMobilePlan()+" "+popupAlerts.get(0).getPromoBalance());
        HashMap<String, List<PopupAlerts>> hmap = new HashMap<String, List<PopupAlerts>>();
        hmap.put("popup_alert", popupAlerts);
        Logger.d("popup", " "+hmap);
        List<PopupAlerts> popupAlertsList = methods.putPopupHistory(hmap);
        for (PopupAlerts popupAlerts1: popupAlertsList) {

            Logger.d(TAG, "PopUpAlerts  Success Response: " + popupAlerts1.getPopupalertResponseId()
                    + " " + popupAlerts1.getPopupalertResponseId());
            PopupAlerts popUp = PopupAlertsRepo.getPopUpAlert(
                    context, popupAlerts1.getPopupAlterId());
            popUp.setPopupalertResponseId(popupAlerts1.getPopupalertResponseId());
            popUp.setStatus("SENT");
            PopupAlertsRepo.update(context, popUp);
        }
    }



    public void putUserBalance(List<UserBalance> userBalances, final Context context)
            throws InterruptedException {

        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_URL)
                .build();
        ApiMethods methods = restAdapter.create(ApiMethods.class);
        HashMap<String, List<UserBalance>> hmap = new HashMap<String, List<UserBalance>>();
        hmap.put("user_balance",userBalances);
        List<UserBalance> userBalanceList = methods.putUserBalance(hmap);
        for (UserBalance balance:userBalanceList) {

            UserBalance userBalance = UserBalanceRepo.getUserBalance(
                    context, balance.getUserBalanceId());
            userBalance.setUserBalanceResponseId(balance.getUserBalanceResponseId());
            UserBalanceRepo.update(context, userBalance);
            Logger.d(TAG, "userbal success response" + balance.getUserBalanceResponseId() + " " +
                    balance.getUserBalanceId());
        }

    }

    /* public void putOperatorMessage(List<OperatorMessageHistory> histories, final Context context)
             throws InterruptedException {

         final RestAdapter restAdapter = new RestAdapter.Builder()
                 .setEndpoint(Constants.API_URL)
                 .build();
         ApiMethods methods = restAdapter.create(ApiMethods.class);
         HashMap<String, List<OperatorMessageHistory>> hmap = new HashMap<String, List<OperatorMessageHistory>>();
         hmap.put("operator_message_history", histories);
         List<OperatorMessageHistory> operatorMessageHistoryList = methods.putOperatorMessageHistory(hmap);

         for (OperatorMessageHistory history:operatorMessageHistoryList) {

             Logger.d(TAG, "OperatorMessage  Success Response: " + history.getResponseId()
                     + " " + history.getMessageHistoryId());
             OperatorMessageHistory operatorMessage = OperatorMessageHistoryRepo.getOperatorMessage(
                             context, history.getMessageHistoryId());
                     operatorMessage.setResponseId(history.getResponseId());
                     operatorMessage.setStatus("SENT");
                     OperatorMessageHistoryRepo.update(context, operatorMessage);
         }

     }
 */

    public void putWifiList(List<WifiList> wifiLists, final Context context)
            throws InterruptedException {

        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_URL)
                .build();
        ApiMethods methods = restAdapter.create(ApiMethods.class);
        HashMap<String, List<WifiList>> hmap = new HashMap<String, List<WifiList>>();
        hmap.put("wifi_list", wifiLists);
        List<WifiList> wifiListList = methods.putWifiList(hmap);

        for (WifiList wifiList : wifiListList) {
            WifiList wifi = WifiListRepo.getWifi(
                    context, wifiList.getWifiId());
            Logger.d(TAG,"Wifi Success Response : "+wifiList.getWifiResponseId());
            Logger.d(TAG," wifi Success Response : "+wifiList.getWifiName());
            wifi.setWifiResponseId(wifiList.getWifiResponseId());
            WifiListRepo.update(context, wifi);
            Logger.d(TAG, "wifi " + wifiList.getWifiResponseId()+ " " + wifiList.getWifiId());
        }

    }
}

