package com.akira.superplan.model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;


public class GenerateOTP {



    @SerializedName("id")
        private String id;



    @SerializedName("data")
        private HashMap data=new HashMap();
        @SerializedName("user_id")
        private String userId;
        @SerializedName("epoch")
        private String epoch;
        @SerializedName("signature")
        private String signature;
        @SerializedName("user_number_id")
        private String user_number_id;
      @SerializedName("mobile_number")
         private String mobile_number;
    @SerializedName("operator_code")
    private String operator_code;
    @SerializedName("circle_code")
    private String circle_code;

    public String getOperator_code() {
        return operator_code;
    }

    public void setOperator_code(String operator_code) {
        this.operator_code = operator_code;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getCircle_code() {
        return circle_code;
    }

    public void setCircle_code(String circle_code) {
        this.circle_code = circle_code;
    }


    public void setData(HashMap data) {
        this.data = data;
    }

    public String getUser_number_id() {
        return user_number_id;
    }

    public void setUser_number_id(String user_number_id) {
        this.user_number_id = user_number_id;
    }

    /**
         *
         * @return
         * The userId

         */
        public String getUserId() {
            return userId;
        }

        /**
         *
         * @param userId
         * The user_id
         */
        public void setUserId(String userId) {
            this.userId = userId;
        }

        /**
         *
         * @return
         * The epoch
         */
        public String getEpoch() {
            return epoch;
        }

        /**
         *
         * @param epoch
         * The epoch
         */
        public void setEpoch(String epoch) {
            this.epoch = epoch;
        }

        /**
         *
         * @return
         * The signature
         */
        public String getSignature() {
            return signature;
        }

        /**
         *
         * @param signature
         * The signature
         */
        public void setSignature(String signature) {
            this.signature = signature;
        }
         public String getId() {
           return id;
    }
    public void setId(String id) {
        this.id = id;
    }


}

