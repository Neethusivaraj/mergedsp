package com.akira.superplan.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vinitha on 18-02-2016.
 */
public class modelClass {



        @SerializedName("data")
        ArrayList data=new ArrayList();
        @SerializedName("user_id")
        private String userId;
        @SerializedName("epoch")
        private String epoch;
        @SerializedName("signature")
        private String signature;
        @SerializedName("user_number_id")
        private String user_number_id;


        public String getUser_number_id() {
            return user_number_id;
        }

        public void setUser_number_id(String user_number_id) {
            this.user_number_id = user_number_id;
        }

        /**
         *
         * @return
         * The userId

         */
        public ArrayList getData() {
            return data;
        }

    public void setData(ArrayList data) {
        this.data = data;
    }
        public String getUserId() {
            return userId;
        }

        /**
         *
         * @param userId
         * The user_id
         */
        public void setUserId(String userId) {
            this.userId = userId;
        }

        /**
         *
         * @return
         * The epoch
         */
        public String getEpoch() {
            return epoch;
        }

        /**
         *
         * @param epoch
         * The epoch
         */
        public void setEpoch(String epoch) {
            this.epoch = epoch;
        }

        /**
         *
         * @return
         * The signature
         */
        public String getSignature() {
            return signature;
        }

        /**
         *
         * @param signature
         * The signature
         */
        public void setSignature(String signature) {
            this.signature = signature;
        }



    }



