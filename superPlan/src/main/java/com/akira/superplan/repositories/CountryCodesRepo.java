package com.akira.superplan.repositories;

import android.content.Context;
import android.util.Log;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.CountryCodes;
import com.akira.superplan.dao.CountryCodesDao;

import java.util.List;

/**
 * Created by LENOVO on 10/12/2015.
 */
public class CountryCodesRepo {

    private static final String TAG ="country codes";

    public static void insertOrUpdate(Context context, List<CountryCodes> countryCodes) {
        Log.d(TAG, "countrycodes dumped !!" + countryCodes.get(0).getCountryName());
        getCountryCodesDao(context).insertInTx(countryCodes);

    }
    private static CountryCodesDao getCountryCodesDao(Context context) {
        return ((DaoTestApplication) context.getApplicationContext()).getDaoSession()
                .getCountryCodesDao();
    }

    public static List<CountryCodes> getAllCountryCodes(Context context) {
        return getCountryCodesDao(context).queryBuilder().list();
    }

    public static String getCountryDetail(Context context, String code)
    {
        String country="invalid";
        for(int i=2;i<=4;i++) {
            String check=code.substring(0,i);
            List<CountryCodes> countryCodes = getCountryCodesDao(context).queryBuilder().where(
                    CountryCodesDao.Properties.NumberCode.eq(check)).list();
            if (countryCodes.size() != 0) {
                country=countryCodes.get(0).getCountryName();
                break;
            }
        }
        return country;
    }

}
