package com.akira.superplan.repositories;

import android.content.Context;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.UserBalance;
import com.akira.superplan.dao.UserBalanceDao;

import java.util.List;

/**
 * Created by Poornima on 15-10-2015.
 */
public class UserBalanceRepo {

    public static void insertOrUpdate(Context context, UserBalance balance) {
        getUserBalanceDao(context).insertOrReplace(balance);
    }

    public static List<UserBalance> getAllBalance(Context context) {
        return getUserBalanceDao(context).queryBuilder().list();
    }

    public static String getRecentBalance(Context context) {
        List<UserBalance> balance = getUserBalanceDao(context).queryBuilder().where(
                UserBalanceDao.Properties.CurrentBalance.isNotNull()).orderDesc(
                UserBalanceDao.Properties.LastUpdated).list();
        if(balance.size() > 0)
        {
            return balance.get(0).getCurrentBalance();
        }
        else {
            return "--.--";
        }

    }
    public static String getDataBalance(Context context) {
        List<UserBalance> balance = getUserBalanceDao(context).queryBuilder().where
                               (UserBalanceDao.Properties.DataBalance.isNotNull()).orderDesc(
                UserBalanceDao.Properties.LastUpdated).list();
        if(balance.size() > 0)
        {
            return balance.get(0).getDataBalance();
        }
        else {
            return "--.--";
        }
    }
    public static UserBalance getUserBalance(Context context,Long id)
    {

        return getUserBalanceDao(context).queryBuilder().where(
                               UserBalanceDao.Properties.UserBalanceId.eq(id)).list().get(0);


    }

    public static Long getUnsentCount(Context context) {
                return getUserBalanceDao(context).queryBuilder().where(
                        UserBalanceDao.Properties.UserBalanceResponseId.eq(0)).count();
            }

    public static Long getSentCount(Context context) {
        return getUserBalanceDao(context).queryBuilder().where(
                UserBalanceDao.Properties.UserBalanceResponseId.notEq(0)).count();
    }

    public static List<UserBalance> getUnsentHistories(Context context) {
        return getUserBalanceDao(context).queryBuilder().where(
                UserBalanceDao.Properties.UserBalanceResponseId.eq(0)).list();
    }

    public static UserBalanceDao getUserBalanceDao(Context context) {
        return ((DaoTestApplication) context.getApplicationContext()).
                getDaoSession().getUserBalanceDao();
    }
    public static void update(Context context, UserBalance balance) {
        getUserBalanceDao(context).update(balance);
    }
}
