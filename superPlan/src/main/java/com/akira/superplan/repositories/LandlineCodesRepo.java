package com.akira.superplan.repositories;

import android.content.Context;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.LandLineCode;
import com.akira.superplan.dao.LandLineCodeDao;
import com.akira.superplan.utils.Logger;

import java.util.List;

/**
 * Created by LENOVO on 10/12/2015.
 */
public class LandlineCodesRepo {

    public static void insertOrUpdate(Context context, LandLineCode landlineCodes) {
        getLandLineCodeDao(context).insertOrReplace(landlineCodes);
    }

    private static LandLineCodeDao getLandLineCodeDao(Context context) {
        return ((DaoTestApplication) context.getApplicationContext()).
                getDaoSession().getLandLineCodeDao();
    }
    public static String getMobileCircle(Context context, String code)
    {
        String mobileCircle="invalid";
        if(code.length() >= 8)
        for(int i=2;i<=8;i++) {
            String check=code.substring(0,i);
            Logger.d("SummaryActivity",check);
            List<LandLineCode> Codes = getLandLineCodeDao(context).queryBuilder().where(
                    LandLineCodeDao.Properties.StartingCode.eq(check)).list();
            if (Codes.size() != 0) {
                mobileCircle=Codes.get(0).getTelecomCircleCode();
                break;
            }
        }
        return mobileCircle;
    }
    public static List<LandLineCode> getAllLandlineCodes(Context context) {
        return getLandLineCodeDao(context).queryBuilder().list();
    }

    public static void insertOrUpdateTx(Context context, List<LandLineCode> landlineCodes) {

        getLandLineCodeDao(context).insertInTx(landlineCodes);

    }


    public static Long getCount(Context context) {

        return getLandLineCodeDao(context).count();

    }


}
