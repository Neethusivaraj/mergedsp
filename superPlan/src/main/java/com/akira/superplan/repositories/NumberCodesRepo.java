package com.akira.superplan.repositories;

import android.content.Context;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.NumberCode;
import com.akira.superplan.dao.NumberCodeDao;

import java.util.List;

/**
 * Created by LENOVO on 10/12/2015.
 */
public class NumberCodesRepo {

    public static void insertOrUpdate(Context context, NumberCode numberCodes) {
        getNumberCodeDao(context).insertOrReplace(numberCodes);
    }

    public static void insertOrUpdateNumberList(Context context, List<NumberCode> numberCodesList) {

        getNumberCodeDao(context).insertInTx(numberCodesList);

    }

    private static NumberCodeDao getNumberCodeDao(Context context) {
        return ((DaoTestApplication) context.getApplicationContext()).
                getDaoSession().getNumberCodeDao();
    }


    public static List<NumberCode> getAllNumberCodes(Context context) {
        return getNumberCodeDao(context).queryBuilder().list();
    }

    public static NumberCode getNumberDetails(Context context, String code) {
        return getNumberCodeDao(context).queryBuilder().list().get(0);

    }


    public static String getNumberCircle(Context context, String code)
    {
        List<NumberCode> numberCodes=getNumberCodeDao(context).queryBuilder().where(
                NumberCodeDao.Properties.NumberCode.eq(code)).list();
        if(numberCodes.size()!=0)
        {
            return numberCodes.get(0).getTelecomCircle();
        }
        else
        {
            return "invalid";
        }
    }
    public static String getNetworkOperator(Context context, String code)
    {
        List<NumberCode> numberCodes=getNumberCodeDao(context).queryBuilder().where(
                NumberCodeDao.Properties.NumberCode.eq(code)).list();
        if(numberCodes.size()!=0)
        {
            return numberCodes.get(0).getNetworkOperator();
        }
        else
        {
            return "false";
        }
    }
}
