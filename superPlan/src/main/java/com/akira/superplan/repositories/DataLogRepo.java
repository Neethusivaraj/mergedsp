package com.akira.superplan.repositories;

import android.content.Context;
import android.util.Log;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.DataLog;
import com.akira.superplan.dao.DataLogDao;
import com.akira.superplan.utils.Constants;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by nanda on 6/16/15.
 */
public class DataLogRepo {

    static long totalRx = 0;
    static long totalTx = 0;
    static long mobileRx = 0;
    static long mobileTx = 0;
    static long wifiRx = 0;
    static long wifiTx = 0;
    private static String TAG="data log";

    public static void insertOrUpdate(Context context, DataLog dataLog) {
        Log.d(TAG, "datalog"+dataLog);
        getDataLogDao(context).insertOrReplace(dataLog);
    }

    public static List<DataLog> getAllLogs(Context context) {
        return getDataLogDao(context).queryBuilder().orderDesc(
                DataLogDao.Properties.HappenedAt).list();
    }

    public static List<DataLog> getUnsentHistories(Context context) {
        return getDataLogDao(context).queryBuilder().where(
                DataLogDao.Properties.Status.eq(Constants.STATUS_UNSENT)).list();
    }

    public static void update(Context context, DataLog dataLog) {
        getDataLogDao(context).update(dataLog);
    }

    public static void calculateDataUsageSummary(Context context) {
        totalRx = totalTx = mobileTx = mobileRx = wifiRx = wifiTx = 0;
        List<DataLog> mobileDataLogs = getDataLogDao(context).queryBuilder().where(
                DataLogDao.Properties.Type.eq(Constants.MOBILE_TYPE)).list();
        List<DataLog> wifiDataLogs = getDataLogDao(context).queryBuilder().where(
                DataLogDao.Properties.Type.eq(Constants.WIFI_TYPE)).list();

        for (DataLog dataLog : mobileDataLogs) {
            mobileRx += dataLog.getRx();
            mobileTx += dataLog.getTx();
        }
        for (DataLog dataLog : wifiDataLogs) {
            wifiRx += dataLog.getRx();
            wifiTx += dataLog.getTx();
        }
        totalRx = mobileRx + wifiRx;
        totalTx = mobileTx + wifiTx;
    }

    public static String getTotalRx(Context context) {
        return convertDataFromLong(totalRx);
    }

    public static String getTotalTx(Context context) {
        return convertDataFromLong(totalTx);
    }

    public static String getMobileRx(Context context) {
        return convertDataFromLong(mobileRx);
    }

    public static String getWifiRx(Context context) {
        return convertDataFromLong(wifiRx);
    }

    public static String getWifiTx(Context context) {
        return convertDataFromLong(wifiTx);
    }

    public static String getMobileTx(Context context) {
        return convertDataFromLong(mobileTx);
    }

    public static String convertDataFromLong(Long bytes) {

        float kb = (float) bytes / 1024;
        float mb = (float) bytes / (1024 * 1024);
        float gb = (float) bytes / (1024 * 1024 * 1024);

        DecimalFormat dec = new DecimalFormat("0.00");

        if (gb >= 1) {
            return String.valueOf(dec.format(gb)) + " GB";
        } else if (mb >= 1) {
            return String.valueOf(dec.format(mb)) + " MB";
        } else if (kb >= 1) {
            return String.valueOf(dec.format(kb)) + " kb";
        } else {
            return String.valueOf(bytes) + " bytes";
        }
    }

    public static DataLog getDataLog(Context context,Long dataLogId)
    {
        return getDataLogDao(context).queryBuilder().where(DataLogDao.Properties.DataLogID.eq(dataLogId)).list().get(0);
    }

    public static Long getUnsentCount(Context context) {
        return getDataLogDao(context).queryBuilder().where(DataLogDao.Properties.Status.eq(Constants.STATUS_UNSENT)).count();
    }
    public static Long getSentCount(Context context) {
        return getDataLogDao(context).queryBuilder().where(DataLogDao.Properties.Status.eq("SENT")).count();
    }
    public static List<DataLog> getLogbyDate(Context context,java.util.Date date)
    {
        return getDataLogDao(context).queryBuilder().where(
                DataLogDao.Properties.HappenedAt.ge(date)).orderDesc(
                DataLogDao.Properties.HappenedAt).list();
    }
    public static List<DataLog> getYesterdaysLog(Context context,java.util.Date date1,Date date2)
    {
        return getDataLogDao(context).queryBuilder().where(
                DataLogDao.Properties.HappenedAt.ge(date1), DataLogDao.Properties.HappenedAt.le(date2)).
                orderDesc(DataLogDao.Properties.HappenedAt).list();
    }
    private static DataLogDao getDataLogDao(Context context) {
        return ((DaoTestApplication) context.getApplicationContext()).
                getDaoSession().getDataLogDao();
    }
}
