package com.akira.superplan.repositories;

import android.content.Context;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.OperatorDetails;
import com.akira.superplan.dao.OperatorDetailsDao;

import java.util.List;

/**
 * Created by LENOVO on 10/22/2015.
 */
public class OperatorDetailsRepo {

    public static OperatorDetailsDao getOperatorDetailsDao(Context context) {
        return ((DaoTestApplication) context.getApplicationContext()).
                getDaoSession().getOperatorDetailsDao();
    }


    public static void insertOrUpdate(Context context, List<OperatorDetails> operatorDetails) {
        getOperatorDetailsDao(context).insertOrReplaceInTx(operatorDetails);
    }

    public static List<OperatorDetails> getAllDetails(Context context) {
        return getOperatorDetailsDao(context).queryBuilder().list();
    }

    public static OperatorDetails getOperatorName(Context context, String code) {
        return getOperatorDetailsDao(context).queryBuilder().where(
                OperatorDetailsDao.Properties.OperatorCode.eq(code)).list().get(0);
    }

}
