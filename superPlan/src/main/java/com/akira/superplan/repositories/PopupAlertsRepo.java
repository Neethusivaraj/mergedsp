package com.akira.superplan.repositories;

import android.content.Context;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.PopupAlerts;
import com.akira.superplan.dao.PopupAlertsDao;
import com.akira.superplan.utils.Constants;

import java.util.List;

/**
 * Created by nanda on 6/16/15.
 */
public class PopupAlertsRepo {

    public static void insertOrUpdate(Context context, PopupAlerts popupAlerts) {
        getPopupAlertsDao(context).insertOrReplace(popupAlerts);
    }

    public static List<PopupAlerts> getAllLogs(Context context) {
        return getPopupAlertsDao(context).queryBuilder().orderDesc(
                PopupAlertsDao.Properties.HappenedAt).list();
    }

    public static PopupAlerts getLastLog(Context context) {
        return getPopupAlertsDao(context).queryBuilder().limit(1).orderDesc(
                PopupAlertsDao.Properties.HappenedAt).list().get(0);
    }

    public static List<PopupAlerts> getUnsentHistories(Context context) {
        return getPopupAlertsDao(context).queryBuilder().where(
                PopupAlertsDao.Properties.Status.eq(Constants.STATUS_UNSENT)).list();
    }


    public static PopupAlerts getPopupTextByID(Context context, Long aAlertId) {
        return getPopupAlertsDao(context).queryBuilder().where
                (PopupAlertsDao.Properties.PopupAlterId.eq(aAlertId)).unique();
    }


    public static Long getUnsentCount(Context context) {
        return getPopupAlertsDao(context).queryBuilder().where(
                PopupAlertsDao.Properties.Status.eq(Constants.STATUS_UNSENT)).count();
    }

    public static Long getSentCount(Context context) {
        return getPopupAlertsDao(context).queryBuilder().where(
                PopupAlertsDao.Properties.Status.eq("SENT")).count();
    }

    public static void update(Context context, PopupAlerts popupAlerts) {
        getPopupAlertsDao(context).update(popupAlerts);
    }

    private static PopupAlertsDao getPopupAlertsDao(Context context) {
        return ((DaoTestApplication) context.getApplicationContext()).
                getDaoSession().getPopupAlertsDao();
    }


    public static PopupAlerts getPopUpAlert(Context context, Long alertId) {
        return getPopupAlertsDao(context).queryBuilder().where(
                PopupAlertsDao.Properties.PopupAlterId.eq(alertId)).list().get(0);
    }
}
