package com.akira.superplan.repositories;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.CallHistory;
import com.akira.superplan.dao.CallHistoryDao;
import com.akira.superplan.utils.Constants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by nanda on 22/4/15.
 */
public class HistoryRepo {

    public static int totalIn;
    public static int totalOut;
    private static String TAG="CALL HISTORY TABLE ";

    public static void insertOrUpdate(Context context, CallHistory history) {
        getCallHistoryDao(context).insertOrReplace(history);

    }

    public static void insertOrUpdateTx(Context context, List<CallHistory> history) {
        Log.d(TAG,"dumped call log::"+history);
        Log.d(TAG,"dumped call log::"+history.get(0).getHappenedAt());

        Log.d(TAG,"dumped call log::"+history.get(0).getOtherNumber());
        getCallHistoryDao(context).insertInTx(history);

    }

    public static List<CallHistory> getAllHistories(Context context) {
        return getCallHistoryDao(context).queryBuilder().orderDesc(
                CallHistoryDao.Properties.HappenedAt).list();
//        return getHistoryDao(context).loadAll();
    }

    public static CallHistory getPopUpTextByID(Context context, Long aHistoryID) {
        return getCallHistoryDao(context).queryBuilder().where
                (CallHistoryDao.Properties.PopupalertId.eq(aHistoryID)).unique();
    }



    public static CallHistory getLastLog(Context context) {
        return getCallHistoryDao(context).queryBuilder().limit(1).orderDesc(
                CallHistoryDao.Properties.HappenedAt).list().get(0);
    }

    public static CallHistory getHistory(Context context, Long HistoryId) {
        return getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.CallHistoryId.eq(HistoryId)).list().get(0);
    }
    public static List<CallHistory> getallHistory(Context context) {
        return getCallHistoryDao(context).queryBuilder().list();
    }

    public static List<CallHistory> getUnsentHistories(Context context) {
        return getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.Status.eq(Constants.STATUS_UNSENT)).list();
    }

    public static Long getUnsentCount(Context context) {
        return getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.Status.eq(Constants.STATUS_UNSENT)).count();
    }

    public static Long getSentCount(Context context) {
        return getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.Status.eq("SENT")).count();
    }

    public static void deleteAllHistory(Context context) {
        getCallHistoryDao(context).deleteAll();
    }

    public static void updateContact(Context context, String oldName, String newName) {
        List<CallHistory> histories = getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.Name.eq(oldName)).list();
        for (CallHistory history : histories) {
            history.setName(newName);
            update(context, history);
        }
    }

    public static void update(Context context, CallHistory history) {
        getCallHistoryDao(context).update(history);
    }


    public static Long getCount(Context context) {
        return getCallHistoryDao(context).count();
    }

    public static Long getIncomingCount(Context context) {
        return getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.Type.eq(Constants.RECEIVED_CALL)).count();
    }

    public static Long getInNetworkCount(Context context) {
        return getCallHistoryDao(context).queryBuilder().where(CallHistoryDao.Properties.IsInNetwork.eq("true")).count();
    }

    public static Long getOutNetworkCount(Context context) {
        return getCallHistoryDao(context).queryBuilder().where(CallHistoryDao.Properties.IsInNetwork.eq("false")).count();
    }

    public static void calculateDuration(Context context) {
        totalIn = totalOut = 0;
        List<CallHistory> totalIncoming = getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.Type.eq(Constants.RECEIVED_CALL)).list();
        Log.d(TAG,"total calls:"+totalIncoming);
        List<CallHistory> totalOutgoing = getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.Type.eq(Constants.OUTGOING_CALL)).list();

        for (CallHistory history : totalIncoming) {
            totalIn += history.getSeconds();
        }
        for (CallHistory history : totalOutgoing) {
            totalOut += history.getSeconds();
        }
    }

    public static void getFrequentList(Context context) {
        ArrayList<String> result = new ArrayList<String>();
        Date date = Calendar.getInstance().getTime();
        Log.d("list", " " + date);
        Cursor c = getCallHistoryDao(context).getDatabase().rawQuery(
                "SELECT COUNT(OTHER_NUMBER)as FAV_COUNT, "
                        + "OTHER_NUMBER FROM CALL_HISTORY GROUP BY OTHER_NUMBER ORDER BY FAV_COUNT DESC", null);
        if (c.moveToFirst()) {
            do {
                result.add(c.getString(0));
                Log.d("list", c.getString(0) + " " + c.getString(1));
            } while (c.moveToNext());
        }
        c.close();

    }


    public static Long getOutgoingCount(Context context) {
        return getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.Type.eq(Constants.OUTGOING_CALL)).count();
    }

    public static Long getMissedCount(Context context) {
        return getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.Type.eq(Constants.MISSED_CALL)).count();
    }

    public static Long getLocalCount(Context context) {
        return getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.CallCategory.eq("local")).count();
    }

    public static Long getStdCount(Context context) {
        return getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.CallCategory.eq("STD")).count();
    }

    public static Long getIsdCount(Context context) {
        return getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.CallCategory.like("ISD")).count();
    }

    public static Long getInvalidCount(Context context) {
        return getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.CallCategory.like("invalid")).count();
    }

    public static List<CallHistory> getHistorybyDate(Context context, java.util.Date date) {
        return getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.HappenedAt.ge(date)).orderDesc(
                CallHistoryDao.Properties.HappenedAt).list();
    }

    public static List<CallHistory> getYesterdaysLog(Context context, java.util.Date date1, Date date2) {
        return getCallHistoryDao(context).queryBuilder().where(
                CallHistoryDao.Properties.HappenedAt.ge(date1), CallHistoryDao.Properties.HappenedAt.le(date2)).
                orderDesc(CallHistoryDao.Properties.HappenedAt).list();
    }

    private static CallHistoryDao getCallHistoryDao(Context context) {
        return ((DaoTestApplication) context.getApplicationContext())
                .getDaoSession().getCallHistoryDao();
    }
}
