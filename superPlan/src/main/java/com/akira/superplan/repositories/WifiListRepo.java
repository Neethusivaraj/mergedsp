package com.akira.superplan.repositories;

import android.content.Context;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.WifiList;
import com.akira.superplan.dao.WifiListDao;
import com.akira.superplan.utils.Common;

import java.util.List;

/**
 * Created by LENOVO on 10/20/2015.
 */
public class WifiListRepo {

    private static final String TAG = "WifiListRepo";

    public static void insertOrUpdate(Context context, WifiList wifiList) {
        getWifiListDao(context).insertOrReplace(wifiList);
    }

    public static List<WifiList> getAllWifiList(Context context) {
        return getWifiListDao(context).queryBuilder().list();
    }

    public static void update(Context context, WifiList wifiList) {
        getWifiListDao(context).update(wifiList);
    }

    public static void updateTime(Context context, long rx, long tx) {

        List<WifiList> list=getWifiListDao(context).queryBuilder().orderDesc(
                WifiListDao.Properties.ConnectionEstablishedTime).list();
        if(list.size()>0) {
            WifiList wifiList = getWifiListDao(context).queryBuilder().orderDesc(
                    WifiListDao.Properties.ConnectionEstablishedTime).list().get(0);
            wifiList.setRx(rx);
            wifiList.setTx(tx);
            wifiList.setConnectionOffTime(Common.getTime());
            update(context, wifiList);
        }

    }

    public static WifiListDao getWifiListDao(Context context) {
        return ((DaoTestApplication) context.getApplicationContext()).
                getDaoSession().getWifiListDao();
    }

    public static WifiList getWifi(Context context,Long id)
    {
        return getWifiListDao(context).queryBuilder().where(
                               WifiListDao.Properties.WifiId.eq(id)).list().get(0);
    }

    public static List<WifiList> getUnsentWifi(Context context) {
        return getWifiListDao(context).queryBuilder().where(
                WifiListDao.Properties.WifiResponseId.eq(0),
                               WifiListDao.Properties.ConnectionOffTime.isNotNull()).list();
    }

    public static Long getUnsentCount(Context context) {
                return getWifiListDao(context).queryBuilder().where(
                        WifiListDao.Properties.WifiResponseId.eq(0)).count();
            }

     public static Long getSentCount(Context context) {
            return getWifiListDao(context).queryBuilder().where(
                    WifiListDao.Properties.WifiResponseId.notEq(0)).count();
    }
}
