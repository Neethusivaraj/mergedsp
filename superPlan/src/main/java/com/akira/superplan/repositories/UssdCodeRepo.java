package com.akira.superplan.repositories;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.USSDCode;
import com.akira.superplan.dao.USSDCodeDao;

import java.util.List;


/**
 * Created by LENOVO on 10/14/2015.
 */
public class UssdCodeRepo {

//    public static void insertOrUpdate(Context context, ussd_codes codes) {
//        getUssdCodesDao(context).insertOrReplace(codes);
//    }


    public static void insertOrUpdate(Context context, List<USSDCode> ussdCodes) {
        getUSSDCodeDao(context).insertInTx(ussdCodes);
    }

    public static List<USSDCode> getAllussdCodes(Context context) {
        return getUSSDCodeDao(context).queryBuilder().list();
    }

    private static USSDCodeDao getUSSDCodeDao(Context context) {
        return ((DaoTestApplication) context.getApplicationContext()).
                getDaoSession().getUSSDCodeDao();
    }


    public static String getCallCode(Context context, String operator) {

        Cursor c = getUSSDCodeDao(context).getDatabase().rawQuery(
                "SELECT * FROM USSDCODE WHERE  NETWORK_OPERATOR  = '" + operator + "'", null);


        String code = operator;
        if (c.moveToFirst()) {
            do {
                code = c.getString(c.getColumnIndex("USSD_CODE"));
            } while (c.moveToNext());
        }

        return code;


    }

    public static String getDataUssdCode(Context context, String operator) {

        Cursor c = getUSSDCodeDao(context).getDatabase().rawQuery(
                "SELECT * FROM USSDCODE WHERE USSD_CODE  = '" + operator + "'", null);
        String code = operator;
        SharedPreferences sp = DaoTestApplication.getSharedPreferences();
        if (c.moveToFirst()) {
            do {
              /*  if(DaoTestApplication.getInstance().getMyOperator().equals("AIRTEL"))
                {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("DATAPACK", );
                }*/
                code = c.getString(c.getColumnIndex("BALANCE_2G"));
            } while (c.moveToNext());
        }
        return code;

    }

    public static List<USSDCode> getBalanceCode(Context context, String operator) {
        return getUSSDCodeDao(context).queryBuilder().where(USSDCodeDao.Properties.NetworkOperatorCode.eq(operator),
                USSDCodeDao.Properties.UssdType.eq("balance")).list();
    }

    public static String getCodeByUssdType(Context context, String operator, String type) {


        return String.valueOf(getUSSDCodeDao(context).queryBuilder().where(
                USSDCodeDao.Properties.NetworkOperatorCode.eq(operator), USSDCodeDao.Properties.UssdType.eq(type)).unique());
    }

    public static String getDataCode(Context context, String operator, String plan) {

        Cursor c = getUSSDCodeDao(context).getDatabase().rawQuery(
                "SELECT * FROM USSDCODE WHERE NETWORK_OPERATOR  = '" + operator + "'", null);
        String code = operator;
        SharedPreferences sp = DaoTestApplication.getSharedPreferences();
        if (c.moveToFirst()) {
            do {
                if (plan.equals("2g")) {
                    code = c.getString(c.getColumnIndex("BALANCE_2G"));
                } else {
                    code = c.getString(c.getColumnIndex("BALANCE_3G"));
                }
            } while (c.moveToNext());
        }
        return code;

    }
}
