package com.akira.superplan.repositories;

import android.content.Context;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.MessageHistory;
import com.akira.superplan.dao.MessageHistoryDao;
import com.akira.superplan.utils.Constants;

import java.util.Date;
import java.util.List;

/**
 * Created by nanda on 5/26/15.
 */
public class MessageHistoryRepo {
    public static void insertOrUpdate(Context context, MessageHistory messageHistory) {
        getMessageHistoryDao(context).insertOrReplace(messageHistory);
    }
    public static void insertOrUpdateTx(Context context, List<MessageHistory> messageHistory) {

        getMessageHistoryDao(context).insertInTx(messageHistory);

    }
    public static Long getCount(Context context) {
        return getMessageHistoryDao(context).count();
    }
    public static MessageHistory getMessageHistory(Context context,Long MessageHistoryId)
    {
        return getMessageHistoryDao(context).queryBuilder().where(MessageHistoryDao.Properties.MessageHistoryId.eq(MessageHistoryId)).list().get(0);
    }
    public static Long getSentCount(Context context) {
        return getMessageHistoryDao(context).queryBuilder().where(
                MessageHistoryDao.Properties.Type.eq(Constants.SMS_SENT)).count();
    }

    public static Long getReceivedCount(Context context) {
        return getMessageHistoryDao(context).queryBuilder().where(
                MessageHistoryDao.Properties.Type.eq(Constants.SMS_RECEIVED)).count();
    }

    public static List<MessageHistory> getAllHistories(Context context) {
        return getMessageHistoryDao(context).queryBuilder().orderDesc(
                MessageHistoryDao.Properties.HappenedAt).list();
    }

    public static List<MessageHistory> getUnsentHistories(Context context) {
        return getMessageHistoryDao(context).queryBuilder().where(
                MessageHistoryDao.Properties.Status.eq(Constants.STATUS_UNSENT)).list();
    }


    public static MessageHistory getLastLog(Context context) {
        return getMessageHistoryDao(context).queryBuilder().limit(1).orderDesc(
                MessageHistoryDao.Properties.HappenedAt).list().get(0);
    }

    public static void update(Context context, MessageHistory messageHistory) {
        getMessageHistoryDao(context).update(messageHistory);
    }
    public static Long getMsgUnsentCount(Context context) {
        return getMessageHistoryDao(context).queryBuilder().where(MessageHistoryDao.Properties.Status.eq(Constants.STATUS_UNSENT)).count();
    }
    public static Long getMsgSentCount(Context context) {
        return getMessageHistoryDao(context).queryBuilder().where(MessageHistoryDao.Properties.Status.eq("SENT")).count();
    }

    public static List<MessageHistory> getHistorybyDate(Context context,java.util.Date date)
    {
        return getMessageHistoryDao(context).queryBuilder().where(
                MessageHistoryDao.Properties.HappenedAt.ge(date)).orderDesc(
                MessageHistoryDao.Properties.HappenedAt).list();
    }
    public static List<MessageHistory> getYesterdaysLog(Context context,java.util.Date date1,Date date2)
    {
        return getMessageHistoryDao(context).queryBuilder().where(
                MessageHistoryDao.Properties.HappenedAt.ge(date1),
                MessageHistoryDao.Properties.HappenedAt.le(date2)).
                orderDesc(MessageHistoryDao.Properties.HappenedAt).list();
    }
    private static MessageHistoryDao getMessageHistoryDao(Context context) {
        return ((DaoTestApplication) context.getApplicationContext()).
                getDaoSession().getMessageHistoryDao();
    }
}
