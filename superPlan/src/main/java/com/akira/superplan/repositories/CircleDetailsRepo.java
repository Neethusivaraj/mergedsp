package com.akira.superplan.repositories;

import android.content.Context;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.CircleDetails;
import com.akira.superplan.dao.CircleDetailsDao;

import java.util.List;

/**
 * Created by LENOVO on 10/22/2015.
 */
public class CircleDetailsRepo {

    public static CircleDetailsDao getCircleDetailsDao(Context context) {
        return ((DaoTestApplication) context.getApplicationContext()).getDaoSession()
                .getCircleDetailsDao();
    }


    public static void insertOrUpdate(Context context, List<CircleDetails> circleDetails) {
        getCircleDetailsDao(context).insertInTx(circleDetails);
    }

    public static List<CircleDetails> getAllDetails(Context context) {
        return getCircleDetailsDao(context).queryBuilder().list();
    }

    public static CircleDetails getCircleName(Context context, String code) {
        return getCircleDetailsDao(context).queryBuilder().
                where(CircleDetailsDao.Properties.CircleCode.eq(code)).list().get(0);
    }

}
