package com.akira.superplan.repositories;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.Contacts;
import com.akira.superplan.dao.ContactsDao;

import java.util.List;

/**
 * Created by LENOVO on 10/7/2015.
 */
public class ContactsRepo {
    private static String TAG="CONTACTS UPDATED";

    public static void insertOrUpdate(Context context, Contacts contacts) {
        getContactsDao(context).insertOrReplace(contacts);
    }
    public static void insertOrUpdateTx(Context context, List<Contacts> contacts) {
        Log.d(TAG, "contacts inserted :" + contacts);
        Log.d(TAG, "contacts inserted :" + contacts.get(0).getName());
        Log.d(TAG, "contacts inserted :" + contacts.get(0).getNumber());

        getContactsDao(context).insertInTx(contacts);

    }
    public static void deleteAll(Context context) {
        getContactsDao(context).deleteAll();
    }

    public static void delete(Context context, Contacts contacts) {
        getContactsDao(context).delete(contacts);
    }

    public static List<Contacts> getAllContacts(Context context) {
        return getContactsDao(context).queryBuilder().list();
    }

    private static ContactsDao getContactsDao(Context context) {
        return ((DaoTestApplication) context.getApplicationContext())
                .getDaoSession().getContactsDao();
    }

    public static void update(Context context, Contacts contacts) {
        getContactsDao(context).update(contacts);
    }

    public static Long getCount(Context context) {
        return getContactsDao(context).queryBuilder().count();
    }

    public static Contacts getName(Context context, String number) {
        number = number.replace("+91", "");
        Contacts contacts = new Contacts();
        if(number.startsWith("0")) {
            number = number.replaceFirst("0", "");
        }
        if(number.length()>=10) {
            number = number.substring(number.length() - 10, number.length());
            Cursor c = getContactsDao(context).getDatabase().rawQuery(
                    "SELECT * FROM CONTACTS WHERE NUMBER LIKE '%" + number + "'", null);
            if (c.moveToFirst()) {
                do {
                    contacts.setName(c.getString(c.getColumnIndex("NAME")));
                    contacts.setIsActive(c.getInt(c.getColumnIndex("IS_ACTIVE")));
                } while (c.moveToNext());
            }
        }else{
           contacts.setName(number);
        }
        return contacts;

    }

    public static List<Contacts> getVersion(Context context, String contact_id) {
        return getContactsDao(context).queryBuilder().where(ContactsDao.Properties.
                DeviceContactID.eq(contact_id)).list();

    }

    public static void updateName(Context context, Contacts contacts) {
        // Cursor c = getContactsDao(context).getDatabase().rawQuery(
        // "UPDATE TABLE CONTACTS SET NAME = '"+name+"' WHERE DEVICE_CONTACT_ID = '"+contact_id
        // +"'", null);
        getContactsDao(context).update(contacts);
    }

}
