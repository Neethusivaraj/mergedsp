package com.akira.superplan.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.R;
import com.akira.superplan.adapters.CallFragmentAdapter;
import com.akira.superplan.dao.CallHistory;
import com.akira.superplan.dao.PopupAlerts;
import com.akira.superplan.repositories.HistoryRepo;
import com.akira.superplan.repositories.PopupAlertsRepo;
import com.akira.superplan.utils.Common;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nanda on 5/25/15.
 */
public class CallsFragment extends Fragment {

    private static final String TAG = "CallsFragment";
    private Context mContext;
    private ArrayList<HashMap<String, String>> mList;
    private View view;
    String lcost;
    private PopupAlerts mPopupAlerts;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_calls, container, false);
        populateListView();
        return view;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = DaoTestApplication.getInstance().getApplicationContext();

    }

    @Override
    public void onResume() {
        super.onResume();
        populateListView();
    }

    private String getCallTypeInChar(String type) {
        switch (type) {
            case Constants.MISSED_CALL:
                return "Missed";
            case Constants.RECEIVED_CALL:
                return "In";
            case Constants.OUTGOING_CALL:
                return "Out";
            default:
                return "X";

        }

    }

    private String getDurationInFormat(int duration) {
        int hours = duration / 3600;
        int minutes = (duration % 3600) / 60;
        int seconds = (duration % 3600) % 60;

        String strHours = String.valueOf(hours);
        String strMinutes = String.valueOf(minutes);
        String strSeconds = String.valueOf(seconds);

        String strDuration;

        if (hours != 0) {
            strDuration = strHours + "h " + strMinutes + "m " + strSeconds + "s";
        } else if (minutes != 0) {
            strDuration = strMinutes + "m " + strSeconds + "s";
        } else {
            strDuration = strSeconds + "s";
        }
        return strDuration;
    }

    private void populateListView() {
        String from = DaoTestApplication.getSharedPreferences().getString("From", "");
        List<CallHistory> histories;
        List<PopupAlerts> popupAlertsList = PopupAlertsRepo.getAllLogs(mContext);


        if (from.equals("Today")) {
            Date date = Common.getTodaysDate();
            histories = HistoryRepo.getHistorybyDate(getActivity(), date);
        } else if (from.equals("7days")) {
            Date date = Common.get7daysDate();
            histories = HistoryRepo.getHistorybyDate(getActivity(), date);
        } else if (from.equals("Yesterday")) {
            Date fromDate = Common.getYesterdaysDate();
            Date toDate = Common.getTodaysDate();
            histories = HistoryRepo.getYesterdaysLog(getActivity(), fromDate, toDate);

        } else {
            Date date = Common.get30daysDate();
            histories = HistoryRepo.getHistorybyDate(getActivity(), date);
        }
        ListView listView1 = (ListView) view.findViewById(R.id.listView1);
        mList = new ArrayList<HashMap<String, String>>();
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String contactName;


        for (CallHistory history : histories) {

         //   String text = history.getPopupText();
        //    Logger.d(TAG, "Histrory text : " + text);
            Logger.d(TAG, "History AlertId : " + history.getPopupalertId());

            if (history.getPopupalertId() != null) {

              //  Logger.d(TAG, "History Popup Text : " + HistoryRepo.getPopUpTextByID(mContext,
                //        history.getPopupalertId()).getPopupText());
                Logger.d(TAG, " fetching details from PopupAlert : "
                        + PopupAlertsRepo.getPopupTextByID(mContext,
                        history.getPopupalertId()).getPopupText());
                Logger.d(TAG, " fetching details from PopupAlert : "
                        + PopupAlertsRepo.getPopupTextByID(mContext,
                        history.getPopupalertId()).getCallCost());
                lcost = PopupAlertsRepo.getPopupTextByID(mContext,
                        history.getPopupalertId()).getCallCost();

            }


            HashMap<String, String> temp = new HashMap<String, String>();
            contactName = history.getName();
            if (contactName != null) {
                temp.put(Constants.FIRST_COLUMN, contactName);
            } else {
                temp.put(Constants.FIRST_COLUMN, history.getOtherNumber());
            }
            temp.put(Constants.SECOND_COLUMN, getCallTypeInChar(history.getType()));
            temp.put(Constants.THIRD_COLUMN, getDurationInFormat(history.getSeconds()));
            temp.put(Constants.FOURTH_COLUMN, history.getHappenedAt().toString());
            temp.put(Constants.FIFTH_COLUMN, history.getCallCategory());
            if (history.getPopupalertId() != null) {
                temp.put(Constants.SIXTH_COLUMN, lcost + " INR ");
            }


            if (history.getIsInNetwork().equals("true"))
                temp.put(Constants.SEVENTH_COLUMN, "InNetwork");
            else
                temp.put(Constants.SEVENTH_COLUMN, "OutNetwork");


            mList.add(temp);


        }
        CallFragmentAdapter adapter1 = new CallFragmentAdapter(this, mList);
        listView1.setAdapter(adapter1);
    }


}
