package com.akira.superplan.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.R;
import com.akira.superplan.SharedPreference;
import com.akira.superplan.api.ApiMethods;
import com.akira.superplan.dao.CircleDetails;
import com.akira.superplan.dao.NumberCodeDao;
import com.akira.superplan.dao.OperatorDetails;
import com.akira.superplan.model.GenerateOTP;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.EventBusHelper;
import com.akira.superplan.utils.Logger;
import com.akira.superplan.utils.Utils;
import com.squareup.okhttp.OkHttpClient;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

//import retrofit.GsonConverterFactory;

public class VerifyNumberFragment extends Fragment {
    private Button mContinueButton;
    private EditText mEditText;
    Context context = getActivity();
    public int count = 1;
    public NumberCodeDao numbercodedao;
    OperatorDetails mOperatorDetails;
    CircleDetails mCircleDetails;

    SharedPreferences sp = DaoTestApplication.getSharedPreferences();
    SharedPreferences.Editor editor = sp.edit();

    private String TAG = "SigninFargment";
    private SharedPreference sharedPreference;

    public NumberCodeDao getNumberCodedao(Context context) {

        return numbercodedao;
    }

    public VerifyNumberFragment() {
        super();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_verify_number, container, false);
        initUi(view);
        return view;
    }

    private void initUi(final View view) {
        if (getActivity() != null && view != null) {
            Utils.overrideFonts(getActivity(), view);
            mContinueButton = (Button) view.findViewById(R.id.continue_button);
            mEditText = (EditText) view.findViewById(R.id.phone_number_view);
            final ImageView imageView = (ImageView) view.findViewById(R.id.back_button);
            imageView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    EventBusHelper.postMessage(Constants.PERFORM_BACK_PRESS);
                }
            });
            mEditText = (EditText) view.findViewById(R.id.phone_number_view);

            mEditText.setText("  +91   ");
            Selection.setSelection(mEditText.getText(), mEditText.getText().length());
            mEditText.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!s.toString().startsWith("  +91   ")) {
                        mEditText.setText("  +91   ");
                        Selection.setSelection(mEditText.getText(), mEditText.getText().length());
                    }
                }
            });

            mContinueButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    if (validate()) {
                        EventBusHelper.postMessage(Constants.SHOW_AUTHENTICATING_POPUP);

                        mContinueButton.setEnabled(false);
                        mEditText.setEnabled(false);
                    } else {
                        return;
                    }
                }
            });
        }
    }

    final RequestInterceptor requestInterceptor = new RequestInterceptor() {
        public void intercept(RequestFacade request) {
            //String device_token = DaoTestApplication.getSharedPreferences().getString(Constants.DEVICE_TOKEN, "");
            //   String install_id = DaoTestApplication.getSharedPreferences().getString(Constants.INSTALLATION_ID,"");
            request.addHeader("Content-Type", "application/json");
            //  request.addHeader("Application-Id", app_token);
            //  request.addHeader("Device-Token",device_token);
            // request.addHeader("Installation-Id",install_id);
        }
    };


    public boolean validate() {
        boolean valid = true;
        String number = mEditText.getText().toString();
        SharedPreferences pref = getActivity().getPreferences(0);
        SharedPreferences.Editor edt = pref.edit();
        edt.putString("MyPhoneNumber", number);
        edt.commit();
        pref = getActivity().getPreferences(0);
        String id = pref.getString("MyPhoneNumber","empty");
        Log.d(TAG, "Shared preference working:" +id);
        Logger.d(TAG, "otp executing started!!");
        DaoTestApplication mainObj = DaoTestApplication.getInstance();
        mainObj.setPhoneNumber(number);
        mainObj.dumpAndSetFlag();
        //dumpNumberCodes();
        if (count == 1) {
            count++;
            OkHttpClient okHttpClient = new OkHttpClient();
            //okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
            okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constants.API_URL)
                    .setRequestInterceptor(requestInterceptor)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setClient(new OkClient(okHttpClient))
                    .build();
            ApiMethods methods = restAdapter.create(ApiMethods.class);

            HashMap<String, String> data = new HashMap<String, String>();
            data.put("mobile_number", "8870715891");


            GenerateOTP otpdetails = new GenerateOTP();
            sharedPreference = new SharedPreference();
            otpdetails.setData(data);
            otpdetails.setUserId("1");
            otpdetails.setEpoch("123456");
            otpdetails.setSignature("");
       /*  methods.putGenerateOTP(otpdetails, new Callback<GenerateOTP>() {

             @Override
             public void success(GenerateOTP generateOTP, retrofit.client.Response response) {
                 Logger.d(TAG,"otp send id1:"+generateOTP.getId());
                 SharedPreferences sp = DaoTestApplication.getSharedPreferences();
                 SharedPreferences.Editor editor = sp.edit();
                 editor.putString("optId",generateOTP.getId());
                 editor.commit();

             }

             @Override

                public void failure(RetrofitError error) {
                    Logger.d(TAG, " api details error " + error.getUrl());
                    //  Logger.d(TAG, " api details error " + error.getBody());
                    Logger.d(TAG, " api details error " + error.getLocalizedMessage());
                }
            });   */
        }

        if (number.isEmpty() || number.length() < 18 || number.length() > 18) {
            mEditText.setError("Incorrect Number");
            valid = false;
        } else {
            mEditText.setError(null);
        }


        if (number.length() == 10) {
            boolean isReady = number.length() > 9;
            if (isReady) {
                String numb = number.substring(0, 4);
                Logger.d(TAG, " " + numb);
                //SharedPreferences preferences = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
                mEditText.getBackground().setColorFilter(getResources().getColor(R.color.blue), PorterDuff.Mode.SRC_ATOP);
              //  sharedPreference = new SharedPreference();
             //   sharedPreference.save(context, number);


            }
            return isReady;


        }
        return valid;
    }
}

