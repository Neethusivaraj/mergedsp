package com.akira.superplan.fragments;

import android.animation.ArgbEvaluator;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akira.superplan.R;
import com.akira.superplan.adapters.CustomViewPager;
import com.akira.superplan.adapters.TourViewPageAdapter;
import com.viewpagerindicator.CirclePageIndicator;

public class TourFragment extends Fragment {

    public TourFragment() {
        super();
    }
    Integer[] colors = null;
    ViewPager viewPager;
    TourViewPageAdapter adapter;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_tour, container, false);
        initUI(view);
        return view;
    }

    private void initUI(final View view) {
        if (view != null) {
            viewPager = (ViewPager) view.findViewById(R.id.tour_screens);
            adapter = new TourViewPageAdapter(getActivity());
            viewPager.setAdapter(adapter);

            final CirclePageIndicator indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
            indicator.setPageColor(Color.GRAY);
            indicator.setFillColor(Color.WHITE);
            indicator.setViewPager(viewPager);
            indicator.setOnPageChangeListener(new CustomOnPageChangeListener());
            setUpColors();
        }

    }

    private void setUpColors(){

        Integer color1 = getResources().getColor(R.color.blue);
        Integer color2 = getResources().getColor(R.color.red);
        Integer color3 = getResources().getColor(R.color.yellow);
        Integer color4 = getResources().getColor(R.color.gray);

        Integer[] colors_temp = {color1, color2, color3, color4};
        colors = colors_temp;

    }

    private class CustomOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            if (position < (adapter.getCount() - 1) && position < (colors.length - 1)) {
                viewPager.setBackgroundColor((Integer) argbEvaluator.evaluate(positionOffset, colors[position], colors[position + 1]));
            } else {
                viewPager.setBackgroundColor(colors[colors.length - 1]);
            }
        }

        @Override
        public void onPageSelected(int position) {
        }

        @Override
        public void onPageScrollStateChanged(int i) {
        }

    }
}
