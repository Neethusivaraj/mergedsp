package com.akira.superplan.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.R;
import com.akira.superplan.adapters.DataLogFragmentAdapter;
import com.akira.superplan.dao.DataLog;
import com.akira.superplan.repositories.DataLogRepo;
import com.akira.superplan.utils.Common;
import com.akira.superplan.utils.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class DataLogFragment extends Fragment {

    View view;
    private ArrayList<HashMap<String, String>> list;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_messages, container, false);
        populateListView();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        populateListView();
    }

    private void populateListView() {

        String from= DaoTestApplication.getSharedPreferences().getString("From", "");
        List<DataLog> logs;
        if(from.equals("Today"))
        {
            Date date = Common.getTodaysDate();
            logs = DataLogRepo.getLogbyDate(getActivity(), date);
        }
        else if(from.equals("7days")) {
            Date date = Common.get7daysDate();
            logs = DataLogRepo.getLogbyDate(getActivity(), date);
        }
        else if(from.equals("Yesterday"))
        {
            Date fromDate=Common.getYesterdaysDate();
            Date toDate=Common.getTodaysDate();
            logs = DataLogRepo.getYesterdaysLog(getActivity(), fromDate, toDate);

        }
        else {
            Date date = Common.get30daysDate();
            logs = DataLogRepo.getLogbyDate(getActivity(), date);
        }

        ListView listView = (ListView) view.findViewById(R.id.listViewMessages);
        list = new ArrayList<HashMap<String, String>>();
        for (DataLog log : logs) {
            HashMap<String, String> temp = new HashMap<String, String>();

            temp.put(Constants.TOTAL_RX_BYTES, DataLogRepo.convertDataFromLong(log.getRx()).toString());
            temp.put(Constants.TOTAL_TX_BYTES, DataLogRepo.convertDataFromLong(log.getTx()).toString());
            temp.put(Constants.COL_TYPE, log.getType());
            Date lHappenedAt = log.getHappenedAt();
            temp.put(Constants.COL_HAPPENED_AT, (lHappenedAt == null ? "" : lHappenedAt.toString()));
            list.add(temp);
        }
        DataLogFragmentAdapter adapter = new DataLogFragmentAdapter(this, list);
        listView.setAdapter(adapter);
    }
}
