package com.akira.superplan.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.akira.superplan.R;
import com.akira.superplan.adapters.MyPagerAdapter;
import com.akira.superplan.utils.Constants;
import com.astuetz.PagerSlidingTabStrip;


public class SummaryActivity extends ActionBarActivity {

    private static final String TAG = "SummaryActivity";

    public Button b;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_summary);
            b=(Button)findViewById(R.id.button1);
            //   TextView tvPhoneNumber = (TextView) findViewById(R.id.tvPhoneNumber);
            //   tvPhoneNumber.setText("Your number is - " + DaoTestApplication.getInstance()
            //         .getMyPhoneNumber());

            ViewPager pager = (ViewPager) findViewById(R.id.pager);
            pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

            // Bind the tabs to the ViewPager
            PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
            tabs.setViewPager(pager);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent go=new Intent(getApplicationContext(),StatusAct.class);
                    startActivity(go);

                }
            });

        }

        @Override
        protected void onResume() {
            super.onResume();
        }

        private String getCallTypeInChar(String type) {
            switch (type) {
                case Constants.MISSED_CALL:
                    return "Missed";
                case Constants.RECEIVED_CALL:
                    return "In";
                case Constants.OUTGOING_CALL:
                    return "Out";
                default:
                    return "X";
            }
        }

        private String getDurationInFormat(int duration) {
            int hours = duration / 3600;
            int minutes = (duration % 3600) / 60;
            int seconds = (duration % 3600) % 60;

            String strHours = String.valueOf(hours);
            String strMinutes = String.valueOf(minutes);
            String strSeconds = String.valueOf(seconds);

            String strDuration;

            if (hours != 0) {
                strDuration = strHours + "h " + strMinutes + "m " + strSeconds + "s";
            } else if (minutes != 0) {
                strDuration = strMinutes + "m " + strSeconds + "s";
            } else {
                strDuration = strSeconds + "s";
            }
            return strDuration;
        }



        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement


            return super.onOptionsItemSelected(item);
        }
    }

