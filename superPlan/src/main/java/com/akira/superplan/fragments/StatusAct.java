package com.akira.superplan.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.R;
import com.akira.superplan.repositories.DataLogRepo;
import com.akira.superplan.repositories.HistoryRepo;
import com.akira.superplan.repositories.MessageHistoryRepo;
import com.akira.superplan.repositories.UssdCodeRepo;
import com.akira.superplan.utils.Connectivity;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.Logger;

import java.util.Date;

public class StatusAct extends ActionBarActivity {
    Button b;
    private static final String TAG = "SummaryActivity";

    private Context mContext;
    private boolean status;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = DaoTestApplication.getInstance().getApplicationContext();

        Logger.d(TAG, "Accessibility : " + status);
        setContentView(R.layout.finalsummary);
        b=(Button)findViewById(R.id.button21);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newinten=new Intent(getApplicationContext(),sampleHistoryview.class);
                startActivity(newinten);
            }
        });

        Logger.d (TAG, "GET all USSD Codes : " + UssdCodeRepo.getAllussdCodes(mContext).toString());

        Logger.d(TAG, "Check of Internet Connection");
        DataLogRepo.calculateDataUsageSummary(this);
        HistoryRepo.calculateDuration(this);
         HistoryRepo.getFrequentList(this);
//        } else {

//            Common.showNoInternetToast(this);
//
//        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        setSummary();
        getUserBalance();
    }

    private void getUserBalance() {
//        List<userBalance> list = UserBalanceRepo.getAllBalance(getApplicationContext());
//        if (list.size() != 0) {
//            Logger.d(TAG, "reliance" + list.get(0).getCurrentBalance()
//                    + " " + list.get(0).getCallValidity());
//        } else {
//            Logger.d("TAG", "reliance");
//        }
        NetworkInfo networkInfo = Connectivity.getNetworkInfo(getApplicationContext());
        if (networkInfo != null && networkInfo.isConnected()) {
            String networkType = Connectivity.isConnectionFast(
                    networkInfo.getType(), networkInfo.getSubtype());
            //Logger.d(TAG, "net " + networkType);
        }
    }

    private void setSummary() {
        StringBuffer sb = new StringBuffer();
        sb.append("Your Number is - " + DaoTestApplication.getInstance().getMyPhoneNumber());
        sb.append("\nYour Operator is - " + DaoTestApplication.getInstance().getMyOperator());
        sb.append("\nYour Device type is - " + getPhoneType() + " sim ");
        sb.append("\nYour USSD Service Status is - " + ((DaoTestApplication.getInstance()
                .isAccessibilityEnabled(this, "in.co.akira.daotestapplication/"
                        + ".services.NotificationAccessibilityService"))
                ? "running" : "not running"));
        sb.append("\nRoaming Status - " + DaoTestApplication.getInstance().getSim1RoamingStatus() +
                " Location " + DaoTestApplication.getSharedPreferences().getString("Location", ""));

        sb.append("\n\nlandline - " + DaoTestApplication.getSharedPreferences()
                .getString("landline", "wait"));

        sb.append("\n\nTotal - " + HistoryRepo.getCount(this));
        sb.append("\nIncoming - " + HistoryRepo.getIncomingCount(this)
                + "/" + HistoryRepo.totalIn + " secs");
        sb.append("\nOutgoing - " + HistoryRepo.getOutgoingCount(this)
                + "/" + HistoryRepo.totalOut + " secs");
        sb.append("\nMissed - " + HistoryRepo.getMissedCount(this));
        sb.append("\nInNetworkCalls - " + HistoryRepo.getInNetworkCount(this));
        sb.append("\nOutNetworkCalls - " + HistoryRepo.getOutNetworkCount(this));
        sb.append("\nLocal calls - " + HistoryRepo.getLocalCount(this));
        sb.append("\nStd calls - " + HistoryRepo.getStdCount(this));
        sb.append("\nISD calls - " + HistoryRepo.getIsdCount(this));
        sb.append("\nInvalid calls - " + HistoryRepo.getInvalidCount(this));

        sb.append("\n");

        sb.append("\nSMS Total - " + MessageHistoryRepo.getCount(this));
        sb.append("\nSMS Sent - " + MessageHistoryRepo.getSentCount(this));
        sb.append("\nSMS Received - " + MessageHistoryRepo.getReceivedCount(this));
        sb.append("\n");

        sb.append("\nTotal Rx - " + DataLogRepo.getTotalRx(this));
        sb.append("\nTotal Tx - " + DataLogRepo.getTotalTx(this));

        sb.append("\nWiFi Rx - " + DataLogRepo.getWifiRx(this));
        sb.append("\nWiFi Tx - " + DataLogRepo.getWifiTx(this));

        sb.append("\nMobile Rx - " + DataLogRepo.getMobileRx(this));
        sb.append("\nMobile Tx - " + DataLogRepo.getMobileTx(this));
        sb.append("\n");
        Date timeNow = new Date(System.currentTimeMillis());
        sb.append("\nCurrent network status - " + DaoTestApplication.getSharedPreferences()
                .getString(Constants.CONNECTIVITY, Constants.CONNECTED_UNKNOWN));
        sb.append("\nServer sync status - " + DaoTestApplication.getSharedPreferences()
                .getString(Constants.LAST_SERVER_SYNC_STATUS, "Yet to begin sync"));
        sb.append("\nUpdated at - " + DaoTestApplication.getSharedPreferences()
                .getString(Constants.LAST_SERVER_SYNC_TIME, timeNow.toString()));
        TextView tvSummary = (TextView) findViewById(R.id.txtSummary);
        tvSummary.setText(sb);
        showUssd();
    }


    private void showUssd() {
       /* List<ussd_codes> codes = UssdCodeRepo.getAllussdCodes(getApplicationContext());

        for (ussd_codes ussdCodes : codes) {
            Log.d("ussdcodes", ussdCodes.getOperatorcode() + " " + ussdCodes.getBalance_2g()
                    + " " + ussdCodes.getBalance_3g());
        }*/

        // String number="4295233466";
        String number = "8049653429";
        //Logger.d(TAG, LandlineCodesRepo.getMobileCircle(getApplicationContext(), number));
    }

    private String getPhoneType() {
        String lSimType;
        SharedPreferences settings = getSharedPreferences("SimType", 0);
        lSimType = settings.getString("simtype", "");
        return lSimType;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public void showMainActivity(View view) {
        this.startActivity(new Intent(this, SummaryActivity.class));
    }



}
