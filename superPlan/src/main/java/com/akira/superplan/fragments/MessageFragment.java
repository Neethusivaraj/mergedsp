package com.akira.superplan.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.R;
import com.akira.superplan.adapters.MessageFragmentAdapter;
import com.akira.superplan.dao.MessageHistory;
import com.akira.superplan.repositories.MessageHistoryRepo;
import com.akira.superplan.utils.Common;
import com.akira.superplan.utils.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MessageFragment extends Fragment {

    View view;
    private ArrayList<HashMap<String, String>> list;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_messages, container, false);
        populateListView();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        populateListView();
    }

    private void populateListView() {
        String from= DaoTestApplication.getSharedPreferences().getString("From", "");
        List<MessageHistory> histories;
        if(from.equals("Today"))
        {
            Date date = Common.getTodaysDate();
            histories = MessageHistoryRepo.getHistorybyDate(getActivity(), date);
        }
        else if(from.equals("7days")) {
            Date date = Common.get7daysDate();
            histories = MessageHistoryRepo.getHistorybyDate(getActivity(), date);
        }
        else if(from.equals("Yesterday"))
        {
            Date fromDate=Common.getYesterdaysDate();
            Date toDate=Common.getTodaysDate();
            histories = MessageHistoryRepo.getYesterdaysLog(getActivity(), fromDate, toDate);

        }
        else {
            Date date = Common.get30daysDate();
            histories = MessageHistoryRepo.getHistorybyDate(getActivity(), date);
        }
        ListView listView = (ListView) view.findViewById(R.id.listViewMessages);
        list = new ArrayList<HashMap<String, String>>();
        for (MessageHistory history : histories) {
            HashMap<String, String> temp = new HashMap<String, String>();
            temp.put(Constants.COL_NUMBER, history.getOtherNumber());
            temp.put(Constants.COL_TYPE, history.getType());
            temp.put(Constants.COL_HAPPENED_AT, history.getHappenedAt().toString());
            temp.put(Constants.COL_MESSAGE_LENGTH, history.getMessageLength().toString());
            list.add(temp);
        }
        MessageFragmentAdapter adapter = new MessageFragmentAdapter(this, list);
        listView.setAdapter(adapter);
    }
}
