package com.akira.superplan.utils;

public class Constants {
    public static final String OPEN_CREATE_PROFILE = "OPEN_CREATE_PROFILE";
    public static String PERFORM_BACK_PRESS = "PERFORM_BACK_PRESS";
    public static String SHOW_EMAILS_POPUP = "SHOW_EMAILS_POPUP";
    public static String SHOW_AUTHENTICATING_POPUP = "SHOW_AUTHENTICATING_POPUP";

    public static String OPEN_VERIFY_NUMBER_SCREEN = "OPEN_VERIFY_NUMBER_SCREEN";
    public static int AUTHENTICATING_POPUP_TIME = 10000;
    public static final String SHARED_PREFS_KEY = "phoneiq.shared_prefs";

    public static final String SQLITE_DB_NAME = "quarks.sqlite";
    public static final String IS_DUMPED = "isDumped";
    public static final String MY_NUMBER_FLAG = "myNumberFlag";
    public static final String MY_NUMBER = "MY_NUMBER";
    public static final String MY_OPERATOR = "MY_OPERATOR";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String INSTALLATION_ID = "install_id";

    public static final boolean FALSE = false;
    public static final boolean TRUE = true;

//    public static final String AZURE_URL = "https://phoneiq.azure-mobile.net";
//    public static final String AZURE_APP_KEY = "gJdDYGXIoyPyLOigbxAVUiDkXbbwUf18";

    public static final String AZURE_URL = "https://totemiqv04.azure-mobile.net/";
    public static final String AZURE_APP_KEY = "dycuSOGoIVqCNhnSWGJTQRZerInmZR67";

    public static final String MISSED_CALL = "Missed";
    public static final String RECEIVED_CALL = "Incoming";
    public static final String OUTGOING_CALL = "Outgoing";
    public static final String BLOCKED_CALL = "Blocked";

    public static final String FIRST_COLUMN = "First";
    public static final String SECOND_COLUMN = "Second";
    public static final String THIRD_COLUMN = "Third";
    public static final String FOURTH_COLUMN = "Fourth";
    public static final String FIFTH_COLUMN = "Fifth";
    public static final String SIXTH_COLUMN = "Sixth";
    public static final String SEVENTH_COLUMN = "Seventh";

    public static final String COL_NUMBER = "NC";
    public static final String COL_TYPE = "TC";
    public static final String COL_HAPPENED_AT = "HC";
    public static final String COL_APP_NAME = "AN";
    public static final String COL_PACKAGE_NAME = "PN";
    public static final String COL_RECEIVED_BYTES = "RB";
    public static final String COL_SENT_BYTES = "SB";
    public static final String COL_MESSAGE_LENGTH = "ML";


    public static final String STATUS_UNSENT = "UNSENT";
    public static final String STATUS_SENDING = "SENDING";
    public static final String STATUS_SENT = "SENT";

    public static final String CURRENT_CALL_LOG_ID = "CURRENT_CALL_LOG_ID";
    public static final String CURRENT_MESSAGE_LOG_ID = "CURRENT_MESSAGE_LOG_ID";

    public static final String SMS_SENT = "SMS_SENT";
    public static final String SMS_RECEIVED = "SMS_RECEIVED";

    public static final int SERVER_SLEEP_TIME = 0;
    public static final int NUMBER_OF_TABS = 3;

    public static final String TOTAL_RX_BYTES = "TOTALRXBYTES";
    public static final String TOTAL_TX_BYTES = "TOTALTXBYTES";
    public static final String MOBILERxBYTES = "MOBILERXBYTES";
    public static final String MOBILETxBYTES = "MOBILETXBYTES";
    public static final String WIFIRxBYTES = "WIFIRXBYTES";
    public static final String WIFITxBYTES = "WIFITXBYTES";

    public static final String CONNECTIVITY = "CONNECTIVITY";
    public static final String WIFI_TYPE = "WIFI";
    public static final String MOBILE_TYPE = "MOBILE";
    public static final String CONNECTED_UNKNOWN = "Yet to find";

    public static final String NOT_CONNECTED = "NOT_CONNECTED";
    public static final String WIFI_CONNECTED = "WIFI_CONNECTED";
    public static final String MOBILE_CONNECTED = "MOBILE_CONNECTED";
    public static final String WIFI_DISCONNECTED = "WIFI_DISCONNECTED";
    public static final String MOBILE_DISCONNECTED = "MOBILE_DISCONNECTED";

    public static final int REPEAT_TIME = 3600 * 1000;    // 1 hour
    public static final int SERVER_SYNC_TIME = 7200 * 1000; // 2 hours
    // public static final int SERVER_SYNC_TIME = 720 * 100; // 2 hours

    public static final String LAST_SERVER_SYNC_STATUS = "last_server_sync_status";
    public static final String LAST_SERVER_SYNC_TIME = "last_server_sync_time";

    public static final String POPUP_INSERTED_AT = "PopupInsertedAt";
    public static final String LOG_INSERTED_AT = "LogInsertedAt";

    public static final Long USSD_DELAY = 8L;

    public static final String USSD_CALLED_AT = " UssdCalledAt";

    public static final String LAST_SERVER_SYNC_CALLID = "last_server_sync_callId";
    public static final String LAST_SERVER_SYNC_MSGID = "last_server_sync_msgID";

    public static final String LAST_SERVER_SYNC_OP_MSGID = "last_server_sync_opMsgId";
    public static final String MISSED_USSD_COUNT = "missed_ussd_count";

    public static final String API_URL = "http://akiralinuxvm-12.cloudapp.net";

    // public static final String API_URL = "http://akiralinuxvm-2a.trafficmanager.net";
    // public static final String API_URL2 = "https://protected-sands-3738.herokuapp.com";

    public static final String PREF_FILE_NAME = "UserProfile";

    public static final int SERVER_SEND_LIMIT = 50;

}
