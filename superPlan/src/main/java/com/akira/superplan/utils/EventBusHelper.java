package com.akira.superplan.utils;

import de.greenrobot.event.EventBus;

public class EventBusHelper {

	public static void postMessage(final Object message) {
		postMessage(message, false);
	}

	public static void postMessage(final Object message, final boolean sticky) {
		if (sticky) {
			getEventBus().postSticky(message);
		} else {
			getEventBus().post(message);
		}
	}

	public static void registerSubscriber(final Object subscriber) {
		if (!getEventBus().isRegistered(subscriber)) {
			getEventBus().registerSticky(subscriber);
		}
	}

	public static void unregisterSubscriber(final Object subscriber) {
		if (getEventBus().isRegistered(subscriber)) {
			getEventBus().unregister(subscriber);
		}
	}

	private static EventBus getEventBus() {
		return EventBus.getDefault();
	}

	public static void clearMessage(final Object message) {
		getEventBus().removeStickyEvent(message);
	}
}
