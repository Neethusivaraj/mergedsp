package com.akira.superplan.utils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by LENOVO on 11/16/2015.
 */
public class DeviceDetails {

    @SerializedName("id")
    public int id;
    @SerializedName("mobile_number")
    public String mobileNumber;
    @SerializedName("device_id")
    public String deviceId;
    @SerializedName("device_os_version")
    public String deviceOsVersion;
    @SerializedName("device_manufacturer")
    public String deviceManufacturer;
    @SerializedName("device_model")
    public String deviceModel;
    @SerializedName("app_identifier")
    public String appIdentifier;
    @SerializedName("app_name")
    public String appName;
    @SerializedName("app_version")
    public String appVersion;
    @SerializedName("gcm_token")
    public String gcmToken;
    @SerializedName("gcm_token_last_modified")
    public String gcmTokenLastModified;
    @SerializedName("installation_id")
    public String installationId;
    @SerializedName("device_token")
    public String deviceToken;
    @SerializedName("current_status")
    public String currentStatus;
    @SerializedName("is_active")
    public String isActive;


}
