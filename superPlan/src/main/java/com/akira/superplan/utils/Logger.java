package com.akira.superplan.utils;

import android.util.Log;

public class Logger {

	private static final String AUPER_PLAN_LOGGER = "AUPER_PLAN_LOGGER";
	private static final boolean IS_DEBUG_ENABLED = true;

	private Logger() {
		super();
	}
	private static final boolean ENABLE_LOGGING = true;

	public static void d(String tag, String msg) {
		if (ENABLE_LOGGING) {
			Log.d(tag, msg);
		}
	}

	public static void e(String tag, String msg) {
		if (ENABLE_LOGGING) {
			Log.e(tag, msg);
		}

	}

	public static void w(String tag, String msg) {
		if (ENABLE_LOGGING) {
			Log.w(tag, msg);
		}

	}

	public static void i(String tag, String msg) {
		if (ENABLE_LOGGING) {
			Log.i(tag, msg);
		}

	}

	public static void v(String tag, String msg) {
		if (ENABLE_LOGGING) {
			Log.v(tag, msg);
		}

	}

	public static void temp(String tag, String msg) {
		Log.v(tag, msg);

	}


	public static void log(final String message, final Exception e) {
		if (IS_DEBUG_ENABLED) {
			if (e != null) {
				System.err.println(AUPER_PLAN_LOGGER + message
						+ e.getLocalizedMessage());
			} else {
				System.err.println(AUPER_PLAN_LOGGER + message);

			}
		}
	}

}
