package com.akira.superplan.utils;


import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;

import com.akira.superplan.dao.WifiList;
import com.akira.superplan.repositories.WifiListRepo;
import com.akira.superplan.services.LocationService;

import java.util.List;

/**
 * Check device's network connectivity and speed.
 */

public class Connectivity {

    List<ScanResult> wifiList;
    private final static String TAG = "Connectivity";

    /**
     * Get the network info.
     */
    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    /**
     * Check if there is any connectivity.
     */
    public static boolean isConnected(Context context) {
        NetworkInfo info = Connectivity.getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    /**
     * Check if there is any connectivity to a Wifi network
     *
     * @param context //     * @param type
     * @return
     */
    public static boolean isConnectedWifi(Context context) {
        NetworkInfo info = Connectivity.getNetworkInfo(context);
        return (info != null && info.isConnected()
                && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    /**
     * Check if there is any connectivity to a mobile network.
     */

    public static boolean isConnectedMobile(Context context) {
        NetworkInfo info = Connectivity.getNetworkInfo(context);
        return (info != null && info.isConnected()
                && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    /**
     * Check if there is fast connectivity.
     */
   /* public static boolean isConnectedFast(Context context){
        NetworkInfo info = Connectivity.getNetworkInfo(context);
        return (info != null && info.isConnected() &&
         Connectivity.isConnectionFast(info.getType(),info.getSubtype()));
    }*/

    /**
     * Get the current state of the connectivity.
     */
    public static String getCurrentNetworkStatus(Context context) {
        if (isConnectedMobile(context)) {
            return Constants.MOBILE_CONNECTED;
        } else if (isConnectedWifi(context)) {
            //getWifiInfo(context);
            return Constants.WIFI_CONNECTED;
        }
        return Constants.NOT_CONNECTED;
    }

    /**
     * Get the current network type. Returns mobile or wifi.
     */
    public static String getNetworkType(Context context) {
        if (isConnectedMobile(context))
            return Constants.MOBILE_TYPE;
        else if (isConnectedWifi(context)) {
            getWifiInfo(context);
            return Constants.WIFI_TYPE;
        }
        return Constants.NOT_CONNECTED;
    }

    /**
     * Check if the connection is fast.
     */
    public static String isConnectionFast(int type, int subType) {
        if (type == ConnectivityManager.TYPE_WIFI) {
            return "wifi";
        } else if (type == ConnectivityManager.TYPE_MOBILE) {
            switch (subType) {
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    return "2g"; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    return "2g"; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return "2g"; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    return "3g"; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    return "3g"; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return "2g"; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    return "4g"; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    return "3g"; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return "4g"; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    return "3g"; // ~ 400-7000 kbps
            /*
             * Above API level 7, make sure to set android:targetSdkVersion
			 * to appropriate level to use these.
			 */
                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                    return "4g"; // ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                    return "4g"; // ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                    return "4g"; // ~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    return "2g"; // ~25 kbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                    return "4g"; // ~ 10+ Mbps
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return "unknown";
            }
        } else {
            return "unknown";
        }
    }


    public static void getCurrentAddress(Context context, WifiList wifiList) {

        LocationService mLocationService = new LocationService(context);
        Location location = mLocationService
                .getLocation(LocationManager.NETWORK_PROVIDER);

        if (location != null) {
            wifiList.setLatitude(location.getLatitude());
            Logger.d(TAG, location.getLatitude() + " " + location.getLongitude());
            wifiList.setLongitude(location.getLongitude());
        } else {
            Logger.d(TAG, "Unable to find Location");
        }


    }

    public static String getWifiInfo(Context context) {

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        WifiInfo info = wifiManager.getConnectionInfo();
        String textStatus = "";
        textStatus += "\n\n Current WiFi Status: " + info.toString();
        String lBSSID = info.getBSSID();
        String MAC = info.getMacAddress();
        NetworkInfo networkInfo = Connectivity.getNetworkInfo(context);
        String networkType = Connectivity.isConnectionFast(
                networkInfo.getType(), networkInfo.getSubtype());
        WifiList wifiList = new WifiList();
        wifiList.setWifiResponseId((long) 0);
        wifiList.setSSID(lBSSID);
        wifiList.setWifiName(networkInfo.getExtraInfo());
        wifiList.setConnectionEstablishedTime(Common.getTime());
        getCurrentAddress(context, wifiList);
        WifiListRepo.insertOrUpdate(context, wifiList);
        Logger.d(TAG, "ssid " + wifiList.getSSID() + "name " + wifiList.getWifiName()
                + " latitude " + wifiList.getLatitude() + " longitude " + wifiList.getLongitude()
                + " connection start time " + wifiList.getConnectionEstablishedTime());

        return textStatus;
    }


}


//class WifiReceiver extends BroadcastReceiver {
//    public void onReceive(Context c, Intent intent) {
//        sb = new StringBuilder();
//        wifiList = mainWifi.getScanResults();
//        for(int i = 0; i < wifiList.size(); i++){
//            sb.append(new Integer(i+1).toString() + ".");
//            sb.append((wifiList.get(i)).toString());
//            sb.append("\n");
//        }
//        Logger.d(TAG,"List : "+sb);
//    }
//}
