package com.akira.superplan.calls;

import android.content.Context;
import android.content.SharedPreferences;

import com.akira.superplan.DaoTestApplication;

import java.util.Date;

public class CallReceiver extends PhoneCallReceiver {

    @Override
    protected void onIncomingCallStarted(Context context, String number, Date start) {
    }

    @Override
    protected void onOutgoingCallStarted(Context context, String number, Date start) {
    }

    @Override
    protected void onIncomingCallEnded(Context context, String number, Date start, Date end) {
//        createHistoryRecord(context, number, start, end, Constants.RECEIVED_CALL);
    }

    @Override
    protected void onOutgoingCallEnded(Context context, String number, Date start, Date end) {
//        createHistoryRecord(context, number, start, end, Constants.OUTGOING_CALL);
    }

    @Override
    protected void onMissedCall(Context context, String number, Date start) {
//        createHistoryRecord(context, number, start, start, Constants.MISSED_CALL);
    }

   private void createHistoryRecord(Context context, String number, Date start, Date end, String type) {
      //  CallHistoryBC record = new CallHistoryBC();
        int duration;
        duration = (int) (end.getTime() - start.getTime()) / 1000;

      //  record.setMyNumber(DaoTestApplication.getInstance().getMyPhoneNumber());
      //  record.setOtherNumber(number);
      //  record.setType(type);
      //  record.setHappenedAt(start);
      //  record.setSeconds(duration);
      //  record.setStatus(Constants.STATUS_UNSENT);
      //  record.setRoaming(DaoTestApplication.getInstance().getSim1RoamingStatus());

        //CallHistoryBCRepo.insertOrUpdate(context, record);

        SharedPreferences sp = DaoTestApplication.getSharedPreferences();
        SharedPreferences.Editor spEdit = sp.edit();
        spEdit.putLong("BCLogInsertedAt", System.currentTimeMillis());
        spEdit.commit();

    }
}