package com.akira.superplan.calls;

import android.content.Context;
import android.telephony.TelephonyManager;

import java.lang.reflect.Method;

/**
 * Created by LENOVO on 8/13/2015.
 */
public final class TelephonyInfo {

    private static TelephonyInfo telephonyInfo;
    private String imeiSIM1;
    private String imeiSIM2;
    private boolean isSIM1Ready;
    private boolean isSIM2Ready;
    private boolean isSIM1Roaming;
    private boolean isSIM2Roaming;

    public boolean isSIM1Roaming() {
        return isSIM1Roaming;
    }

    public boolean isSIM2Roaming() {
        return isSIM2Roaming;
    }

    public String getImeiSIM1() {
        return imeiSIM1;
    }

    public String getImeiSIM2() {
        return imeiSIM2;
    }

    public boolean isSIM1Ready() {
        return isSIM1Ready;
    }

    public boolean isSIM2Ready() {
        return isSIM2Ready;
    }

    public boolean isDualSIM() {
        return imeiSIM2 != null;
    }

    private TelephonyInfo() {
    }

    public static TelephonyInfo getInstance(Context context) {

        if (telephonyInfo == null) {

            telephonyInfo = new TelephonyInfo();

            TelephonyManager telephonyManager = ((TelephonyManager) context.getSystemService(
                    Context.TELEPHONY_SERVICE));

            telephonyInfo.imeiSIM1 = telephonyManager.getDeviceId();
            telephonyInfo.imeiSIM2 = null;
            try {
                telephonyInfo.imeiSIM1 = getDeviceIdBySlot(context, "getDeviceId", 0);
                telephonyInfo.imeiSIM2 = getDeviceIdBySlot(context, "getDeviceId", 1);
            } catch (Exception e1) {
                e1.printStackTrace();
            }


            telephonyInfo.isSIM1Ready = telephonyManager.getSimState() == TelephonyManager.
                    SIM_STATE_READY;
            telephonyInfo.isSIM2Ready = false;

            try {
                telephonyInfo.isSIM1Ready = getSIMStateBySlot(context, "getSimState", 0);
                telephonyInfo.isSIM2Ready = getSIMStateBySlot(context, "getSimState", 1);

            } catch (Exception e1) {
                e1.printStackTrace();
            }


            telephonyInfo.isSIM1Roaming = telephonyManager.isNetworkRoaming();
            telephonyInfo.isSIM2Roaming = false;

            try {
                telephonyInfo.isSIM1Roaming = getRoamingStateBySlot(context, "isNetworkRoaming", 0);
                telephonyInfo.isSIM2Roaming = getRoamingStateBySlot(context, "isNetworkRoaming", 1);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

        }

        return telephonyInfo;
    }

    private static String getDeviceIdBySlot(Context context, String predictedMethodName,
                                            int slotID) {

        String imsi = null;

        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        try {

            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());

            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getSimID = telephonyClass.getMethod(predictedMethodName, parameter);

            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            Object lObjPhone = getSimID.invoke(telephony, obParameter);

            if (lObjPhone != null) {
                imsi = lObjPhone.toString();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imsi;
    }

    private static boolean getSIMStateBySlot(Context context, String predictedMethodName, int slotID) {

        boolean isReady = false;

        TelephonyManager telephony = (TelephonyManager) context.getSystemService(
                Context.TELEPHONY_SERVICE);

        try {

            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());

            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getSimStateGemini = telephonyClass.getMethod(predictedMethodName, parameter);

            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            Object ob_phone = getSimStateGemini.invoke(telephony, obParameter);

            if (ob_phone != null) {
                int simState = Integer.parseInt(ob_phone.toString());
                if (simState == TelephonyManager.SIM_STATE_READY) {
                    isReady = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isReady;
    }

    private static boolean getRoamingStateBySlot(Context context,
                                                 String predictedMethodName, int slotID) {

        boolean isRoaming = false;

        TelephonyManager telephony = (TelephonyManager) context.getSystemService(
                Context.TELEPHONY_SERVICE);

        try {

            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());

            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getRoamingStateGemini = telephonyClass.getMethod(predictedMethodName, parameter);

            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            Object lObjPhone = getRoamingStateGemini.invoke(telephony, obParameter);

            if (lObjPhone != null) {
                if (lObjPhone.toString().equals("true")) {
                    isRoaming = true;
                } else {
                    isRoaming = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isRoaming;
    }


}
