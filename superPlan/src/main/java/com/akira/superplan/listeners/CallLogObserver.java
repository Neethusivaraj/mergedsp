package com.akira.superplan.listeners;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.CallLog;
import android.util.Log;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.CallHistory;
import com.akira.superplan.dao.Contacts;
import com.akira.superplan.dao.PopupAlerts;
import com.akira.superplan.repositories.ContactsRepo;
import com.akira.superplan.repositories.CountryCodesRepo;
import com.akira.superplan.repositories.HistoryRepo;
import com.akira.superplan.repositories.LandlineCodesRepo;
import com.akira.superplan.repositories.NumberCodesRepo;
import com.akira.superplan.repositories.PopupAlertsRepo;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.Logger;

import java.util.Date;

//import com.akira.superplan.calls.LocalSTDCallFinder;


/**
 * Created by nanda on 5/12/15.
 */
public class CallLogObserver extends ContentObserver {

    private final static String TAG = "CallLogObserver";
    private Handler mHandler;
    private Context context;

    private static int counter = 0;
    private static int receivedCounter = 0;

    public CallLogObserver(Handler handler, Context context) {
        super(handler);
        this.mHandler = handler;
        this.context = context;
    }

    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Log.d("max id", "onchange");
        getFromCallLogAndInsertIntoHistory();
    }

    private void getFromCallLogAndInsertIntoHistory() {

        Long maxId = 0L;

        SharedPreferences sp = DaoTestApplication.getSharedPreferences();
        maxId = sp.getLong(Constants.CURRENT_CALL_LOG_ID, Long.MAX_VALUE);
        Log.d("max id", " " + maxId);
        Cursor managedCursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI,
                null, "_id > ?", new String[]{String.valueOf(maxId)}, null);

        if (managedCursor != null && managedCursor.moveToFirst()) {
            int lId = managedCursor.getColumnIndex(android.provider.CallLog.Calls._ID);
            int lNumber = managedCursor.getColumnIndex(android.provider.CallLog.Calls.NUMBER);
            int lDate = managedCursor.getColumnIndex(android.provider.CallLog.Calls.DATE);
            int lDuration = managedCursor.getColumnIndex(android.provider.CallLog.Calls.DURATION);
            int lCallType = managedCursor.getColumnIndex(android.provider.CallLog.Calls.TYPE);
            int lName = managedCursor.getColumnIndex(android.provider.CallLog.Calls.CACHED_NAME);
            int lNumberType = managedCursor.getColumnIndex(android.provider.CallLog.Calls
                    .CACHED_NUMBER_TYPE);
            int lNew = managedCursor.getColumnIndex(android.provider.CallLog.Calls.NEW);
            long lLogId = maxId;

            while (!managedCursor.isAfterLast()) {

                lLogId = managedCursor.getLong(lId);
                String number = managedCursor.getString(lNumber);
                String name = managedCursor.getString(lName);
                String callDate = managedCursor.getString(lDate);
                Date callDayTime = new Date(Long.valueOf(callDate));
                int duration = managedCursor.getInt(lDuration);
                String callType = managedCursor.getString(lCallType);
                int dircode = Integer.parseInt(callType);
                String dir = null;
                if (dircode != 10) {
                    switch (dircode) {
                        case CallLog.Calls.OUTGOING_TYPE:
                            dir = Constants.OUTGOING_CALL;
                            break;

                        case CallLog.Calls.INCOMING_TYPE:
                            dir = Constants.RECEIVED_CALL;
                            break;

                        case CallLog.Calls.MISSED_TYPE:
                            dir = Constants.MISSED_CALL;
                            break;
                        case 5:
                            dir = Constants.RECEIVED_CALL;
                            break;
                        case 6:
                            dir = Constants.BLOCKED_CALL;
                            break;
                        default :
                            break;
                    }

                    String category=getCallCategory(context, number);
                    CallHistory record = new CallHistory();
                    record.setCallHistoryResponseId((long) 0);
                    record.setOtherNumber(number);
                   // record.setName(getName(number));
                    record.setType(dir);
                    record.setHappenedAt(callDayTime);
                    record.setSeconds(duration);
                   // record.setMyNumber(DaoTestApplication.getInstance().getMyPhoneNumber());
                    record.setStatus(Constants.STATUS_UNSENT);
                    record.setLogId(lLogId);
                    record.setSim1Roaming(DaoTestApplication.getInstance().getSim1RoamingStatus());
                    record.setSim2Roaming("nil");
                 //   record.setPopupText("nil");
                    record.setCallCategory(category);
                    record.setIsInNetwork(DaoTestApplication.getInstance().checkIsInNetwork(number,category));


                    HistoryRepo.insertOrUpdate(context, record);


                    Date timeNow = new Date();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putLong(Constants.CURRENT_CALL_LOG_ID, lLogId);
                    editor.putLong(Constants.LOG_INSERTED_AT, System.currentTimeMillis());
                    editor.commit();

                    long popupInsertedAt = sp.getLong(Constants.POPUP_INSERTED_AT, 0L);
                    long logSeconds = Math.abs((new Date(popupInsertedAt).getTime()
                            - timeNow.getTime()) / 1000);
                    if (logSeconds < Constants.USSD_DELAY) {
                        insertPopupIntoCallLog(context, record);
                    }
                }
                managedCursor.moveToNext();
            }
        }
    }

    public String getCallCategory(Context context,String number) {
        String category = null;
        String myCircle = DaoTestApplication.getSharedPreferences().getString("myCircleCode", "");
        String roaming = DaoTestApplication.getInstance().getSim1RoamingStatus();
        Logger.d(TAG, myCircle);
        if (number.startsWith("+91") || number.startsWith("0") || number.length()==10) {
            number = number.replace("+91", "");
            if (number.startsWith("0")) {
                number = number.replaceFirst("0", "");
            }
            String code = number.substring(0, 4);
            String otherCircle = NumberCodesRepo.getNumberCircle(context, code);
            if ((otherCircle.equals("CH") && myCircle.equals("TN")) ||
                    (otherCircle.equals("TN") && myCircle.equals("CH"))) {
                if (roaming.equals("true")) {
                    category = "Roaming local";
                } else {
                    category = "local";
                }
            } else if (otherCircle.equals(myCircle)) {
                    if (roaming.equals("true")) {
                        category = "Roaming local";
                    } else {
                        category = "local";
                    }
            } else {
                    if (roaming.equals("true")) {
                        category = "Roaming STD";
                    } else {
                        category = "STD";
                    }
                }

            if(otherCircle.equals("invalid"))
            {
                String circle= LandlineCodesRepo.getMobileCircle(context, number);

                if (circle.equals(myCircle)) {
                    if (roaming.equals("true")) {
                        category = "Roaming local landline";
                    } else {
                        category = "local landline";
                    }
                } else if(circle.equals("invalid")) {
                    category="inavlid";
                } else{
                    if (roaming.equals("true")) {
                        category = "Roaming STD landline";
                    } else {
                        category = "STD landline";
                    }
                }
            }
        }
        else if(number.startsWith("+"))
        {
            String country = CountryCodesRepo.getCountryDetail(context, number);
            if(country.equals("invalid"))
            {
                category="invalid";
            }
            else
            {
                category="ISD";
            }
        }
        else {
            category = "invalid";
        }
        return  category;
    }
    private void insertPopupIntoCallLog(Context context, CallHistory history) {
        PopupAlerts popupAlert = PopupAlertsRepo.getLastLog(context);
      //  history.setPopupText(popupAlert.getPopupText());
        HistoryRepo.insertOrUpdate(context, history);
    }

    public String getName(String number) {
        String name = number;
        number=number.replace("+91","");
        if(number.startsWith("0"))
           number= number.replaceFirst("0","");
        Contacts contacts = new Contacts();
        contacts = ContactsRepo.getName(context, number);
        if (contacts.getIsActive() != 0) {
            name = contacts.getName();
        }
        return name;
    }
//    private String getSim1RoamingStatus()
//    {
//        String sim1Roaming="false";
//        TelephonyManager telephonyManager=(TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
//        if(telephonyManager.isNetworkRoaming())
//            sim1Roaming="true";
//        return sim1Roaming;
//    }
}