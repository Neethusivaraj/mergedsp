package com.akira.superplan.listeners;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.Telephony;
import android.util.Log;

import com.akira.superplan.DaoTestApplication;
import com.akira.superplan.dao.MessageHistory;
import com.akira.superplan.dao.UserBalance;
import com.akira.superplan.repositories.MessageHistoryRepo;
import com.akira.superplan.repositories.UserBalanceRepo;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.Logger;

import java.util.Date;
import java.util.List;


/**
 * Created by nanda on 5/12/15.
 */
public class MessageLogObserver extends ContentObserver {

    private static final String TAG = "ContentObserver";
    private Handler handler;
    private Context context;

    private static int counter = 0;
    private static int receivedCounter = 0;

    public MessageLogObserver(Handler handler, Context context) {
        super(handler);
        this.handler = handler;
        this.context = context;
    }

    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        checkMessageForType();
    }

    private void checkMessageForType() {
        Uri uriSMSURI = Uri.parse("content://sms/");

        Long maxId = 0L;
        Long messageLogId = 0L;
        String protocol;

        SharedPreferences sp = DaoTestApplication.getSharedPreferences();
        maxId = sp.getLong(Constants.CURRENT_MESSAGE_LOG_ID, Long.MAX_VALUE);

        Cursor managedCursor = context.getContentResolver().query(uriSMSURI, null, null, null, null);
        managedCursor.moveToNext();

        int lId = managedCursor.getColumnIndex(Telephony.Sms._ID);
        int lProtocol = managedCursor.getColumnIndex("protocol");

        protocol = managedCursor.getString(lProtocol);
        messageLogId = managedCursor.getLong(lId);

        if (protocol == null && messageLogId > maxId) {
            insertMessageIntoDB(managedCursor, Constants.SMS_SENT, messageLogId);
            insertOperatorMessageIntoDB(managedCursor, Constants.SMS_SENT, messageLogId);
            counter += 1;

            SharedPreferences.Editor editor = sp.edit();
            editor.putLong(Constants.CURRENT_MESSAGE_LOG_ID, messageLogId);
            editor.commit();
        } else if (protocol != null && messageLogId > maxId) {
            insertMessageIntoDB(managedCursor, Constants.SMS_RECEIVED, messageLogId);
            insertOperatorMessageIntoDB(managedCursor, Constants.SMS_RECEIVED, messageLogId);
            receivedCounter += 1;
            //the message is received just now

            SharedPreferences.Editor editor = sp.edit();
            editor.putLong(Constants.CURRENT_MESSAGE_LOG_ID, messageLogId);
            editor.commit();
        }
        managedCursor.close();
    }

    private void insertMessageIntoDB(Cursor managedCursor, String type, Long messageLogId) {

        String myNumber = DaoTestApplication.getInstance().getMyPhoneNumber();

        int lSender = managedCursor.getColumnIndex(Telephony.Sms.ADDRESS);
        int lMessage = managedCursor.getColumnIndex(Telephony.Sms.BODY);
        int lHappenedAt = managedCursor.getColumnIndex(Telephony.Sms.DATE);

        String sender = managedCursor.getString(lSender);
        String message = managedCursor.getString(lMessage);
        String messageDate = managedCursor.getString(lHappenedAt);
        Date happenedAt = new Date(Long.valueOf(messageDate));

        if (sender == null) {
            return;
        }

        MessageHistory mh = new MessageHistory();
        mh.setMessageResponseId((long) 0);
        mh.setMessageLogId(messageLogId);
        //mh.setMyNumber(myNumber);
        mh.setOtherNumber(sender);

        mh.setMessageLength(Long.valueOf(message.length()));
        mh.setHappenedAt(happenedAt);
        mh.setStatus(Constants.STATUS_UNSENT);
        mh.setType(type);
        MessageHistoryRepo.insertOrUpdate(context, mh);
    }

    private void insertOperatorMessageIntoDB(Cursor managedCursor, String type, Long messageLogId) {

        String myNumber = DaoTestApplication.getInstance().getMyPhoneNumber();

        int lSender = managedCursor.getColumnIndex(Telephony.Sms.ADDRESS);
        int lMessage = managedCursor.getColumnIndex(Telephony.Sms.BODY);
        int lHappenedAt = managedCursor.getColumnIndex(Telephony.Sms.DATE);


        String sender = managedCursor.getString(lSender);
        String message = managedCursor.getString(lMessage);
        String messageDate = managedCursor.getString(lHappenedAt);
        Date happenedAt = new Date(Long.valueOf(messageDate));

        if (sender == null) {
            return;
        }

        if(sender.equals("55333"))
        {
            Log.d("reliance data", " " + message);
            Date timeNow = new Date();
            UserBalance updateBalance = new UserBalance();
            updateBalance.setUserBalanceResponseId((long) 0);
            updateBalance.setCurrentBalance(message);
            updateBalance.setLastUpdated(timeNow);
            UserBalanceRepo.insertOrUpdate(context, updateBalance);
            List<UserBalance> list = UserBalanceRepo.getAllBalance(context);
            for (UserBalance balance : list) {
                Logger.d(TAG, "current balance " + balance.getCurrentBalance() +
                        "callValidity" + balance.getCallValidity() + " time "
                        + balance.getLastUpdated() + " data " + balance.getDataBalance()
                        + "data validity " + balance.getDataValidity());
            }
        }


     //   String senderName = OperatorMessageSenderRepo.getSender(context,sender);
//        Logger.d(TAG,"Sender Name : "+senderName);

       /* if (DaoTestApplication.stringContainsItemFromList(sender)) {
            OperatorMessageHistory mh = new OperatorMessageHistory();
            mh.setMessageLogId(messageLogId);
            mh.setMyNumber(myNumber);
            mh.setOtherNumber(sender);
            mh.setMessageText(message);
            mh.setMessageLength(Long.valueOf(message.length()));
            mh.setHappenedAt(happenedAt);
            mh.setStatus(Constants.STATUS_UNSENT);
            mh.setType(type);
            OperatorMessageHistoryRepo.insertOrUpdate(context, mh);
        } */
    }
}
