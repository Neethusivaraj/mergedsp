package com.akira.superplan.listeners;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.util.Log;

import com.akira.superplan.dao.Contacts;
import com.akira.superplan.repositories.ContactsRepo;
import com.akira.superplan.repositories.HistoryRepo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ContactsObserver extends ContentObserver {

    private static final String TAG = "ContactsObserver";
    private static final long ONE_MINUTE_IN_MILLIS = 60000;

    private Handler handler;
    private Context context;

    public ContactsObserver(Handler handler, Context context) {
        super(handler);
        this.handler = handler;
        this.context = context;
    }

    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Message msg = handler.obtainMessage();
        Log.d(TAG, "onchange called" + msg.toString());
        changeContact();
    }

    public void changeContact() {

        String output = null;
        ContentResolver contentResolver = context.getContentResolver();
        Date d = new Date();
        Date beforeFiveMins = new Date(d.getTime() - (2 * ONE_MINUTE_IN_MILLIS));
        String a = beforeFiveMins.getTime() + " ";
        String b = a.substring(0, a.length() - 1);
//        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null,
//                ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP
//                        + " > ?", new String[]{b}, null);

        Cursor cursor;

        if (Build.VERSION.SDK_INT <= 18) {
            Log.d(TAG, "onchange called inside if");
            cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null,
                    ContactsContract.Contacts.CONTACT_STATUS_TIMESTAMP
                            + " > ?", new String[]{b}, null);
        }
        else {
            Log.d(TAG, "onchange called inside else");
            cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null,
                    ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP
                            + " > ?", new String[]{b}, null);
        }

        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.
                        Contacts.DISPLAY_NAME));
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.
                        Contacts.NAME_RAW_CONTACT_ID));
                Log.d(TAG, name + " " + id);
                String version = null;
                String lNumber;
                Cursor rawcursor = contentResolver.query(ContactsContract.RawContacts.
                                CONTENT_URI, null, ContactsContract.RawContacts._ID + " = ? ",
                        new String[]{id}, null);
                while (rawcursor.moveToNext()) {
                    version = rawcursor.getString(rawcursor.getColumnIndex(
                            ContactsContract.RawContacts.VERSION));
                }
                rawcursor.close();

                List<Contacts> lContactList = ContactsRepo.getVersion(context, id);
                for (Contacts contacts : lContactList) {
                    if (contacts.getVersion() != version) {
                        String lOldName = contacts.getName();
                        if (lOldName != name) {
                            contacts.setName(name);
                            ContactsRepo.update(context, contacts);
                            HistoryRepo.updateContact(context, lOldName, name);
                        }
                    }
                }
                List<CheckNumber> lNewNumbers = new ArrayList<CheckNumber>();
                Cursor phoneCursor = contentResolver.query(ContactsContract.
                        CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.
                        CommonDataKinds.Phone.RAW_CONTACT_ID + " = ?", new String[]{id}, null);
                int i = 0;
                while (phoneCursor.moveToNext()) {
                    lNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.
                            CommonDataKinds.Phone.NUMBER));
                    lNumber = lNumber.replaceAll("[^\\x00-\\x7F]", "");
                    lNumber = lNumber.replaceAll(" ", "");

                    CheckNumber cn = new CheckNumber();
                    cn.number = lNumber;
                    cn.isactive = 0;
                    lNewNumbers.add(i, cn);
                }
                phoneCursor.close();
                for (Contacts contacts : lContactList) {
                    int present = 0;
                    for (CheckNumber checkNumber : lNewNumbers) {
                        if (contacts.getNumber().equals(checkNumber.number)) {
                            checkNumber.isactive = 1;
                            present = 1;
                            break;
                        }

                    }
                    if (present == 0) {
                        contacts.setIsActive(0);
                        ContactsRepo.update(context, contacts);
                        HistoryRepo.updateContact(context, contacts.getName(),
                                contacts.getNumber());
                    }
                }

                for (CheckNumber checkNumber : lNewNumbers) {
                    if (checkNumber.isactive == 0) {
                        Contacts newContact = new Contacts();
                        newContact.setDeviceContactID(id);
                        newContact.setName(name);
                        newContact.setIsActive(1);
                        newContact.setNumber(checkNumber.number);
                        newContact.setVersion(version);
                        ContactsRepo.insertOrUpdate(context, newContact);
                        HistoryRepo.updateContact(context, newContact.getNumber(),
                                newContact.getName());
                    }

                }

            } while (cursor.moveToNext());
            cursor.close();
        } else {
            Cursor deletecursor = contentResolver.query(ContactsContract.Contacts.
                    CONTENT_URI, null, null, null, null);
            List<String> list = new ArrayList<String>();
            while (deletecursor.moveToNext()) {

                list.add(deletecursor.getString(deletecursor.getColumnIndex(ContactsContract.
                        Contacts.NAME_RAW_CONTACT_ID)));
            }

            List<Contacts> contacts = ContactsRepo.getAllContacts(context);
            for (Contacts contact : contacts) {
                int present = 0;
                for (String checkId : list) {
                    if (contact.getDeviceContactID().equals(checkId)) {
                        present = 1;
                        break;
                    }
                }
                if (present == 0) {
                    HistoryRepo.updateContact(context, contact.getName(), contact.getNumber());
                    ContactsRepo.delete(context, contact);
                }

            }
        }

    }
}
